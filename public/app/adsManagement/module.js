define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.adsManagement', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.adsManagement', {
            abstract: true,
            data: {title: 'Ads Management'}
        })
        // Childs
        .state('app.adsManagement.ads', {
            url: '/ads',
            data: {title: 'Ads Management'},
            views: {
                "content@app": {
                    templateUrl: 'app/adsManagement/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'adsManagement/controller/adsIndexCtrl',
                            'adsManagement/controller/AdsCtrl',

                            'adsManagement/directives/adsList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});