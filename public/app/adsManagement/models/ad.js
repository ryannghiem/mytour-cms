
define(['adsManagement/module'], function (module) {

    'use strict';

    return module.registerFactory('adsList', function ($http, $q) {
        var dfd = $q.defer();

        var adsList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'ads',
            method: "GET",
            params: {
                orderBy: 'ads.created_at',
                orderDir: 'desc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            adsList.data.data = response.data.data;
            adsList.data.currentPage = 1;
            adsList.data.itemsPerPage = 10;
            adsList.data.orderBy = 'ads.created_at';
            adsList.data.orderDir = 'desc';
            adsList.data.adSearchValue = '';
            adsList.data.totalAds = response.data.recordsFiltered;
            dfd.resolve(adsList)
        });

        return adsList;
    });
});
