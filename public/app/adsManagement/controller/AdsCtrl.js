
define(['adsManagement/module'], function (module) {

    'use strict';

    module.registerController('AdsCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.selectStatus = [
            {value: '1', text: 'Published'},
            {value: '0', text: 'Pending'},
            {value: '-1', text: 'Trash'}
        ];

        $scope.clearFilterAds = function () {
            $('.btn-ads-refresh i').addClass('fa-spin');
            $('.ads-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'ads',
                method: "GET",
                params: {
                    orderBy: 'ads.created_at',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.adsList.itemsPerPage,
                    search: '',
                    status : ''
                }
            }).then(function (response) {
                $('.ads-article .tables_processing').hide();
                $('.btn-ads-refresh i').removeClass('fa-spin');
                $scope.adsList.data = response.data.data;
                $scope.adsList.currentPage = 1;
                $scope.adsList.orderBy = 'ads.created_at';
                $scope.adsList.adsSearchValue = '';
                $scope.adsList.filterByStatus = '';
                $scope.adsList.orderDir = 'desc';
                $scope.adsList.totalAds = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterAds = function () {
            $('.ads-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'ads',
                method: "GET",
                params: {
                    orderBy: $scope.adsList.orderBy,
                    orderDir: $scope.adsList.orderDir,
                    start: 0,
                    length: $scope.adsList.itemsPerPage,
                    search: $scope.adsList.adSearchValue,
                    status : $scope.adsList.filterByStatus
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.ads-article .tables_processing').hide();
                $scope.adsList.data = response.data.data;
                $scope.adsList.currentPage = 1;
                $scope.adsList.totalAds = response.data.recordsFiltered;
            });
        };

        $scope.setPageAds = function (page) {
            $('.ads-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'ads',
                method: "GET",
                params: {
                    orderBy: $scope.adsList.orderBy,
                    orderDir: $scope.adsList.orderDir,
                    start: (page - 1) * $scope.adsList.itemsPerPage,
                    length: $scope.adsList.itemsPerPage,
                    search: $scope.adsList.adsSearchValue,
                    status : $scope.adsList.filterByStatus
                }
            }).then(function (response) {
                $('.ads-article .tables_processing').hide();
                $scope.adsList.data = response.data.data;
                $scope.adsList.currentPage = page;
            });
        };

        $scope.sortAds = function (colName) {
            $('.ads-article .tables_processing').show();
            $scope.adsList.orderBy = colName;
            $scope.adsList.orderDir = $scope.adsList.orderDir == 'asc' ? 'desc' : 'asc';
            $http({
                url: appConfig.api_path + 'ads',
                method: "GET",
                params: {
                    orderBy: $scope.adsList.orderBy,
                    orderDir: $scope.adsList.orderDir,
                    start: ($scope.adsList.currentPage - 1) * $scope.adsList.itemsPerPage,
                    length: $scope.adsList.itemsPerPage,
                    search: $scope.adsList.adsSearchValue,
                    status : $scope.adsList.filterByStatus
                }
            }).then(function (response) {
                $('.ads-article .tables_processing').hide();
                $scope.adsList.data = response.data.data;
            });
        };

        $scope.addAds = function () {
            $scope.currentAds = {};
            $scope.currentAds.addMode = true;
            $scope.currentAds.ordering = 9999;
            $scope.currentAds.show_in_home = 0;
            $('#modal-add-ads').modal('show');
        };

        $scope.editAds = function (index, ads) {
            $scope.currentAds = {};
            $scope.currentAds.addMode = false;
            var $id = ads.id;
            $http({
                url: appConfig.api_path + 'ads/' + $id + '/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentAds = response.data.data;
                $(".ads-select-hotel").select2('val', $scope.currentAds.hotel_id);
                $(".ads-select-room").select2('val', $scope.currentAds.room_id);
                $scope.currentAds.version = '?v=' + $scope.getRandomSpan();
                if ($scope.currentAds.image_origin) {
                    $('.ads-image-origin div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentAds.image_origin + $scope.currentAds.version + "' />");
                }
                if ($scope.currentAds.image_small) {
                    $('.ads-image-small div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentAds.image_small + $scope.currentAds.version + "' />");
                }
            });
            $('#modal-add-ads').modal('show');
        };

        // Save change
        $scope.saveAds = function ($status) {
            var formValidation = $('.addAdsForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($('.ads-image-origin img').attr('src').length == 0) {
                $('.ads-image-origin').addClass('has-error');
                $('.modal').animate({
                    scrollTop: $('.ads-image-origin').position().top + parseFloat($('.ads-image-origin').parents(".modal-body").position().top)
                }, 1000);
                return;
            }
            var $currentAds = angular.copy($scope.currentAds);
            if ($status) $currentAds.status = $status;
            $currentAds.image_origin = $('.ads-image-origin img').attr('src').replace($currentAds.version,'');
            $currentAds.image_small = ($('.ads-image-small img') && $('.ads-image-small img').attr('src')) ? $('.ads-image-small img').attr('src').replace($currentAds.version,'') : '';
            $('#modal-add-ads').modal('hide');
            $('.ads-article .tables_processing').show();
            $http.put(appConfig.api_path + 'ads/update',$currentAds
            ).then(function (response) {
                if (response.data.msg === true) {
                    $scope.filterAds();
                }
            });
        };

        $scope.saveResetAds = function () {
            var formValidation = $('.resetAdForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($scope.resetAds.emailExist) return;
            $http.post(appConfig.api_path + 'user/resetPass',$scope.resetAd
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterAds();
                }
            });
            $('#modal-reset-ads').modal('hide');
            $('.ads-article .tables_processing').show();
        };

        $scope.deleteAds = function (id) {
            $('.ads-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'ads/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.adsList.orderBy,
                    orderDir: $scope.adsList.orderDir,
                    start: ($scope.adsList.currentPage - 1) * $scope.adsList.itemsPerPage,
                    length: $scope.adsList.itemsPerPage,
                    search: $scope.adsList.adsSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.ads-article .tables_processing').text('Xoá thành công!');
                    $scope.filterAds();
                } else $('.ads-article .tables_processing').text("Không thể xóa nhân viên này!");
                setTimeout(function () {
                    $('.ads-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.ads-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'ads/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.ads-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("adsColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.name = true;
                $scope.columnsShown.description = true;
                $scope.columnsShown.ordering = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("adsColumnsShown",JSON.stringify($scope.columnsShown));
        };

        $scope.getRandomSpan = function(){
            return Math.floor((Math.random()*10)+1);
        };
    });
});

