define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.introduction', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.introduction', {
            abstract: true,
            data: {title: 'Hướng dẫn sử dụng'}
        })
        // Childs
        .state('app.introduction.index', {
            url: '/intro/index',
            data: {title: 'Hướng dẫn sử dụng'},
            views: {
                "content@app": {
                    templateUrl: 'app/introduction/index/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'introduction/index/controller/indexCtrl'

                            // 'introduction/index/directives/customersList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});