
define(['staffsManager/module'], function (module) {

    'use strict';

    module.registerController('SalarycCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.uppercaseUsername = function () {
            $scope.currentSalaryc.username = $scope.currentSalaryc.username.toUpperCase();
        };

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.clearFilterSalarycs = function () {
            $('.btn-staff-refresh i').addClass('fa-spin');
            $('.salaryc-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/configList',
                method: "GET",
                params: {
                    orderBy: 's.name',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.salarycList.itemsPerPage,
                    search: ''
                }
            }).then(function (response) {
                $('.salaryc-article .tables_processing').hide();
                $('.btn-staff-refresh i').removeClass('fa-spin');
                $scope.salarycList.data = response.data.data;
                $scope.salarycList.currentPage = 1;
                $scope.salarycList.orderBy = 's.name';
                $scope.salarycList.salarycSearchValue = '';
                $scope.salarycList.orderDir = 'desc';
                $scope.salarycList.totalSalaryc = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterSalaryc = function () {
            $('.salaryc-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'salary/configList',
                method: "GET",
                params: {
                    orderBy: $scope.salarycList.orderBy,
                    orderDir: $scope.salarycList.orderDir,
                    start: 0,
                    length: $scope.salarycList.itemsPerPage,
                    search: $scope.salarycList.salarycSearchValue
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.salaryc-article .tables_processing').hide();
                $scope.salarycList.data = response.data.data;
                $scope.salarycList.currentPage = 1;
                $scope.salarycList.totalSalaryc = response.data.recordsFiltered;
            });
        };

        $scope.setPageSalaryc = function (page) {
            $('.salaryc-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/configList',
                method: "GET",
                params: {
                    orderBy: $scope.salarycList.orderBy,
                    orderDir: $scope.salarycList.orderDir,
                    start: (page - 1) * $scope.salarycList.itemsPerPage,
                    length: $scope.salarycList.itemsPerPage,
                    search: $scope.salarycList.salarycSearchValue
                }
            }).then(function (response) {
                $('.salaryc-article .tables_processing').hide();
                $scope.salarycList.data = response.data.data;
                $scope.salarycList.currentPage = page;
            });
        };

        $scope.sortSalaryc = function (colName) {
            $('.salaryc-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/configList',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.salarycList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.salarycList.currentPage - 1) * $scope.salarycList.itemsPerPage,
                    length: $scope.salarycList.itemsPerPage,
                    search: $scope.salarycList.salarycSearchValue
                }
            }).then(function (response) {
                $('.salaryc-article .tables_processing').hide();
                $scope.salarycList.data = response.data.data;
                $scope.salarycList.orderBy = colName;
                $scope.salarycList.orderDir = $scope.salarycList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.addSalaryc = function () {
            $scope.currentSalaryc = {};
            $scope.currentSalaryc.addMode = true;
            $scope.currentSalaryc.status = 1;
            $scope.currentSalaryc.monthly_salary = 0;
            $scope.currentSalaryc.daily_salary = 0;
            $scope.currentSalaryc.salary_type = 2;
            $('#modal-add-salaryc').modal('show');
        };

        $scope.editSalaryc = function (index, staff) {
            $scope.currentSalaryc = {};
            $scope.currentSalaryc.addMode = false;
            var $id = staff.id;
            $http({
                url: appConfig.api_path + 'salary/configList/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentSalaryc = response.data.data;
                if ($scope.currentSalaryc.birthday) {
                    $scope.currentSalaryc.birthday = moment($scope.currentSalaryc.birthday).format('DD/MM/YYYY');
                }
            });
            $('#modal-add-salaryc').modal('show');
        };

        // Save change
        $scope.saveSalaryc = function () {
            var formValidation = $('.addSalarycForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            $('#modal-add-salaryc').modal('hide');
            $('.salaryc-article .tables_processing').show();
            $http.post(appConfig.api_path + 'salary/configList/update',$scope.currentSalaryc
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterSalaryc();
                    }
                });
        };

        $scope.deleteSalaryc = function (id) {
            $('.salaryc-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/configList/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.salarycList.orderBy,
                    orderDir: $scope.salarycList.orderDir,
                    start: ($scope.salarycList.currentPage - 1) * $scope.salarycList.itemsPerPage,
                    length: $scope.salarycList.itemsPerPage,
                    search: $scope.salarycList.salarycSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.salaryc-article .tables_processing').text('Xoá thành công!');
                    $scope.filterSalaryc();
                } else $('.salaryc-article .tables_processing').text("Không thể xóa mức lương này!");
                setTimeout(function () {
                    $('.salaryc-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.salaryc-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'salary/configList/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.salaryc-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("salarycColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.salary_type = true;
                $scope.columnsShown.description = true;
                $scope.columnsShown.allowance = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("salarycColumnsShown",JSON.stringify($scope.columnsShown));
        };
    });
});

