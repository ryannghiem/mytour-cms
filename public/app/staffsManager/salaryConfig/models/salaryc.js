
define(['staffsManager/module'], function (module) {

    'use strict';

    return module.registerFactory('salarycList', function ($http, $q) {
        var dfd = $q.defer();

        var salarycList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'salary/configList',
            method: "GET",
            params: {
                orderBy: 's.name',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            salarycList.data.data = response.data.data;
            salarycList.data.currentPage = 1;
            salarycList.data.itemsPerPage = 10;
            salarycList.data.orderBy = 's.name';
            salarycList.data.orderDir = 'asc';
            salarycList.data.salarycSearchValue = '';
            salarycList.data.totalSalaryc = response.data.recordsFiltered;
            dfd.resolve(salarycList)
        });

        return salarycList;
    });
});
