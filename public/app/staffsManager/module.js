define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.staffsManager', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.staffsManager', {
            abstract: true,
            data: {title: 'Quản lý nhân viên'}
        })
        // Childs
        .state('app.staffsManager.staff', {
            url: '/staffs',
            data: {title: 'Danh sách nhân viên'},
            views: {
                "content@app": {
                    templateUrl: 'app/staffsManager/staff/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'staffsManager/staff/controller/staffsIndexCtrl',
                            'staffsManager/staff/controller/StaffsCtrl',

                            'staffsManager/staff/directives/staffsList'
                        ])
                    }
                }
            }
        })
        .state('app.staffsManager.salaryConfig', {
            url: '/staffs/salary-config',
            data: {title: 'Danh mục mức lương'},
            views: {
                "content@app": {
                    templateUrl: 'app/staffsManager/salaryConfig/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'staffsManager/salaryConfig/controller/salarycIndexCtrl',
                            'staffsManager/salaryConfig/controller/SalarycCtrl',

                            'staffsManager/salaryConfig/directives/salarycList'
                        ])
                    }
                }
            }
        })
        .state('app.staffsManager.salaryTimekeeping', {
            url: '/staffs/salary-timekeeping',
            data: {title: 'Chấm công'},
            views: {
                "content@app": {
                    templateUrl: 'app/staffsManager/salaryTimekeeping/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'staffsManager/salaryTimekeeping/controller/salarytIndexCtrl',
                            'staffsManager/salaryTimekeeping/controller/SalarytCtrl',

                            'staffsManager/salaryTimekeeping/directives/salarytList'
                        ])
                    }
                }
            }
        })
        .state('app.staffsManager.salaryHistory', {
            url: '/staffs/salary-history',
            data: {title: 'Lịch sử trả lương'},
            views: {
                "content@app": {
                    templateUrl: 'app/staffsManager/salaryHistory/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'staffsManager/salaryHistory/controller/salaryhIndexCtrl',
                            'staffsManager/salaryHistory/controller/SalaryhCtrl',

                            'staffsManager/salaryHistory/directives/salaryhList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});