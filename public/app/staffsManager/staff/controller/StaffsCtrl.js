
define(['staffsManager/module'], function (module) {

    'use strict';

    module.registerController('StaffsCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.uppercaseUsername = function () {
            $scope.currentStaff.username = $scope.currentStaff.username.toUpperCase();
        };

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.clearFilterStaffs = function () {
            $('.btn-staff-refresh i').addClass('fa-spin');
            $('.staff-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'staff/list',
                method: "GET",
                params: {
                    orderBy: 'staffs.lastname',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.staffsList.itemsPerPage,
                    search: '',
                    status : ''
                }
            }).then(function (response) {
                $('.staff-article .tables_processing').hide();
                $('.btn-staff-refresh i').removeClass('fa-spin');
                $scope.staffsList.data = response.data.data;
                $scope.staffsList.currentPage = 1;
                $scope.staffsList.orderBy = 'staffs.lastname';
                $scope.staffsList.staffSearchValue = '';
                $scope.staffsList.filterByStatus = '';
                $scope.staffsList.orderDir = 'desc';
                $scope.staffsList.totalStaffs = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterStaff = function () {
            $('.staff-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'staff/list',
                method: "GET",
                params: {
                    orderBy: $scope.staffsList.orderBy,
                    orderDir: $scope.staffsList.orderDir,
                    start: 0,
                    length: $scope.staffsList.itemsPerPage,
                    search: $scope.staffsList.staffSearchValue,
                    status : $scope.staffsList.filterByStatus
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.staff-article .tables_processing').hide();
                $scope.staffsList.data = response.data.data;
                $scope.staffsList.currentPage = 1;
                $scope.staffsList.totalStaffs = response.data.recordsFiltered;
            });
        };

        $scope.setPageStaff = function (page) {
            $('.staff-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'staff/list',
                method: "GET",
                params: {
                    orderBy: $scope.staffsList.orderBy,
                    orderDir: $scope.staffsList.orderDir,
                    start: (page - 1) * $scope.staffsList.itemsPerPage,
                    length: $scope.staffsList.itemsPerPage,
                    search: $scope.staffsList.staffSearchValue,
                    status : $scope.staffsList.filterByStatus
                }
            }).then(function (response) {
                $('.staff-article .tables_processing').hide();
                $scope.staffsList.data = response.data.data;
                $scope.staffsList.currentPage = page;
            });
        };

        $scope.sortStaff = function (colName) {
            $('.staff-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'staff/list',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.staffsList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.staffsList.currentPage - 1) * $scope.staffsList.itemsPerPage,
                    length: $scope.staffsList.itemsPerPage,
                    search: $scope.staffsList.staffSearchValue,
                    status : $scope.staffsList.filterByStatus
                }
            }).then(function (response) {
                $('.staff-article .tables_processing').hide();
                $scope.staffsList.data = response.data.data;
                $scope.staffsList.orderBy = colName;
                $scope.staffsList.orderDir = $scope.staffsList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.addStaff = function () {
            $scope.currentStaff = {};
            $scope.currentStaff.addMode = true;
            $scope.currentStaff.gender = 1;
            $scope.currentStaff.status = 1;
            $scope.currentStaff.user_type = 1;
            if ($rootScope.userLogin.store_id) {
                $scope.currentStaff.store_id = $rootScope.userLogin.store_id;
                $(".staff-select-store").select2('val', $scope.currentStaff.store_id);
            }
            $('#modal-add-staff').modal('show');
        };

        $scope.editStaff = function (index, staff) {
            $scope.currentStaff = {};
            $scope.currentStaff.addMode = false;
            var $id = staff.id;
            $http({
                url: appConfig.api_path + 'staff/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentStaff = response.data.data;
                if ($scope.currentStaff.birthday) {
                    $scope.currentStaff.birthday = moment($scope.currentStaff.birthday).format('DD/MM/YYYY');
                }
                if ($scope.currentStaff.start_work) {
                    $scope.currentStaff.start_work = moment($scope.currentStaff.start_work).format('DD/MM/YYYY');
                }
                //$scope.currentStaff.store_id = JSON.parse($scope.currentStaff.store_id);
                $(".staff-select-store").select2('val', $scope.currentStaff.store_id);
            });
            $('#modal-add-staff').modal('show');
        };

        $scope.resetPassStaff = function (index, staff) {
            $scope.resetStaff = {};
            $scope.resetStaff.id = staff.id;
            $scope.resetStaff.email = staff.email;
            $scope.resetStaff.password = '';
            $('#modal-reset-staff').modal('show');
        };

        // Save change
        $scope.saveStaff = function () {
            var formValidation = $('.addStaffForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($scope.currentStaff.emailExist) return;
            if ($('#staff-birthday-date').val()) {
                $scope.currentStaff.birthday = moment($('#staff-birthday-date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.currentStaff.birthday = null;
            if ($('#staff-start-work').val()) {
                $scope.currentStaff.start_work = moment($('#staff-start-work').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.currentStaff.start_work = null;
            $scope.currentStaff.store_id = $(".staff-select-store").select2('val');//.toString();
            $('#modal-add-staff').modal('hide');
            $('.staff-article .tables_processing').show();
            $http.post(appConfig.api_path + 'staff/update',$scope.currentStaff
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterStaff();
                    }
                });
        };

        $scope.saveResetStaff = function () {
            var formValidation = $('.resetStaffForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($scope.resetStaff.emailExist) return;
            $http.post(appConfig.api_path + 'user/resetPass',$scope.resetStaff
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterStaff();
                }
            });
            $('#modal-reset-staff').modal('hide');
            $('.staff-article .tables_processing').show();
        };

        $scope.deleteStaff = function (id) {
            $('.staff-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'staff/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.staffsList.orderBy,
                    orderDir: $scope.staffsList.orderDir,
                    start: ($scope.staffsList.currentPage - 1) * $scope.staffsList.itemsPerPage,
                    length: $scope.staffsList.itemsPerPage,
                    search: $scope.staffsList.staffSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.staff-article .tables_processing').text('Xoá thành công!');
                    $scope.filterStaff();
                } else $('.staff-article .tables_processing').text("Không thể xóa nhân viên này!");
                setTimeout(function () {
                    $('.staff-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.staff-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'staff/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.staff-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("staffColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.avatar = true;
                $scope.columnsShown.fullname = true;
                $scope.columnsShown.lastname = true;
                $scope.columnsShown.phone = true;
                $scope.columnsShown.email = true;
                $scope.columnsShown.firstname = false;
                $scope.columnsShown.address = false;
                $scope.columnsShown.birthday = false;
                $scope.columnsShown.identity_card = false;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("staffColumnsShown",JSON.stringify($scope.columnsShown));
        };

        $scope.checkEmailExist = function ($staff) {
            $http({
                url: appConfig.api_path + 'user/checkEmailExist',
                method: "GET",
                params: {
                    id : $staff.id,
                    email : $staff.email
                }
            }).then(function (response) {
                $staff.emailExist = response.data.exist
            });
        }
    });
});

