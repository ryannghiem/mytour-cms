

define(['staffsManager/module'], function(module){

    "use strict";

    return module.registerDirective('staffsList', function(staffsList){
        return {
            restrict: 'A',
            templateUrl: 'app/staffsManager/staff/views/staffs.tpl.html',
            link: function(scope){
                staffsList.initialized.then(function(){
                    scope.staffsList = staffsList.data;
                    setTimeout(function () {
                        var addStaffValidator = $(".addStaffForm").validate({
                            lang: 'vi',
                            errorElement: "span", // contain the error msg in a small tag
                            errorClass: 'help-block myErrorClass',
                            errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                                } else if (element.hasClass("ckeditor") || element.hasClass("date-required")) {
                                    error.appendTo($(element).closest('.form-group'));
                                } else {
                                    error.insertAfter(element);
                                    // for other inputs, just perform default behavior
                                }
                            },
                            highlight: function (element, errorClass, validClass) {
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                                } else {
                                    $(element).closest('.help-block').removeClass('valid');
                                    // display OK icon
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                                    // add the Bootstrap error class to the control group
                                }
                            },
                            unhighlight: function (element, errorClass, validClass) {
                                // revert the change done by hightlight
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                                } else {
                                    $(element).closest('.form-group').removeClass('has-error');
                                    // set error class to the control group
                                }
                            },
                            success: function (label, element) {
                                label.addClass('help-block valid');
                                // mark the current input as valid and display OK icon
                                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                            }
                        });
                        var resetStaffValidator = $('.resetStaffForm').validate({
                            lang: 'vi',
                            rules: {
                                email: {
                                    required: true,
                                    email: true,
                                    minlength: 5,
                                    maxlength: 100
                                },
                                password: {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32
                                },
                                password_confirmation: {
                                    required: true,
                                    minlength: 6,
                                    maxlength: 32,
                                    equalTo: "#new-password"
                                }
                            },
                            errorElement: "span", // contain the error msg in a small tag
                            errorClass: 'help-block myErrorClass',
                            focusInvalid: false,
                            invalidHandler: function (form, validator) {
                                if (!validator.numberOfInvalids())
                                    return;
                                $('html, body').animate({
                                    scrollTop: $form.offset().top - 100//$(validator.errorList[0].element).offset().top
                                }, 1000);

                            },
                            errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                                } else if (element.hasClass("ckeditor")) {
                                    error.appendTo($(element).closest('.form-group'));
                                } else if (element.parent().hasClass("input-group")) {
                                    error.insertAfter(element.parent());
                                } else {
                                    error.insertAfter(element);
                                    // for other inputs, just perform default behavior
                                }
                            },
                            highlight: function (element, errorClass, validClass) {
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                                } else {
                                    $(element).closest('.help-block').removeClass('valid');
                                    // display OK icon
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                                    // add the Bootstrap error class to the control group
                                }
                            },
                            unhighlight: function (element, errorClass, validClass) {
                                // revert the change done by hightlight
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                                } else {
                                    $(element).closest('.form-group').removeClass('has-error');
                                    // set error class to the control group
                                }
                            },
                            success: function (label, element) {
                                label.addClass('help-block valid');
                                // mark the current input as valid and display OK icon
                                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                            }
                        });
                        $('body').on('hidden.bs.modal', '.modal', function () {
                            $('.fileinput').fileinput('clear');
                            addStaffValidator.resetForm();
                            resetStaffValidator.resetForm();
                            $('.has-error').removeClass('has-error');
                            $('.myErrorClass').removeClass('myErrorClass');
                            $('.has-success').removeClass('has-success');
                            $('.form-group').find(".symbol.ok").removeClass('ok').addClass('required');
                            $(this).data('bs.modal', null);
                        });
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            }else $('body').removeClass('modal-open');
                        });
                        $('.datetimepicker.birthday').datetimepicker({
                            format: "DD/MM/YYYY",
                            locale: 'vi',
                            showClear : true,
                            viewMode: 'years'
                        });

                        $('#staff-start-work').datetimepicker({
                            format: "DD/MM/YYYY",
                            locale: 'vi',
                            showClear : true
                        });

                        $(".staff-select-store").select2({
                            //multiple: true,
                            placeholder: "Tên cửa hàng, địa chỉ",
                            minimumInputLength: 2,
                            maximumSelectionSize: 0,
                            // instead of writing the function to execute the request we use Select2's convenient helper
                            ajax: {
                                url: appConfig.api_path + "store/getStores",
                                dataType: "json",
                                data: function (term) {
                                    return {
                                        // search term
                                        search: term
                                    };
                                },
                                results: function (data, page) {
                                    // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                                    return {results: data.items};
                                },
                                cache: true
                            },
                            initSelection: function (element, callback) {
                                // the input tag has a value attribute preloaded that points to a preselected repository's id
                                // this function resolves that id attribute to an object that select2 can render
                                // using its formatResult renderer - that way the repository name is shown preselected
                                var id = $(element).val();
                                if (id !== "") {
                                    $.ajax(appConfig.api_path + "store/getStoreSelected", {
                                        dataType: "json",
                                        method: 'get',
                                        data: {id: id},
                                        cache: true
                                    }).done(function (data) {
                                        callback(data.items);
                                    });
                                }
                            },
                            formatResult: repoFormatResultStore,
                            formatSelection: repoFormatSelectionStore
                        });

                        function repoFormatResultStore(repo) {
                            var markup = "<div class='select2-result-repository clearfix'>" +
                                "<div class='select2-result-repository__title'>" + repo.name + "</div>";

                            markup += "<div class='select2-result-repository__description'>" + repo.address + "</div>";
                            markup += "</div>";
                            return markup;
                        }

                        function repoFormatSelectionStore(repo) {
                            return repo.name;
                        }

                        $('.table').on('click','.delete-row', function (e) {
                            var id = $(this).attr('delete-id');
                            $.smallBox({
                                title: "Cảnh báo!",
                                content: "Bạn chắc chắn muốn xóa không? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#staff_ctrl\")).scope().deleteStaff("+id+");' class='btn btn-danger btn-sm'>Có</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>Không</a></p>",
                                color: "#C46A69",
                                //timeout: 8000,
                                icon: "fa fa-exclamation-triangle swing animated"
                            });
                        });

                        $('#avatar-image').cropImage({
                            handleCompletedFile: handUploadAvatarImage,
                            inputId: 'cropDealImage',
                            modalId: 'changeImageModal',
                            width:200,
                            height:200
                        });
                        function handUploadAvatarImage( fileData){
                            angular.element($('#staff_ctrl')).scope().currentStaff.avatar = fileData;
                            angular.element($('#staff_ctrl')).scope().$apply();
                        }
                    },2000);
                });
            }
        }
    })
});
