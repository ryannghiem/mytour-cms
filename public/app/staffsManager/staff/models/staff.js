
define(['staffsManager/module'], function (module) {

    'use strict';

    return module.registerFactory('staffsList', function ($http, $q) {
        var dfd = $q.defer();

        var staffsList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'staff/list',
            method: "GET",
            params: {
                orderBy: 'staffs.lastname',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            staffsList.data.data = response.data.data;
            staffsList.data.currentPage = 1;
            staffsList.data.itemsPerPage = 10;
            staffsList.data.orderBy = 'staffs.lastname';
            staffsList.data.orderDir = 'asc';
            staffsList.data.staffSearchValue = '';
            staffsList.data.totalStaffs = response.data.recordsFiltered;
            dfd.resolve(staffsList)
        });

        return staffsList;
    });
});
