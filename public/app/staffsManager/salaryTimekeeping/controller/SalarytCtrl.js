
define(['staffsManager/module'], function (module) {

    'use strict';

    module.registerController('SalarytCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.getStores = function () {
            $http({
                url: appConfig.api_path + 'store/getStores',
                method: "GET",
                params: {search : ''}
            }).then(function (response) {
                $scope.stores = response.data.items;
                if ($scope.stores.length == 1) $scope.filterInvoicesByStore = $scope.stores[0].id;
            });
        };

        $scope.getStores();

        $scope.checkFilterDate = function () {
            if ($('#timekeepingStartDate').val()) {
                $scope.filterTimekeepingByStartDate = moment($('#timekeepingStartDate').val(), 'DD/MM/YYYY, HH:mm').format('YYYY-MM-DD, HH:mm');
                $('#timekeepingStartDate').parents('.form-group').removeClass('has-error');
            } else {
                $('#timekeepingStartDate').parents('.form-group').addClass('has-error');
                return false;
            }
            if ($('#timekeepingEndDate').val()) {
                $scope.filterTimekeepingByEndDate = moment($('#timekeepingEndDate').val(), 'DD/MM/YYYY, HH:mm').format('YYYY-MM-DD, HH:mm');
                $('#timekeepingEndDate').parents('.form-group').removeClass('has-error');
            } else {
                $('#timekeepingEndDate').parents('.form-group').addClass('has-error');
                return false;
            }
            return true;
        };
        $scope.filterTimekeepingByStatus = 1;

        $scope.generateData = function () {
            $scope.salarytList.timekeepings = [];
            for (var i = 0; i < $scope.salarytList.data.length; i++){
                $scope.salarytList.timekeepings[i] = [];
                for (var j = 0; j < $scope.salarytList.days.length; j++) {
                    if ($scope.timekeepings.length) {
                        for (var k = 0; k < $scope.timekeepings.length; k++){
                            var $icheck = false;
                            if ($scope.salarytList.data[i].id ==  $scope.timekeepings[k].user_id && $scope.salarytList.days[j].work_day == $scope.timekeepings[k].work_day) {
                                $icheck = true;
                                $scope.salarytList.timekeepings[i][j] = $scope.timekeepings[k];
                                break;
                            }
                            if (!$icheck) $scope.salarytList.timekeepings[i][j] = {};
                        }
                    }else $scope.salarytList.timekeepings[i][j] = {};
                }
            }
            $('[data-toggle="tooltip"]').tooltip();
        };

        $scope.clearFilterSalaryts = function () {
            if (!$scope.checkFilterDate()) return;
            $('.btn-staff-refresh i').addClass('fa-spin');
            $('.salaryt-article .tables_processing').show();
            $scope.filterTimekeepingByStatus = "";
            $http({
                url: appConfig.api_path + 'salary/timekeepingList',
                method: "GET",
                params: {
                    orderBy: 'us.lastname',
                    orderDir: 'asc',
                    start: 0,
                    length: $scope.salarytList.itemsPerPage,
                    search: '',
                    status: $scope.filterTimekeepingByStatus,
                    start_date : $scope.filterTimekeepingByStartDate,
                    end_date : $scope.filterTimekeepingByEndDate
                }
            }).then(function (response) {
                $('.salaryt-article .tables_processing').hide();
                $('.btn-staff-refresh i').removeClass('fa-spin');
                $scope.salarytList.data = response.data.data;
                $scope.salarytList.days = response.data.days;
                $scope.timekeepings = response.data.timekeepings;
                $scope.salarytList.currentPage = 1;
                $scope.salarytList.orderBy = 'us.lastname';
                $scope.salarytList.salarytSearchValue = '';
                $scope.salarytList.orderDir = 'asc';
                $scope.salarytList.totalSalaryt = response.data.recordsFiltered;
                $scope.generateData();
            });
        };

        var canceler = $q.defer();
        $scope.filterSalaryt = function () {
            if (!$scope.checkFilterDate()) return;
            $('.salaryt-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'salary/timekeepingList',
                method: "GET",
                params: {
                    orderBy: $scope.salarytList.orderBy,
                    orderDir: $scope.salarytList.orderDir,
                    start: 0,
                    length: $scope.salarytList.itemsPerPage,
                    search: $scope.salarytList.salarytSearchValue,
                    status: $scope.filterTimekeepingByStatus,
                    start_date : $scope.filterTimekeepingByStartDate,
                    end_date : $scope.filterTimekeepingByEndDate
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.salaryt-article .tables_processing').hide();
                $scope.salarytList.data = response.data.data;
                $scope.salarytList.days = response.data.days;
                $scope.timekeepings = response.data.timekeepings;
                $scope.salarytList.currentPage = 1;
                $scope.salarytList.totalSalaryt = response.data.recordsFiltered;
                $scope.generateData();
            });
        };

        $scope.setPageSalaryt = function (page) {
            if (!$scope.checkFilterDate()) return;
            $('.salaryt-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/timekeepingList',
                method: "GET",
                params: {
                    orderBy: $scope.salarytList.orderBy,
                    orderDir: $scope.salarytList.orderDir,
                    start: (page - 1) * $scope.salarytList.itemsPerPage,
                    length: $scope.salarytList.itemsPerPage,
                    search: $scope.salarytList.salarytSearchValue,
                    status: $scope.filterTimekeepingByStatus,
                    start_date : $scope.filterTimekeepingByStartDate,
                    end_date : $scope.filterTimekeepingByEndDate
                }
            }).then(function (response) {
                $('.salaryt-article .tables_processing').hide();
                $scope.salarytList.data = response.data.data;
                $scope.salarytList.days = response.data.days;
                $scope.timekeepings = response.data.timekeepings;
                $scope.salarytList.currentPage = page;
            });
        };

        $scope.sortSalaryt = function (colName) {
            if (!$scope.checkFilterDate()) return;
            $('.salaryt-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/timekeepingList',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.salarytList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.salarytList.currentPage - 1) * $scope.salarytList.itemsPerPage,
                    length: $scope.salarytList.itemsPerPage,
                    search: $scope.salarytList.salarytSearchValue,
                    status: $scope.filterTimekeepingByStatus,
                    start_date : $scope.filterTimekeepingByStartDate,
                    end_date : $scope.filterTimekeepingByEndDate
                }
            }).then(function (response) {
                $('.salaryt-article .tables_processing').hide();
                $scope.salarytList.data = response.data.data;
                $scope.salarytList.days = response.data.days;
                $scope.timekeepings = response.data.timekeepings;
                $scope.salarytList.orderBy = colName;
                $scope.salarytList.orderDir = $scope.salarytList.orderDir == 'asc' ? 'desc' : 'asc';
                $scope.generateData();
            });
        };

        $scope.addSalaryt = function ($user, $aday) {
            $scope.currentSalaryt = {};
            $scope.currentSalaryt.work_value = 0;
            $scope.currentSalaryt.permission = 1;
            $scope.currentSalaryt.user_id = $user.id;
            $scope.currentSalaryt.fullname = $user.fullname;
            $scope.currentSalaryt.work_day = $aday.work_day;
            $('#modal-add-salaryt').modal('show');
        };

        $scope.editSalaryt = function ($timekeeping) {
            $scope.currentSalaryt = {};
            var $id = $timekeeping.id;
            $http({
                url: appConfig.api_path + 'salary/timekeepingList/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentSalaryt = response.data.data;
                $scope.currentSalaryt.fullname = $scope.currentSalaryt.staff.fullname;
            });
            $('#modal-add-salaryt').modal('show');
        };

        // Save change
        $scope.saveSalaryt = function () {
            if ($rootScope.userLogin.user_type < 2) return;
            $('#modal-add-salaryt').modal('hide');
            $('.salaryt-article .tables_processing').show();
            if ($scope.currentSalaryt.permission < 2) $scope.currentSalaryt.work_value = 0;
            $http.post(appConfig.api_path + 'salary/timekeepingList/update',$scope.currentSalaryt
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterSalaryt();
                    }
                });
        };
        
        //checkTimekeeping
        $scope.workedTimekeeping = function ($salaryt, $aday) {
            if ($rootScope.userLogin.user_type < 2) return;
            $('.salaryt-article .tables_processing').show();
            var $timekeeping = {
                user_id : $salaryt.id,
                work_day : $aday.work_day,
                work_value : 1,
                permission : 2
            };
            $http.post(appConfig.api_path + 'salary/timekeepingList/update',$timekeeping
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterSalaryt();
                }
            });
        };

        $scope.checkAllTimekeeping = function ($index, $staff) {
            if ($rootScope.userLogin.user_type < 2) return;
            $('.salaryt-article .tables_processing').show();
            var $timekeepings = [];
            for (var i = 0; i < $scope.salarytList.timekeepings[$index].length; i++) {
                if (!$scope.salarytList.timekeepings[$index][i].permission && $scope.salarytList.timekeepings[$index][i].permission != 0) {
                    var $timekeeping = {
                        user_id : $staff.id,
                        work_day : $scope.salarytList.days[i].work_day,
                        work_value : 1,
                        permission : 2
                    };
                    $timekeepings.push($timekeeping);
                }                    
            }
            $http.post(appConfig.api_path + 'salary/timekeepingList/update-worked-list',$timekeepings
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterSalaryt();
                }
            });
        };

    });
});

