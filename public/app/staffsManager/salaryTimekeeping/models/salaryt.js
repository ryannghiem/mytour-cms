
define(['staffsManager/module'], function (module) {

    'use strict';

    return module.registerFactory('salarytList', function ($http, $q) {
        var dfd = $q.defer();

        var salarytList = {
            initialized: dfd.promise,
            data: {}
        };
        salarytList.data.data = [];
        salarytList.data.currentPage = 1;
        salarytList.data.itemsPerPage = 10;
        salarytList.data.orderBy = 'us.lastname';
        salarytList.data.orderDir = 'asc';
        salarytList.data.salarytSearchValue = '';
        salarytList.data.totalSalaryc = 0;
        dfd.resolve(salarytList);

        return salarytList;
    });
});
