
define(['staffsManager/module'], function (module) {

    'use strict';

    return module.registerFactory('salaryhList', function ($http, $q) {
        var dfd = $q.defer();

        var salaryhList = {
            initialized: dfd.promise,
            data: {}
        };
        salaryhList.data.data = [];
        salaryhList.data.currentPage = 1;
        salaryhList.data.itemsPerPage = 10;
        salaryhList.data.orderBy = 's.pay_at_end';
        salaryhList.data.orderDir = 'desc';
        salaryhList.data.salaryhSearchValue = '';
        salaryhList.data.totalSalaryh = 0;
        dfd.resolve(salaryhList);

        return salaryhList;
    });
});
