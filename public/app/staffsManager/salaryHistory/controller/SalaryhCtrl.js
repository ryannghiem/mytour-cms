
define(['staffsManager/module'], function (module) {

    'use strict';

    module.registerController('SalaryhCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.getStaffs = function () {
            if ($rootScope.userLogin.user_type > 1)
                $http({
                    url: appConfig.api_path + 'staff/all-staffs',
                    method: "GET"
                }).then(function (response) {
                    $scope.allStaffs = response.data;//
                    $scope.staffs = $scope.allStaffs;
                });
            else $scope.staffs = [{id : $rootScope.userLogin.id, fullname: $rootScope.userLogin.fullname}];
        };

        $scope.getStaffs();

        $scope.getSalaries = function () {
            $http({
                url: appConfig.api_path + 'salary/configList/all',
                method: "GET"
            }).then(function (response) {
                $scope.salaries = response.data;//
            });
        };

        $scope.getSalaries();

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.getStores = function () {
            $http({
                url: appConfig.api_path + 'store/getStores',
                method: "GET",
                params: {search : ''}
            }).then(function (response) {
                $scope.stores = response.data.items;
                if ($scope.stores.length == 1) $scope.filterInvoicesByStore = $scope.stores[0].id;
            });
        };

        $scope.getStores();

        $scope.clearFilterSalaryhs = function () {
            $('.btn-staff-refresh i').addClass('fa-spin');
            $('.salaryh-article .tables_processing').show();
            $scope.filterSalaryhByStaff = '';
            $('#filterSalaryhByDate').data("DateTimePicker").clear();
            if ($('#filterSalaryhByDate').val()) {
                $scope.filterSalaryhByDate = moment('28/' + $('.filterSalaryhByDate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.filterSalaryhByDate = null;
            $http({
                url: appConfig.api_path + 'salary/historyList',
                method: "GET",
                params: {
                    orderBy: 's.pay_at_end',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.salaryhList.itemsPerPage,
                    search: '',
                    salary_month : $scope.filterSalaryhByDate,
                    staff_id : $scope.filterSalaryhByStaff
                }
            }).then(function (response) {
                $('.salaryh-article .tables_processing').hide();
                $('.btn-staff-refresh i').removeClass('fa-spin');
                $scope.salaryhList.data = response.data.data;
                $scope.salaryhList.currentPage = 1;
                $scope.salaryhList.orderBy = 's.pay_at_end';
                $scope.salaryhList.salaryhSearchValue = '';
                $scope.salaryhList.orderDir = 'desc';
                $scope.salaryhList.totalSalaryh = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterSalaryh = function () {
            $('.salaryh-article .tables_processing').show();
            if ($('#filterSalaryhByDate').val()) {
                $scope.filterSalaryhByDate = moment('28/' + $('.filterSalaryhByDate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.filterSalaryhByDate = null;
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'salary/historyList',
                method: "GET",
                params: {
                    orderBy: $scope.salaryhList.orderBy,
                    orderDir: $scope.salaryhList.orderDir,
                    start: 0,
                    length: $scope.salaryhList.itemsPerPage,
                    search: $scope.salaryhList.salaryhSearchValue,
                    salary_month : $scope.filterSalaryhByDate,
                    staff_id : $scope.filterSalaryhByStaff
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.salaryh-article .tables_processing').hide();
                $scope.salaryhList.data = response.data.data;
                $scope.salaryhList.currentPage = 1;
                $scope.salaryhList.totalSalaryh = response.data.recordsFiltered;
            });
        };

        $scope.setPageSalaryh = function (page) {
            $('.salaryh-article .tables_processing').show();
            if ($('#filterSalaryhByDate').val()) {
                $scope.filterSalaryhByDate = moment($('.filterSalaryhByDate').val(), 'DD/MM/YYYY, HH:mm').format('YYYY-MM-DD, HH:mm');
            } else $scope.filterSalaryhByDate = null;
            $http({
                url: appConfig.api_path + 'salary/historyList',
                method: "GET",
                params: {
                    orderBy: $scope.salaryhList.orderBy,
                    orderDir: $scope.salaryhList.orderDir,
                    start: (page - 1) * $scope.salaryhList.itemsPerPage,
                    length: $scope.salaryhList.itemsPerPage,
                    search: $scope.salaryhList.salaryhSearchValue,
                    salary_month : $scope.filterSalaryhByDate,
                    staff_id : $scope.filterSalaryhByStaff
                }
            }).then(function (response) {
                $('.salaryh-article .tables_processing').hide();
                $scope.salaryhList.data = response.data.data;
                $scope.salaryhList.currentPage = page;
            });
        };

        $scope.sortSalaryh = function (colName) {
            $('.salaryh-article .tables_processing').show();
            if ($('#filterSalaryhByDate').val()) {
                $scope.filterSalaryhByDate = moment($('.filterSalaryhByDate').val(), 'DD/MM/YYYY, HH:mm').format('YYYY-MM-DD, HH:mm');
            } else $scope.filterSalaryhByDate = null;
            $http({
                url: appConfig.api_path + 'salary/historyList',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.salaryhList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.salaryhList.currentPage - 1) * $scope.salaryhList.itemsPerPage,
                    length: $scope.salaryhList.itemsPerPage,
                    search: $scope.salaryhList.salaryhSearchValue,
                    salary_month : $scope.filterSalaryhByDate,
                    staff_id : $scope.filterSalaryhByStaff
                }
            }).then(function (response) {
                $('.salaryh-article .tables_processing').hide();
                $scope.salaryhList.data = response.data.data;
                $scope.salaryhList.orderBy = colName;
                $scope.salaryhList.orderDir = $scope.salaryhList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.addSalaryh = function () {
            $scope.currentSalaryh = {};
            $scope.currentSalaryh.addMode = true;
            $scope.currentSalaryh.status = 1;
            $scope.currentSalaryh.monthly_salary = 0;
            $scope.currentSalaryh.daily_salary = 0;
            $scope.currentSalaryh.salary_type = 2;
            $scope.currentSalaryh.total_salary = 0;
            $('#salaryhByMonth').data("DateTimePicker").clear();
            $('#salaryhStartDate').data("DateTimePicker").clear();
            $('#salaryhEndDate').data("DateTimePicker").clear();
            $('#modal-add-salaryh').modal('show');
        };

        $scope.editSalaryh = function (index, salary) {
            $scope.currentSalaryh = {};
            $scope.currentSalaryh.addMode = false;
            var $id = salary.id;
            $http({
                url: appConfig.api_path + 'salary/historyList/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentSalaryh = response.data.data;
                if ($scope.currentSalaryh.salary_type == 1) {
                    if ($scope.currentSalaryh.pay_at_start) {
                        $scope.currentSalaryh.pay_at_start = moment($scope.currentSalaryh.pay_at_start).format('DD/MM/YYYY');
                    }
                    if ($scope.currentSalaryh.pay_at_end) {
                        $scope.currentSalaryh.pay_at_end = moment($scope.currentSalaryh.pay_at_end).format('DD/MM/YYYY');
                    }
                }else if($scope.currentSalaryh.salary_type == 2) {
                    if ($scope.currentSalaryh.pay_at_end) {
                        $scope.currentSalaryh.pay_at_month = moment($scope.currentSalaryh.pay_at_end).format('MM/YYYY');
                    }
                }
            });
            $('#modal-add-salaryh').modal('show');
        };
        
        $scope.getDatePayAt = function () {
            if ($scope.currentSalaryh.salary_type == 2) {
                if ($('#salaryhByMonth').val()) {
                    $scope.pay_at_end = moment($scope.currentSalaryh.days_in_month+'/' + $('#salaryhByMonth').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                    $scope.pay_at_start = moment('01/' + $('#salaryhByMonth').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                }
                else return false;
            }
            if ($scope.currentSalaryh.salary_type == 1){
                if ($('#salaryhStartDate').val()) {
                    $scope.pay_at_start = moment($('#salaryhStartDate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                } else return false;
                if ($('#salaryhEndDate').val()) {
                    $scope.pay_at_end = moment($('#salaryhEndDate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                } else return false;
            }
            return true;
        };
        // Save change
        $scope.saveSalaryh = function () {
            var formValidation = $('.addSalaryhForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if (!$scope.getDatePayAt()) return;
            $('#modal-add-salaryh').modal('hide');
            $('.salaryh-article .tables_processing').show();
            var $currentSalaryh = angular.copy($scope.currentSalaryh);
            $currentSalaryh.daily_salary = parseInt($currentSalaryh.daily_salary);
            $currentSalaryh.pay_at_start = $scope.pay_at_start;
            $currentSalaryh.pay_at_end = $scope.pay_at_end;
            $http.post(appConfig.api_path + 'salary/historyList/update',$currentSalaryh
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterSalaryh();
                    }
                });
        };

        $scope.deleteSalaryh = function (id) {
            $('.salaryh-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'salary/historyList/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.salaryhList.orderBy,
                    orderDir: $scope.salaryhList.orderDir,
                    start: ($scope.salaryhList.currentPage - 1) * $scope.salaryhList.itemsPerPage,
                    length: $scope.salaryhList.itemsPerPage,
                    search: $scope.salaryhList.salaryhSearchValue,
                    salary_month : $scope.filterSalaryhByDate,
                    staff_id : $scope.filterSalaryhByStaff
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.salaryh-article .tables_processing').text('Xoá thành công!');
                    $scope.filterSalaryh();
                } else $('.salaryh-article .tables_processing').text("Không thể xóa mức lương này!");
                setTimeout(function () {
                    $('.salaryh-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.fillDataSalary = function () {
            $scope.currentSalaryh.salary_type = $scope.currentSalaryConfig.salary_type;
            $scope.currentSalaryh.daily_salary = $scope.currentSalaryConfig.daily_salary;
            $scope.currentSalaryh.monthly_salary = $scope.currentSalaryConfig.monthly_salary;
            $scope.currentSalaryh.allowance = $scope.currentSalaryConfig.allowance;
            $scope.currentSalaryh.deduction = $scope.currentSalaryConfig.deduction;
            $scope.currentSalaryh.is_pay_debt = $scope.currentSalaryConfig.is_pay_debt;
        };

        $scope.calculatorTimekeeping = function () {
            if (!$scope.currentSalaryh.user_id) return;
            
            if ($scope.currentSalaryh.salary_type == 2) {
                if ($('#salaryhByMonth').val()) {
                    $scope.calculateWorkedSalaryMonth();
                } else return;
            }else $scope.filterTimekeepingByMonth = null;
            
            if ($scope.currentSalaryh.salary_type == 1) {
                if ($('#salaryhStartDate').val()) {
                    $scope.filterTimekeepingByStartDate = moment($('#salaryhStartDate').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                } else return;
                if ($('#salaryhEndDate').val()) {
                    $scope.filterTimekeepingByEndDate = moment($('#salaryhEndDate').val() + ', 23:59:59', 'DD/MM/YYYY, HH:mm:ss').format('YYYY-MM-DD, HH:mm');
                } else return;
            }else {
                $scope.filterTimekeepingByStartDate = null;
                $scope.filterTimekeepingByEndDate = null;
            }
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'salary/timekeepingList/get-timekeeping',
                method: "GET",
                params: {
                    user_id: $scope.currentSalaryh.user_id,
                    start_date: $scope.filterTimekeepingByStartDate,
                    end_date: $scope.filterTimekeepingByEndDate,
                    salary_month : $scope.filterTimekeepingByMonth
                },
                timeout: canceler.promise
            }).then(function (response) {
                var $data = response.data.data;
                $scope.currentSalaryh.worked_days = $data.worked_days;
                $scope.calculateWorkedSalary();                
            });
        };

        $scope.calculateWorkedSalaryMonth = function () {
            if ($('#salaryhByMonth').val()) {
                $scope.currentSalaryh.days_in_month = moment($('#salaryhByMonth').val(), "MM/YYYY").daysInMonth();
                $scope.filterTimekeepingByMonth = moment($scope.currentSalaryh.days_in_month + '/' + $('#salaryhByMonth').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
                $scope.currentSalaryh.daily_salary = $scope.currentSalaryh.monthly_salary / $scope.currentSalaryh.days_in_month;
                $scope.calculateWorkedSalary();
            }
        };
        
        $scope.calculateWorkedSalary = function () {
            $scope.currentSalaryh.work_salary =  parseInt($scope.currentSalaryh.daily_salary * $scope.currentSalaryh.worked_days);
            $scope.calculateTotalSalary();
        };

        $scope.calculateTotalSalary = function () {
            var $work_salary = $scope.currentSalaryh.work_salary ? parseInt($scope.currentSalaryh.work_salary) : 0;
            var $allowance = $scope.currentSalaryh.allowance ? parseInt($scope.currentSalaryh.allowance) : 0;
            var $deduction = $scope.currentSalaryh.deduction ? parseInt($scope.currentSalaryh.deduction) : 0;

            var $pay_debt = $scope.currentSalaryh.pay_debt ? parseInt($scope.currentSalaryh.pay_debt) : 0;
            var $pay_lack = $scope.currentSalaryh.pay_lack ? parseInt($scope.currentSalaryh.pay_lack) : 0;
            var $pay_customer_debt = $scope.currentSalaryh.pay_customer_debt ? parseInt($scope.currentSalaryh.pay_customer_debt) : 0;
            var $pay_wrong_card = $scope.currentSalaryh.pay_wrong_card ? parseInt($scope.currentSalaryh.pay_wrong_card) : 0;
            $scope.currentSalaryh.total_salary =  $work_salary + $allowance - $deduction - $pay_debt + $pay_lack - $pay_customer_debt - $pay_wrong_card;
        };

        $scope.getDebtSalaryh = function ($event, $debt_type) {
            var $checkbox = $event.target;
            if (!$checkbox.checked) {
                switch($debt_type) {
                    case 'total_remaining':
                        $scope.currentSalaryh.pay_debt = 0;
                        break;
                    case 'total_lack':
                        $scope.currentSalaryh.pay_lack = 0;
                        break;
                    case 'total_customer_debt':
                        $scope.currentSalaryh.pay_customer_debt = 0;
                        break;
                    default:
                        $scope.currentSalaryh.pay_wrong_card = 0;
                }
                $scope.calculateTotalSalary();
                return;
            }
            if (!$scope.getDatePayAt()) return;
            $http({
                url: appConfig.api_path + 'salary/historyList/getDebt',
                method: "GET",
                params: {
                    user_id: $scope.currentSalaryh.user_id,
                    start_date: $scope.pay_at_start,
                    end_date: $scope.pay_at_end,
                    debt_type : $debt_type
                }
            }).then(function (response) {
                var $result = response.data.data;
                switch($debt_type) {
                    case 'total_remaining':
                        $scope.currentSalaryh.pay_debt = Math.abs($result.total_remaining);
                        break;
                    case 'total_lack':
                        $scope.currentSalaryh.pay_lack = parseInt($result.total_lack);
                        break;
                    case 'total_customer_debt':
                        $scope.currentSalaryh.pay_customer_debt = Math.abs($result.total_customer_debt);
                        break;
                    default:
                        $scope.currentSalaryh.pay_wrong_card = Math.abs($result.total_wrong_card);
                }
                $scope.calculateTotalSalary();
            });
        };

        $scope.getCanvas = null;

        $scope.exportPNG = function () {
            var element = $("#salary-pdf-body"); // global variable
            $('#modal-add-salaryh').addClass("html2canvasreset");
            element.addClass("html2canvasreset");
            $('.export-elements').show();
            html2canvas(element, {
                height: 3000,
                onrendered: function (canvas) {
                    $("#modal-export-salaryh .modal-body").empty().append(canvas);
                    $('#modal-add-salaryh').removeClass("html2canvasreset");
                    element.removeClass("html2canvasreset");
                    $scope.getCanvas = canvas;
                    $('#store-name-export').text($("#store-selected option:selected").text());
                    $('.export-elements').hide();
                    $('#modal-export-salaryh').modal('show');
                }
            });
        };

        $scope.downloadPNG = function () {
            var $userName = $("#currentSalaryhUser option:selected").text();
            var $paidAt = '';
            if ($scope.currentSalaryh.salary_type == 1) {
                if ($scope.currentSalaryh.pay_at_start) {
                    $paidAt = $scope.currentSalaryh.pay_at_start;
                }
                if ($scope.currentSalaryh.pay_at_end) {
                    $paidAt += '_' + $scope.currentSalaryh.pay_at_end;
                }
            }else if($scope.currentSalaryh.salary_type == 2) {
                if ($scope.currentSalaryh.pay_at_end) {
                    $paidAt = $scope.currentSalaryh.pay_at_month;
                }
            }
            var imgageData = $scope.getCanvas.toDataURL("image/png");
            // Now browser starts downloading it instead of just showing it
            var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            $("#btn-Convert-Html2Image").attr("download", '('+$paidAt+')__'+$userName+".png").attr("href", newData);
        }
    });
});
