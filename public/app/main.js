// Defer AngularJS bootstrap
window.name = "NG_DEFER_BOOTSTRAP!";

define([
    'require',
    'jquery',
    'angular',
    'domReady',

    //'pace',
    'bootstrap',
    'appConfig',
    'app',
    'includes',
    'datetimepicker',
    'crop-image'
], function (require, $, ng, domReady) {
    'use strict';

    $.sound_path = appConfig.sound_path;
    $.sound_on = appConfig.sound_on;
    domReady(function (document) {
        ng.bootstrap(document, ['app']);
        ng.resumeBootstrap();
        $.validator.setDefaults({
            errorElement: "span", // contain the error msg in a small tag
            errorClass: 'help-block myErrorClass',
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                } else if (element.hasClass("ckeditor") || element.hasClass("date-required") || element.parent().hasClass('input-group')) {
                    error.appendTo($(element).closest('.form-group'));
                } else {
                    error.insertAfter(element);
                    // for other inputs, just perform default behavior
                }
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                } else {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                // revert the change done by hightlight
                var elem = $(element);
                if (elem.hasClass("select2-offscreen")) {
                    $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                    $(element).closest('.form-group').removeClass('has-error');
                } else {
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                }
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                // mark the current input as valid and display OK icon
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            },
            focusInvalid: false,
            invalidHandler: function (form, validator) {
                if (!validator.numberOfInvalids())
                    return;
                if ($(validator.errorList[0].element).parents('.modal').length) {
                    $('.modal').animate({
                        scrollTop: $(validator.errorList[0].element).parents('.form-group').last().position().top + parseFloat($(validator.errorList[0].element).parents(".modal-body").position().top)
                    }, 1000);
                }
            }
        });
        setTimeout(function () {
            if (!Array.isArray) {
                Array.isArray = function(arg) {
                    return Object.prototype.toString.call(arg) === '[object Array]';
                };
            }
            $.fn.modal.Constructor.prototype.enforceFocus = function() {
                var modal_this = this;
                $(document).on('focusin.modal', function (e) {
                    if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                        && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select')
                        && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')
                        && $(e.target.parentNode).hasClass('cke_contents cke_reset')
                        && $(e.target.parentNode).hasClass('cke_source')) {
                        modal_this.$element[0].focus();
                    }
                })
            };
            $(document).on('show.bs.modal', '.modal', function () {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });
            $(document).on('hidden.bs.modal', '.modal', function () {
                $('.modal:visible').length && $(document.body).addClass('modal-open');
            });
            $('[data-toggle="tooltip"]').tooltip();
            // $.extend( $.validator.messages, {
            //     required: "Hãy nhập.",
            //     remote: "Hãy sửa cho đúng.",
            //     email: "Hãy nhập email.",
            //     url: "Hãy nhập URL.",
            //     date: "Hãy nhập ngày.",
            //     dateISO: "Hãy nhập ngày (ISO).",
            //     number: "Hãy nhập số.",
            //     digits: "Hãy nhập chữ số.",
            //     creditcard: "Hãy nhập số thẻ tín dụng.",
            //     equalTo: "Hãy nhập thêm lần nữa.",
            //     extension: "Phần mở rộng không đúng.",
            //     maxlength: $.validator.format( "Hãy nhập từ {0} kí tự trở xuống." ),
            //     minlength: $.validator.format( "Hãy nhập từ {0} kí tự trở lên." ),
            //     rangelength: $.validator.format( "Hãy nhập từ {0} đến {1} kí tự." ),
            //     range: $.validator.format( "Hãy nhập từ {0} đến {1}." ),
            //     max: $.validator.format( "Hãy nhập từ {0} trở xuống." ),
            //     min: $.validator.format( "Hãy nhập từ {1} trở lên." )
            // } );

            $('#carousel-example-generic').carousel({
                interval: 5000
            });
            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name]) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        },1000);
    });
});
