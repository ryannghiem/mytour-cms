define(['bookingsManagement/module'], function (module) {

    "use strict";

    return module.registerDirective('bookingsList', function (bookingsList) {
        return {
            restrict: 'A',
            templateUrl: 'app/bookingsManagement/views/bookings.tpl.html',
            link: function (scope) {
                bookingsList.initialized.then(function () {
                    scope.bookingsList = bookingsList.data;
                    setTimeout(function () {
                        var addBookingValidator = $(".addBookingForm").validate();
                        $('body').on('hidden.bs.modal', '.modal', function () {
                            $('.fileinput').fileinput('clear');
                            addBookingValidator.resetForm();
                            $('.has-error').removeClass('has-error');
                            $('.myErrorClass').removeClass('myErrorClass');
                            $('.has-success').removeClass('has-success');
                            $('.form-group').find(".symbol.ok").removeClass('ok').addClass('required');
                            $(this).data('bs.modal', null);
                        });
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if ($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            } else $('body').removeClass('modal-open');
                        });

                        $('#booking-start-work').datetimepicker({
                            format: "DD/MM/YYYY",
                            locale: 'vi',
                            showClear: true
                        });

                        $(".booking-select-hotel").select2({
                            //multiple: true,
                            placeholder: "Hotel name, address",
                            minimumInputLength: 2,
                            maximumSelectionSize: 0,
                            // instead of writing the function to execute the request we use Select2's convenient helper
                            ajax: {
                                url: appConfig.api_path + "hotel/getHotels",
                                dataType: "json",
                                data: function (term) {
                                    return {
                                        // search term
                                        search: term
                                    };
                                },
                                results: function (data, page) {
                                    // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                                    return {results: data.items};
                                },
                                cache: true
                            },
                            initSelection: function (element, callback) {
                                // the input tag has a value attribute preloaded that points to a preselected repository's id
                                // this function resolves that id attribute to an object that select2 can render
                                // using its formatResult renderer - that way the repository name is shown preselected
                                var id = $(element).val();
                                if (id !== "") {
                                    $.ajax(appConfig.api_path + "hotel/getHotelSelected", {
                                        dataType: "json",
                                        method: 'get',
                                        data: {id: id},
                                        cache: true
                                    }).done(function (data) {
                                        callback(data.items);
                                    });
                                }
                            },
                            formatResult: repoFormatResultHotel,
                            formatSelection: repoFormatSelectionHotel
                        }).on('change', function (e) {
                            if ($(".booking-select-room").val())
                                $(".booking-select-room").val(null);
                        });

                        $(".booking-select-room").select2({
                            placeholder: "Room name",
                            // instead of writing the function to execute the request we use Select2's convenient helper
                            ajax: {
                                url: appConfig.api_path + "room/getRooms",
                                dataType: "json",
                                data: function (term) {
                                    return {
                                        // search term
                                        search: term,
                                        hotel_id: scope.currentBooking.hotel_id
                                    };
                                },
                                results: function (data, page) {
                                    // parse the results into the format expected by Select2.
                                    // since we are using custom formatting functions we do not need to alter the remote JSON data
                                    return {results: data.items};
                                },
                                cache: true
                            },
                            initSelection: function (element, callback) {
                                // the input tag has a value attribute preloaded that points to a preselected repository's id
                                // this function resolves that id attribute to an object that select2 can render
                                // using its formatResult renderer - that way the repository name is shown preselected
                                var id = $(element).val();
                                if (id !== "") {
                                    $.ajax(appConfig.api_path + "room/getRoomSelected", {
                                        dataType: "json",
                                        method: 'get',
                                        data: {id: id},
                                        cache: true
                                    }).done(function (data) {
                                        callback(data.items);
                                    });
                                }
                            },
                            formatResult: repoFormatResultRoom,
                            formatSelection: repoFormatSelectionRoom
                        });

                        function repoFormatResultRoom(repo) {
                            var markup = "<div class='select2-result-repository clearfix'>" +
                                "<div class='select2-result-repository__title'>" + repo.name + "</div>";
                            var cont = '';
                            if (repo.has_discount) {
                                cont += "<strike>" + formatNumber(repo.price,'.',',') + "đ</strike>"
                                    + " Giảm còn: " + formatNumber(repo.discount_price,'.',',') + "đ";
                            }else cont = formatNumber(repo.price,'.',',') + "đ";
                            markup += "<div class='select2-result-repository__description'>" + cont + "</div>";
                            markup += "</div>";
                            return markup;
                        }

                        function repoFormatSelectionRoom(repo) {
                            return repo.name;
                        }

                        $('#filterBookingsByHotel').select2("destroy").select2({
                            multiple: true,
                            placeholder: "Select Hotel name, address",
                            maximumSelectionSize: 0,
                            closeOnSelect: false,
                            minimumInputLength: 2,
                            ajax: {
                                url: appConfig.api_path + "hotel/getHotels",
                                dataType: 'json',
                                data: function (term, page) { // page is the one-based page number tracked by Select2
                                    return {
                                        search: term, //search term
                                        page: page // page number
                                    };
                                },
                                results: function (data, page) {
                                    var more = (page * 30) < data.total_count; // whether or not there are more results available

                                    // notice we return the value of more so Select2 knows if more results can be loaded
                                    return {results: data.items, more: more};
                                },
                                cache: true
                            },
                            formatResult: repoFormatResultHotel,
                            formatSelection: repoFormatSelectionHotel
                        }).on('change', function (e) {
                            angular.element($('#booking_ctrl')).scope().filterBooking();
                        });

                        function repoFormatResultHotel(repo) {
                            var markup = "<div class='select2-result-repository clearfix'>" +
                                "<div class='select2-result-repository__title'>" + repo.name + "</div>";

                            markup += "<div class='select2-result-repository__description'>" + repo.address + "</div>";
                            markup += "</div>";
                            return markup;
                        }

                        function repoFormatSelectionHotel(repo) {
                            return repo.name;
                        }

                        $('.table').on('click', '.delete-row', function (e) {
                            var id = $(this).attr('delete-id');
                            $.smallBox({
                                title: "Cảnh báo!",
                                content: "Are you sure want to delete this booking? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#booking_ctrl\")).scope().deleteBooking(" + id + ");' class='btn btn-danger btn-sm'>Yes</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>No</a></p>",
                                color: "#C46A69",
                                //timeout: 8000,
                                icon: "fa fa-exclamation-triangle swing animated"
                            });
                        });

                        $('.filterBookingByCreatedAt').datetimepicker({
                            format: "MMM DD YYYY, HH:mm",
                            locale: 'en',
                            showClear : true,
                            useCurrent: false
                        }).on("dp.change", function (e) {
                            angular.element($('#booking_ctrl')).scope().filterBooking();
                        });
                        $("#filterBookingsByCreatedFrom").on("dp.change", function (e) {
                            if (e.date)
                                $('#filterBookingsByCreatedTo').data("DateTimePicker").minDate(e.date);
                            else $('#filterBookingsByCreatedTo').data("DateTimePicker").minDate(false);
                        });
                        $("#filterBookingsByCreatedTo").on("dp.change", function (e) {
                            if (e.date)
                                $('#filterBookingsByCreatedFrom').data("DateTimePicker").maxDate(e.date);
                            else $('#filterBookingsByCreatedFrom').data("DateTimePicker").maxDate(false);
                        });

                        function formatNumber(nStr, decSeperate, groupSeperate) {
                            nStr += '';
                            var x = nStr.split(decSeperate);
                            var x1 = x[0];
                            var x2 = x.length > 1 ? '.' + x[1] : '';
                            var rgx = /(\d+)(\d{3})/;
                            while (rgx.test(x1)) {
                                x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
                            }
                            return x1 + x2;
                        }
                    }, 1000);
                });
            }
        }
    })
});
