
define(['bookingsManagement/module'], function (module) {

    'use strict';

    return module.registerFactory('bookingsList', function ($http, $q) {
        var dfd = $q.defer();

        var bookingsList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'booking',
            method: "GET",
            params: {
                orderBy: 'bookings.created_at',
                orderDir: 'desc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            bookingsList.data.data = response.data.data;
            bookingsList.data.currentPage = 1;
            bookingsList.data.itemsPerPage = 10;
            bookingsList.data.orderBy = 'bookings.created_at';
            bookingsList.data.orderDir = 'desc';
            bookingsList.data.bookingSearchValue = '';
            bookingsList.data.totalBookings = response.data.recordsFiltered;
            dfd.resolve(bookingsList)
        });

        return bookingsList;
    });
});
