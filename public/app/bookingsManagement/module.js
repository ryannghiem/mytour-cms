define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.bookingsManagement', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.bookingsManagement', {
            abstract: true,
            data: {title: 'Bookings Management'}
        })
        // Childs
        .state('app.bookingsManagement.booking', {
            url: '/bookings',
            data: {title: 'Bookings Management'},
            views: {
                "content@app": {
                    templateUrl: 'app/bookingsManagement/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'bookingsManagement/controller/bookingsIndexCtrl',
                            'bookingsManagement/controller/BookingsCtrl',

                            'bookingsManagement/directives/bookingsList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});