
define(['bookingsManagement/module'], function (module) {

    'use strict';

    module.registerController('BookingsCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.selectStatus = [
            {value: '1', text: 'Published'},
            {value: '0', text: 'Pending'},
            {value: '-1', text: 'Trash'}
        ];

        $scope.clearFilterBookings = function () {
            $('.btn-booking-refresh i').addClass('fa-spin');
            $('.booking-article .tables_processing').show();
            $('#filterBookingsByHotel').val('');
            $scope.filterBookingsByHotel = $('#filterBookingsByHotel').val().toString();
            $http({
                url: appConfig.api_path + 'booking',
                method: "GET",
                params: {
                    orderBy: 'bookings.created_at',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.bookingsList.itemsPerPage,
                    search: '',
                    status : '',
                    "hotelId[]": $scope.filterBookingsByHotel
                }
            }).then(function (response) {
                $('.booking-article .tables_processing').hide();
                $('.btn-booking-refresh i').removeClass('fa-spin');
                $scope.bookingsList.data = response.data.data;
                $scope.bookingsList.currentPage = 1;
                $scope.bookingsList.orderBy = 'bookings.created_at';
                $scope.bookingsList.bookingSearchValue = '';
                $scope.bookingsList.filterByStatus = '';
                $scope.bookingsList.orderDir = 'desc';
                $scope.bookingsList.totalBookings = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterBooking = function () {
            $('.booking-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $scope.filterBookingsByHotel = $('#filterBookingsByHotel').val().toString();
            $http({
                url: appConfig.api_path + 'booking',
                method: "GET",
                params: {
                    orderBy: $scope.bookingsList.orderBy,
                    orderDir: $scope.bookingsList.orderDir,
                    start: 0,
                    length: $scope.bookingsList.itemsPerPage,
                    search: $scope.bookingsList.bookingSearchValue,
                    status : $scope.bookingsList.filterByStatus,
                    "hotelId[]": $scope.filterBookingsByHotel
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.booking-article .tables_processing').hide();
                $scope.bookingsList.data = response.data.data;
                $scope.bookingsList.currentPage = 1;
                $scope.bookingsList.totalBookings = response.data.recordsFiltered;
            });
        };

        $scope.setPageBooking = function (page) {
            $('.booking-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'booking',
                method: "GET",
                params: {
                    orderBy: $scope.bookingsList.orderBy,
                    orderDir: $scope.bookingsList.orderDir,
                    start: (page - 1) * $scope.bookingsList.itemsPerPage,
                    length: $scope.bookingsList.itemsPerPage,
                    search: $scope.bookingsList.bookingSearchValue,
                    status : $scope.bookingsList.filterByStatus,
                    "hotelId[]": $scope.filterBookingsByHotel
                }
            }).then(function (response) {
                $('.booking-article .tables_processing').hide();
                $scope.bookingsList.data = response.data.data;
                $scope.bookingsList.currentPage = page;
            });
        };

        $scope.sortBooking = function (colName) {
            $('.booking-article .tables_processing').show();
            $scope.bookingsList.orderBy = colName;
            $scope.bookingsList.orderDir = $scope.bookingsList.orderDir == 'asc' ? 'desc' : 'asc';
            $http({
                url: appConfig.api_path + 'booking',
                method: "GET",
                params: {
                    orderBy: $scope.bookingsList.orderBy,
                    orderDir: $scope.bookingsList.orderDir,
                    start: ($scope.bookingsList.currentPage - 1) * $scope.bookingsList.itemsPerPage,
                    length: $scope.bookingsList.itemsPerPage,
                    search: $scope.bookingsList.bookingSearchValue,
                    status : $scope.bookingsList.filterByStatus,
                    "hotelId[]": $scope.filterBookingsByHotel
                }
            }).then(function (response) {
                $('.booking-article .tables_processing').hide();
                $scope.bookingsList.data = response.data.data;
            });
        };

        $scope.addBooking = function () {
            $scope.currentBooking = {};
            $scope.currentBooking.addMode = true;
            $scope.currentBooking.ordering = 9999;
            $('#modal-add-booking').modal('show');
        };

        $scope.editBooking = function (index, booking) {
            $scope.currentBooking = {};
            $scope.currentBooking.addMode = false;
            var $id = booking.id;
            $http({
                url: appConfig.api_path + 'booking/' + $id + '/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentBooking = response.data.data;
                $(".booking-select-hotel").select2('val', $scope.currentBooking.hotel_id);
                $(".booking-select-room").select2('val', $scope.currentBooking.room_id);
                if ($scope.currentBooking.image) {
                    $scope.currentBooking.version = '?v=' + $scope.getRandomSpan();
                    $('.booking-image div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentBooking.image + $scope.currentBooking.version + "' />");
                }
            });
            $('#modal-add-booking').modal('show');
        };

        // Save change
        $scope.saveBooking = function ($status) {
            var formValidation = $('.addBookingForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($('.booking-image img').attr('src').length == 0) {
                $('.booking-image').addClass('has-error');
                $('.modal').animate({
                    scrollTop: $('.booking-image').position().top + parseFloat($('.booking-image').parents(".modal-body").position().top)
                }, 1000);
                return;
            }
            var $currentBooking = angular.copy($scope.currentBooking);
            if ($status) $currentBooking.status = $status;
            $currentBooking.image = $('.booking-image img').attr('src').replace($currentBooking.version,'');
            $currentBooking.hotel_id = $(".booking-select-hotel").select2('val');//.toString();
            $currentBooking.room_id = $(".booking-select-room").select2('val');//.toString();
            $('#modal-add-booking').modal('hide');
            $('.booking-article .tables_processing').show();
            $http.put(appConfig.api_path + 'booking/update',$currentBooking
            ).then(function (response) {
                if (response.data.msg === true) {
                    $scope.filterBooking();
                }
            });
        };

        $scope.saveResetBooking = function () {
            var formValidation = $('.resetBookingForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($scope.resetBooking.emailExist) return;
            $http.post(appConfig.api_path + 'user/resetPass',$scope.resetBooking
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterBooking();
                }
            });
            $('#modal-reset-booking').modal('hide');
            $('.booking-article .tables_processing').show();
        };

        $scope.deleteBooking = function (id) {
            $('.booking-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'booking/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.bookingsList.orderBy,
                    orderDir: $scope.bookingsList.orderDir,
                    start: ($scope.bookingsList.currentPage - 1) * $scope.bookingsList.itemsPerPage,
                    length: $scope.bookingsList.itemsPerPage,
                    search: $scope.bookingsList.bookingSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.booking-article .tables_processing').text('Xoá thành công!');
                    $scope.filterBooking();
                } else $('.booking-article .tables_processing').text("Không thể xóa nhân viên này!");
                setTimeout(function () {
                    $('.booking-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.booking-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'booking/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.booking-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("bookingColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.name = true;
                $scope.columnsShown.hotel_name = true;
                $scope.columnsShown.ordering = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("bookingColumnsShown",JSON.stringify($scope.columnsShown));
        };

        $scope.getRandomSpan = function(){
            return Math.floor((Math.random()*10)+1);
        };
    });
});

