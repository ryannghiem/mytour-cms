define(['auth/module'], function (module) {

    'use strict';

   return module.registerFactory('User', function ($http, $q, $location) {
        var dfd = $q.defer();

        var UserModel = {
            initialized: dfd.promise,
            username: undefined,
            picture: undefined
        };
       $http.get(appConfig.api_path + 'user/session').then(function(response){
          if(angular.isDefined(response.data)){
              var $user = response.data;
              UserModel.username = $user.fullname ? $user.fullname : ( $user.username ? $user.username : $user.email );
              UserModel.picture = $user.avatar ? $user.avatar : appConfig.default_avatar;
              dfd.resolve(UserModel);
          }else{
              var nextUrl = $location.path();
              if (nextUrl != '/forgot-password' || nextUrl != '/login') {
                  //angular.isDefined(response.redirect) ? $location.path(response.redirect) : $location.path("/login?next="+nextUrl);
              }
          }
       });

       return UserModel;
    });
});
