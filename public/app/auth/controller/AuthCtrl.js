
define(['auth/module','notification'], function (module) {
    'use strict';
    module.registerController('AuthCtrl', function ($rootScope, $scope, $http, $state, CSRF_TOKEN, $cookieStore, $cookies) {
        var widgetId1;
        var CaptchaCallback = function () {
            widgetId1 = grecaptcha.render('reCaptchaSubmit', {'sitekey': '6Led6CkUAAAAAMRKrzk7UyaXvh8J8qlzA1Bom5-G'});
        };
        if($rootScope.authenticated === true){
            var logout = $http.get(appConfig.api_path + 'user/logout');
            logout.success(function(response){
                $cookies = null;
                $rootScope.userLogin = null;
                $rootScope.authenticated = false;
                location.reload();
            });
            logout.error(function(response){
                // $.smallBox({
                //     title: "Internet connection timeout!",
                //     content: "<i class='fa fa-warning'></i> <i>Internet connection is timeout, please check and try again later!</i>",
                //     color: "#C46A69",
                //     iconSmall: "fa fa-times fa-2x fadeInRight animated",
                //     timeout: 5000
                // });
            });
        }else {
            $http({
                url: appConfig.api_path + 'user/failed-login',
                method: "GET"
            }).then(function (response) {
                $scope.failedLogin = response.data.data;
            });
        }

        $scope.doLogin = function (object) {
            var $form = $('#login-form');
            var formValidation = $form;
            formValidation.validate({
                rules: {
                    password : {
                        minlength : 6,
                        maxlength : 32
                    }
                }
            });
            if (!formValidation.valid()) return;

            // Re-enable button Login
            $('button.status-ajax').addClass('waiting').attr("disabled","disabled");
            var login = $http.post(appConfig.api_path + 'user/authenticate', $form.serializeObject(),  {headers: {'X-CSRF-TOKEN': CSRF_TOKEN}}).then(
                function(response){
                    $('button.status-ajax').removeClass('waiting').removeAttr("disabled");
                    var $res = response.data
                    if($res['error']){ // Error
                        var message = $res['message'];
                        if ($res.failedLogin) {
                            $scope.failedLogin = true;
                            grecaptcha.reset(widgetId1);
                        }
                        $.smallBox({
                            title: "Opps !!!",
                            content: "<i class='fa fa-warning'></i> <i>" + message + "</i>",
                            color: "#C46A69",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }else{ // Success
                        $rootScope.authenticated = true;
                        $rootScope.userLogin = $res;
                        $rootScope.userLogin.avatar = $res.avatar ? $res.avatar : appConfig.default_avatar;
                        localStorage.setItem("initSpamInvoice",JSON.stringify({
                            'countSpam' : 0,
                            'timeOutSpam' : 0,
                            'maxCountSpam' : 1000,//($rootScope.userLogin.user_type > 1) ? 1000 : 3,
                            'maxTimeSpam' : 12,
                            'multiSpam' : 1
                        }));
                        $state.go(appConfig.default_page);
                    }
                },
                function(response){
                    $('button.status-ajax').removeClass('waiting').removeAttr("disabled");
                    grecaptcha.reset(widgetId1);
                }
            );
        };
    });
});
