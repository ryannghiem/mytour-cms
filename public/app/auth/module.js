define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-cookies', 'recaptcha'
], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.auth', ['ui.router','ngCookies']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider.state('login', {
            url: '/login',
            views: {
                root: {
                    templateUrl: 'app/auth/views/login.html',
                    controller: 'AuthCtrl',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'modules/forms/directives/validate/smartValidateForm',
                            'auth/controller/AuthCtrl'
                        ])
                    }
                }
            },
            data: {
                title: 'Đăng nhập',
                htmlId: 'extr-page'
            }
        })

    });

    module.run(function($couchPotato){
        module.lazy = $couchPotato;
    });
    return module;
});