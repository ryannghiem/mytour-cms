define([
    // account
    'auth/module',
    'auth/models/User',
    // 'auth/controller/AuthCtrl',

    // layout
    'layout/module',
    'layout/actions/minifyMenu',
    'layout/actions/toggleMenu',
    'layout/actions/logout',
    'layout/actions/fullScreen',
    'layout/actions/resetWidgets',
    //'layout/actions/searchMobile',
    'layout/directives/demo/demoStates',
    'layout/directives/smartInclude',
    'layout/directives/smartDeviceDetect',
    //'layout/directives/smartFastClick',
    'layout/directives/smartLayout',
    'layout/directives/smartSpeech',
    'layout/directives/smartRouterAnimationWrap',
    'layout/directives/smartFitAppView',
    'layout/directives/radioToggle',
    'layout/directives/dismisser',
    'layout/directives/smartMenu',
    'layout/directives/bigBreadcrumbs',
    'layout/directives/stateBreadcrumbs',
    'layout/directives/smartPageTitle',
    'layout/directives/hrefVoid',
    'layout/service/SmartCss',

    'layout/directives/angular-autoNumeric',
    'layout/directives/clickAndDisable',
    'modules/widgets/directives/widgetGrid',
    'modules/widgets/directives/jarvisWidget',

    //components
    //'components/shortcut/shortcut-directive',

    //'components/todo/TodoCtrl',
    //'components/todo/models/Todo',
    //'components/todo/directives/todoList',

    // chat
    // 'components/chat/module',
    // 'components/chat/directives/asideChatWidget',
    // 'components/chat/directives/chatWidget',
    // 'components/chat/directives/chatUsers',
    // 'components/chat/api/ChatApi',

    // graphs
    'modules/graphs/module',

    // tables
    'modules/tables/module',

    // forms
    'modules/forms/module',

    // ui
    'modules/ui/module',

    // widgets
    'modules/widgets/module',

    // widgets
    'modules/maps/module',

    // appViews
    'modules/app-views/module',

    // misc
    'modules/misc/module',

    // smartAdmin
    'modules/smart-admin/module',

    //Dashboard
    'dashboard/module',

    //Users
    // 'usersManager/module',
    // 'usersManager/customer/models/customers',

    // Staffs
    'staffsManager/module',
    'staffsManager/staff/models/staff',

    //System
    'systemManager/module',
    'systemManager/store/models/store',
    'systemManager/domain/models/domain',
    'systemManager/package/models/package',

    //Hotel
    'contManager/module',
    'contManager/models/hotels',
    'contManager/models/rooms',

    //Slide
    'slidesManagement/module',
    'slidesManagement/models/slide',

    //Ads
    'adsManagement/module',
    'adsManagement/models/ad',

    //Booking
    'bookingsManagement/module',
    'bookingsManagement/models/booking',

    'introduction/module'
], function () {
    'use strict';
});
