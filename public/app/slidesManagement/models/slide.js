
define(['slidesManagement/module'], function (module) {

    'use strict';

    return module.registerFactory('slidesList', function ($http, $q) {
        var dfd = $q.defer();

        var slidesList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'slide',
            method: "GET",
            params: {
                orderBy: 'slides.created_at',
                orderDir: 'desc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            slidesList.data.data = response.data.data;
            slidesList.data.currentPage = 1;
            slidesList.data.itemsPerPage = 10;
            slidesList.data.orderBy = 'slides.created_at';
            slidesList.data.orderDir = 'desc';
            slidesList.data.slideSearchValue = '';
            slidesList.data.totalSlides = response.data.recordsFiltered;
            dfd.resolve(slidesList)
        });

        return slidesList;
    });
});
