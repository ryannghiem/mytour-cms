
define(['slidesManagement/module'], function (module) {

    'use strict';

    module.registerController('SlidesCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment, $rootScope) {

        $scope.selectStatus = [
            {value: '1', text: 'Published'},
            {value: '0', text: 'Pending'},
            {value: '-1', text: 'Trash'}
        ];

        $scope.clearFilterSlides = function () {
            $('.btn-slide-refresh i').addClass('fa-spin');
            $('.slide-article .tables_processing').show();
            $('#filterSlidesByHotel').val('');
            $scope.filterSlidesByHotel = $('#filterSlidesByHotel').val().toString();
            $http({
                url: appConfig.api_path + 'slide',
                method: "GET",
                params: {
                    orderBy: 'slides.created_at',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.slidesList.itemsPerPage,
                    search: '',
                    status : '',
                    "hotelId[]": $scope.filterSlidesByHotel
                }
            }).then(function (response) {
                $('.slide-article .tables_processing').hide();
                $('.btn-slide-refresh i').removeClass('fa-spin');
                $scope.slidesList.data = response.data.data;
                $scope.slidesList.currentPage = 1;
                $scope.slidesList.orderBy = 'slides.created_at';
                $scope.slidesList.slideSearchValue = '';
                $scope.slidesList.filterByStatus = '';
                $scope.slidesList.orderDir = 'desc';
                $scope.slidesList.totalSlides = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterSlide = function () {
            $('.slide-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $scope.filterSlidesByHotel = $('#filterSlidesByHotel').val().toString();
            $http({
                url: appConfig.api_path + 'slide',
                method: "GET",
                params: {
                    orderBy: $scope.slidesList.orderBy,
                    orderDir: $scope.slidesList.orderDir,
                    start: 0,
                    length: $scope.slidesList.itemsPerPage,
                    search: $scope.slidesList.slideSearchValue,
                    status : $scope.slidesList.filterByStatus,
                    "hotelId[]": $scope.filterSlidesByHotel
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.slide-article .tables_processing').hide();
                $scope.slidesList.data = response.data.data;
                $scope.slidesList.currentPage = 1;
                $scope.slidesList.totalSlides = response.data.recordsFiltered;
            });
        };

        $scope.setPageSlide = function (page) {
            $('.slide-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'slide',
                method: "GET",
                params: {
                    orderBy: $scope.slidesList.orderBy,
                    orderDir: $scope.slidesList.orderDir,
                    start: (page - 1) * $scope.slidesList.itemsPerPage,
                    length: $scope.slidesList.itemsPerPage,
                    search: $scope.slidesList.slideSearchValue,
                    status : $scope.slidesList.filterByStatus,
                    "hotelId[]": $scope.filterSlidesByHotel
                }
            }).then(function (response) {
                $('.slide-article .tables_processing').hide();
                $scope.slidesList.data = response.data.data;
                $scope.slidesList.currentPage = page;
            });
        };

        $scope.sortSlide = function (colName) {
            $('.slide-article .tables_processing').show();
            $scope.slidesList.orderBy = colName;
            $scope.slidesList.orderDir = $scope.slidesList.orderDir == 'asc' ? 'desc' : 'asc';
            $http({
                url: appConfig.api_path + 'slide',
                method: "GET",
                params: {
                    orderBy: $scope.slidesList.orderBy,
                    orderDir: $scope.slidesList.orderDir,
                    start: ($scope.slidesList.currentPage - 1) * $scope.slidesList.itemsPerPage,
                    length: $scope.slidesList.itemsPerPage,
                    search: $scope.slidesList.slideSearchValue,
                    status : $scope.slidesList.filterByStatus,
                    "hotelId[]": $scope.filterSlidesByHotel
                }
            }).then(function (response) {
                $('.slide-article .tables_processing').hide();
                $scope.slidesList.data = response.data.data;
            });
        };

        $scope.addSlide = function () {
            $scope.currentSlide = {};
            $scope.currentSlide.addMode = true;
            $scope.currentSlide.ordering = 9999;
            $('#modal-add-slide').modal('show');
        };

        $scope.editSlide = function (index, slide) {
            $scope.currentSlide = {};
            $scope.currentSlide.addMode = false;
            var $id = slide.id;
            $http({
                url: appConfig.api_path + 'slide/' + $id + '/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentSlide = response.data.data;
                $(".slide-select-hotel").select2('val', $scope.currentSlide.hotel_id);
                $(".slide-select-room").select2('val', $scope.currentSlide.room_id);
                if ($scope.currentSlide.image) {
                    $scope.currentSlide.version = '?v=' + $scope.getRandomSpan();
                    $('.slide-image div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentSlide.image + $scope.currentSlide.version + "' />");
                }
            });
            $('#modal-add-slide').modal('show');
        };

        // Save change
        $scope.saveSlide = function ($status) {
            var formValidation = $('.addSlideForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($('.slide-image img').attr('src').length == 0) {
                $('.slide-image').addClass('has-error');
                $('.modal').animate({
                    scrollTop: $('.slide-image').position().top + parseFloat($('.slide-image').parents(".modal-body").position().top)
                }, 1000);
                return;
            }
            var $currentSlide = angular.copy($scope.currentSlide);
            if ($status) $currentSlide.status = $status;
            $currentSlide.image = $('.slide-image img').attr('src').replace($currentSlide.version,'');
            $currentSlide.hotel_id = $(".slide-select-hotel").select2('val');//.toString();
            $currentSlide.room_id = $(".slide-select-room").select2('val');//.toString();
            $('#modal-add-slide').modal('hide');
            $('.slide-article .tables_processing').show();
            $http.put(appConfig.api_path + 'slide/update',$currentSlide
            ).then(function (response) {
                if (response.data.msg === true) {
                    $scope.filterSlide();
                }
            });
        };

        $scope.saveResetSlide = function () {
            var formValidation = $('.resetSlideForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($scope.resetSlide.emailExist) return;
            $http.post(appConfig.api_path + 'user/resetPass',$scope.resetSlide
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterSlide();
                }
            });
            $('#modal-reset-slide').modal('hide');
            $('.slide-article .tables_processing').show();
        };

        $scope.deleteSlide = function (id) {
            $('.slide-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'slide/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.slidesList.orderBy,
                    orderDir: $scope.slidesList.orderDir,
                    start: ($scope.slidesList.currentPage - 1) * $scope.slidesList.itemsPerPage,
                    length: $scope.slidesList.itemsPerPage,
                    search: $scope.slidesList.slideSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.slide-article .tables_processing').text('Xoá thành công!');
                    $scope.filterSlide();
                } else $('.slide-article .tables_processing').text("Không thể xóa nhân viên này!");
                setTimeout(function () {
                    $('.slide-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.slide-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'slide/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.slide-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("slideColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.name = true;
                $scope.columnsShown.hotel_name = true;
                $scope.columnsShown.ordering = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("slideColumnsShown",JSON.stringify($scope.columnsShown));
        };

        $scope.getRandomSpan = function(){
            return Math.floor((Math.random()*10)+1);
        };
    });
});

