define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.slidesManagement', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.slidesManagement', {
            abstract: true,
            data: {title: 'Slides Management'}
        })
        // Childs
        .state('app.slidesManagement.slide', {
            url: '/slides',
            data: {title: 'Slides Management'},
            views: {
                "content@app": {
                    templateUrl: 'app/slidesManagement/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'slidesManagement/controller/slidesIndexCtrl',
                            'slidesManagement/controller/SlidesCtrl',

                            'slidesManagement/directives/slidesList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});