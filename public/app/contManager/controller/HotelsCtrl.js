define(['contManager/module', 'ckeditor'], function (module) {

    'use strict';

    module.registerController('HotelsCtrl', function ($scope, $http, $rootScope, $q, CSRF_TOKEN, moment ) {
        var S = require('stringjs');

        $scope.selectStatus = [
            {value: '1', text: 'Published'},
            {value: '0', text: 'Pending'},
            {value: '-1', text: 'Trash'}
        ];

        $scope.filterHotelsByCategories = '';
        $scope.filterHotelsByStatus = '';
        $scope.filterHotelsByCreatedFrom = '';
        $scope.filterHotelsByCreatedTo = '';
        $scope.filterHotelsByDomain = '';

        $scope.clearFilterHotels = function () {
            $('.btn-hotel-refresh i').addClass('fa-spin');
            $('.hotel-article .tables_processing').show();
            $scope.filterHotelsByStatus = '';
            $('#filterHotelsByCity').select2('val','');
            $scope.filterHotelsByCity = '';
            $http({
                url: appConfig.api_path + 'hotel',
                method: "GET",
                params: {
                    orderBy: 'hotels.created_at',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.hotelsList.itemsPerPage,
                    search: '',
                    domain : $scope.filterHotelsByDomain,
                    city_id : $scope.filterHotelsByCity,
                    status : $scope.filterHotelsByStatus,
                    created_from : $scope.filterHotelsByCreatedFrom,
                    created_to : $scope.filterHotelsByCreatedTo,
                    refresh : 1
                }
            }).then(function (response) {
                $('.hotel-article .tables_processing').hide();
                $('.btn-hotel-refresh i').removeClass('fa-spin');
                $scope.hotelsList.data = response.data.data;
                $scope.hotelsList.currentPage = 1;
                $scope.hotelsList.orderBy = 'hotels.created_at';
                $scope.hotelsList.hotelSearchValue = '';
                $scope.hotelsList.orderDir = 'desc';
                $scope.hotelsList.totalHotels = response.data.recordsFiltered;
            });
        };

        $scope.hotelSearchValue = '';
        var canceler = $q.defer();
        $scope.filterHotel = function () {
            $('.hotel-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            if ($('#filterHotelsByCreatedFrom').val()) {
                $scope.filterHotelsByCreatedFrom = moment.tz($('#filterHotelsByCreatedFrom').val(), 'MMM DD YYYY, HH:mm', appConfig.timeZone).utc().format('YYYY-MM-DD HH:mm:ss');
            }else $scope.filterHotelsByCreatedFrom = '';
            if ($('#filterHotelsByCreatedTo').val()) {
                $scope.filterHotelsByCreatedTo = moment.tz($('#filterHotelsByCreatedTo').val(), 'MMM DD YYYY, HH:mm', appConfig.timeZone).utc().format('YYYY-MM-DD HH:mm:ss');
            }else $scope.filterHotelsByCreatedTo = '';
            $scope.filterHotelsByCity = $('#filterHotelsByCity').select2('val');
            $http({
                url: appConfig.api_path + 'hotel',
                method: "GET",
                params: {
                    orderBy: $scope.hotelsList.orderBy,
                    orderDir: $scope.hotelsList.orderDir,
                    start: ($scope.hotelsList.currentPage - 1) * $scope.hotelsList.itemsPerPage,
                    length: $scope.hotelsList.itemsPerPage,
                    search: $scope.hotelsList.hotelSearchValue,
                    domain : $scope.filterHotelsByDomain,
                    city_id : $scope.filterHotelsByCity,
                    status : $scope.filterHotelsByStatus,
                    created_from : $scope.filterHotelsByCreatedFrom,
                    created_to : $scope.filterHotelsByCreatedTo
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.hotel-article .tables_processing').hide();
                $scope.hotelsList.data = response.data.data;
                $scope.hotelsList.totalHotels = response.data.recordsFiltered;
            });
        };

        $scope.setPageHotel = function (page) {
            $('.hotel-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'hotel',
                method: "GET",
                params: {
                    orderBy: $scope.hotelsList.orderBy,
                    orderDir: $scope.hotelsList.orderDir,
                    start: (page - 1) * $scope.hotelsList.itemsPerPage,
                    length: $scope.hotelsList.itemsPerPage,
                    search: $scope.hotelsList.hotelSearchValue,
                    city_id : $scope.filterHotelsByCity,
                    domain : $scope.filterHotelsByDomain,
                    status : $scope.filterHotelsByStatus,
                    created_from : $scope.filterHotelsByCreatedFrom,
                    created_to : $scope.filterHotelsByCreatedTo,
                    affiliate_name : $scope.filterHotelsByAffiliate
                }
            }).then(function (response) {
                $('.hotel-article .tables_processing').hide();
                $scope.hotelsList.data = response.data.data;
                $scope.hotelsList.currentPage = page;
            });
        };

        $scope.sortHotel = function (colName) {
            $('.hotel-article .tables_processing').show();
            $scope.hotelsList.orderBy = colName;
            $scope.hotelsList.orderDir = $scope.hotelsList.orderDir == 'asc' ? 'desc' : 'asc';
            $http({
                url: appConfig.api_path + 'hotel',
                method: "GET",
                params: {
                    orderBy: $scope.hotelsList.orderBy,
                    orderDir: $scope.hotelsList.orderDir,
                    start: ($scope.hotelsList.currentPage - 1) * $scope.hotelsList.itemsPerPage,
                    length: $scope.hotelsList.itemsPerPage,
                    search: $scope.hotelsList.hotelSearchValue,
                    city_id : $scope.filterHotelsByCity,
                    domain : $scope.filterHotelsByDomain,
                    status : $scope.filterHotelsByStatus,
                    created_from : $scope.filterHotelsByCreatedFrom,
                    created_to : $scope.filterHotelsByCreatedTo,
                    affiliate_name : $scope.filterHotelsByAffiliate
                }
            }).then(function (response) {
                $('.hotel-article .tables_processing').hide();
                $scope.hotelsList.data = response.data.data;
            });
        };

        $scope.deleteHotel = function ($id) {
            $('.hotel-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'hotel/' + $id,
                method: "DELETE"
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.hotel-article .tables_processing').text('Deleted Hotel!');
                    $scope.filterHotel();
                } else $('.hotel-article .tables_processing').text("Can't delete Hotel!");
                setTimeout(function () {
                    $('.hotel-article .tables_processing').hide().text('Processing...');
                }, 2000);
                $('#modal-add-hotel').modal('hide');
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.hotel-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'hotel/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.hotel-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
            //if($status == 'trash'){
            //    return "You are not have permission set this action to Trash!";
            //}
            //return false; //When return a value != true, this function will not save data change to model -> not change value -> show massage return
        };

        $scope.generateAlias = function () {
            var url = '';
            if ($scope.currentHotel.name) {
                url = S($scope.currentHotel.name).slugify().s;
                url = url.toLowerCase();
            } else {
                url = '';
            }
            $scope.currentHotel.alias = url;
            if ($scope.currentHotel.alias) {
                $scope.currentHotel.alias = $scope.currentHotel.alias.replace(/ /g, '-');
                $scope.currentHotel.alias = $scope.currentHotel.alias.replace(/&/g, '-');
                $('span.alias').text($scope.currentHotel.alias);
            }
        };
        $scope.checkHotelAliasExists = function () {
            if ($('#hotel_alias').val()) {
                $('.hotel-alias').addClass('input-checking');
                $http({
                    url: appConfig.api_path + 'hotel/checkAliasExists',
                    method: "GET",
                    params: {
                        id : $scope.currentHotel.id,
                        alias: $('#hotel_alias').val(),
                        countrycode : $scope.currentHotel.countrycode
                    }
                }).then(function (response) {
                    $('.hotel-alias').removeClass('input-checking');
                    $scope.currentHotel.checkAliasExists = response.data.status;
                    if ($scope.currentHotel.checkAliasExists) {
                        $scope.currentHotel.hotelsDuplicateAlias = response.data.hotels;
                    }
                });
            }
        };

        $scope.checkHotelNameExists = function () {
            if ($scope.currentHotel.name) {
                $('.hotel-name').addClass('input-checking');
                $http({
                    url: appConfig.api_path + 'hotel/checkNameExists',
                    method: "GET",
                    params: {
                        id : $scope.currentHotel.id,
                        name: $scope.currentHotel.name,
                        countrycode : $scope.currentHotel.countrycode
                    }
                }).then(function (response) {
                    $('.hotel-name').removeClass('input-checking');
                    $scope.currentHotel.checkNameExists = response.data.status;
                    if ($scope.currentHotel.checkNameExists) {
                        $scope.checkHotelAliasExists();
                        $scope.currentHotel.hotelsDuplicateName = response.data.hotels;
                    }
                });
            }
        };
        $scope.checkHotelURLExists = function () {
            if ($scope.currentHotel.hotel_url) {
                $('.hotel-url').addClass('input-checking');
                $http({
                    url: appConfig.api_path + 'hotel/checkHotelUrlExists',
                    method: "GET",
                    params: {
                        id : $scope.currentHotel.id,
                        hotel_url: $scope.currentHotel.hotel_url,
                        countrycode : $scope.currentHotel.countrycode
                    }
                }).then(function (response) {
                    $('.hotel-url').removeClass('input-checking');
                    $scope.currentHotel.checkURLExists = response.data.status;
                    if ($scope.currentHotel.checkURLExists) {
                        $scope.currentHotel.hotelsDuplicateUrl = response.data.hotels;
                    }
                });
            }
        };

        $scope.listCountries = [];

        $scope.getListCountries = function (keyword) {
            if (keyword.length < 2) return;
            $http({
                url: appConfig.api_path + 'country/list',
                method: "GET",
                params: {k: keyword}
            }).then(function (response) {
                $scope.listCountries = response.data;
            });
        };

        $scope.addHotel = function () {
            $scope.currentHotel = {};
            $scope.currentHotel.addMode = true;
            $scope.autoSaveHotelDraft = true;
            $scope.currentHotel.is_best = 0;
            $scope.currentHotel.is_hot = 0;
            $scope.currentHotel.stars = 1;
            $scope.currentHotel.description = '';
            $('#hotelCity').select2('val', '');
            $('.hotel-image-origin div.fileinput-preview.thumbnail').html("<img src='http://via.placeholder.com/270x180' />");
            $('.hotel-image-small div.fileinput-preview.thumbnail').html("<img src='http://via.placeholder.com/90x60' />");
            $('#modal-add-hotel').modal('show');
        };

        $scope.editHotel = function (index, hotel) {
            $scope.currentHotel = {};
            $scope.currentHotel.addMode = false;
            $scope.autoSaveHotelDraft = false;
            var $id = hotel.id;
            $http({
                url: appConfig.api_path + 'hotel/'+ $id + '/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentHotel = response.data.data;
                // save aff url to compare

                $scope.currentHotel.index = index;
                $scope.currentHotel.editId = hotel.id;
                $('#hotelCity').select2('val', $scope.currentHotel.city_id);
                $scope.currentHotel.version = '?v=' + $scope.getRandomSpan();
                if ($scope.currentHotel.image_origin) {
                    $('.hotel-image-origin div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentHotel.image_origin + $scope.currentHotel.version + "' />");
                }
                if ($scope.currentHotel.image_small) {
                    $('.hotel-image-small div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentHotel.image_small + $scope.currentHotel.version + "' />");
                }

            });
            $('#modal-add-hotel').modal('show');
        };

        // Save change
        $scope.saveHotel = function (status) {
            var formValidation = $('.addHotelForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($('.hotel-image-origin img').attr('src').length == 0) {
                $('.hotel-image-origin').addClass('has-error');
                $('.modal').animate({
                    scrollTop: $('.hotel-image-origin').position().top + parseFloat($('.hotel-image-origin').parents(".modal-body").position().top)
                }, 1000);
                return;
            }
            var $currentHotel = angular.copy($scope.currentHotel);
            $currentHotel.image_origin = $('.hotel-image-origin img').attr('src').replace($currentHotel.version,'');
            $currentHotel.image_small = ($('.hotel-image-small img') && $('.hotel-image-small img').attr('src')) ? $('.hotel-image-small img').attr('src').replace($currentHotel.version,'') : '';
            if (status) $currentHotel.status = status;
            $currentHotel.city_id = $('#hotelCity').select2('val');
            $('#modal-add-hotel').modal('hide');
            $('.hotel-article .tables_processing').show();

            $http.put(appConfig.api_path + 'hotel/update', $currentHotel
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.filterHotel();
                }
            });
        };

        $scope.savedHotelDraft = localStorage.getItem("savedHotelDraft");

        setInterval(function () {
            if ($scope.currentHotel && !$scope.currentHotel.id && $scope.currentHotel.name) {
                $scope.savedHotelDraft = true;
                localStorage.setItem("savedHotelDraft",$scope.savedHotelDraft);
                localStorage.setItem("hotelDraft", angular.toJson($scope.currentHotel));
            }
        },$scope.$parent.timeSaveDraft);

        $scope.loadHotelDraft = function () {
            $scope.currentHotel = angular.fromJson(localStorage.getItem("hotelDraft"));
            $scope.checkHotelNameExists();
            $scope.checkHotelAliasExists();
            $scope.checkHotelURLExists();
            delete $scope.currentHotel.id;
            $scope.currentHotel.cash_back_json = angular.fromJson($scope.currentHotel.cash_back_json);
            if (!$scope.currentHotel.cash_back_json) $scope.currentHotel.cash_back_json = [];
            $scope.currentHotel.description = angular.fromJson($scope.currentHotel.description);
            if (!$scope.currentHotel.description) $scope.currentHotel.description = [];
            if ($scope.currentHotel.vendors) {
                for (var i = 0; i<$scope.currentHotel.vendors.length;i++){
                    $scope.currentHotel.vendors[i].cash_back_json = angular.fromJson($scope.currentHotel.vendors[i].cash_back_json);
                    if (!$scope.currentHotel.vendors[i].cash_back_json) $scope.currentHotel.vendors[i].cash_back_json = [];
                    $scope.currentHotel.vendors[i].description = angular.fromJson($scope.currentHotel.vendors[i].description);
                    if (!$scope.currentHotel.vendors[i].description) $scope.currentHotel.vendors[i].description = [];
                }
            }
            if ($scope.currentHotel.publish_date) {
                $scope.currentHotel.publish_date = moment.utc($scope.currentHotel.publish_date).tz(appConfig.timeZone).format('MMM DD YYYY, HH:mm');
            }
            if (!$scope.currentHotel.countries_code) $scope.currentHotel.countries_code = null;
            $scope.currentHotel.categories_id = JSON.parse($scope.currentHotel.categories_id);
            $scope.currentHotel.countries_code = JSON.parse($scope.currentHotel.countries_code);
            $('#primaryCountry').select2('val', [$scope.currentHotel.countrycode]);
            $('#listCategories').select2('val', $scope.currentHotel.categories_id);
            $('#listCountries').select2('val', $scope.currentHotel.countries_code);
            if ($scope.currentHotel.tags) {
                $('.hotel-tags').select2('val', $scope.currentHotel.tags.split(","));
            } else {
                $('.hotel-tags').select2('val', '');
            }
            if ($scope.currentHotel.logo) {
                $('.hotel-logo div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentHotel.logo + "' />");
            }
            if ($scope.currentHotel.social_image) {
                $('.hotel-social-image div.fileinput-preview.thumbnail').html("<img src='" + $scope.currentHotel.social_image + "' />");
            }

            if ($scope.currentHotel.sid_name && $scope.currentHotel.affiliate_name)
                $scope.currentHotel.sid_affiliate = $scope.currentHotel.sid_name + '__' + $scope.currentHotel.affiliate_name;
        };

        $scope.columnsShown = JSON.parse(localStorage.getItem("hotelColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.coupons = true;
                $scope.columnsShown.deals = true;
                $scope.columnsShown.country = false;
                $scope.columnsShown.createdBy = false;
                $scope.columnsShown.updatedBy = false;
                $scope.columnsShown.updatedAt = false;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("hotelColumnsShown",JSON.stringify($scope.columnsShown));
        };
        $scope.getRandomSpan = function(){
            return Math.floor((Math.random()*10)+1);
        };
    });

});
