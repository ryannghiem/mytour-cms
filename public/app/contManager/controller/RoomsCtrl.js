
define(['contManager/module'], function (module) {

    'use strict';

    module.registerController('RoomsCtrl', function ($scope, $http, $q, CSRF_TOKEN, moment) {

        $scope.now = (new Date).getTime();

        $scope.selectStatus = [
            {value: '1', text: 'Published'},
            {value: '0', text: 'Pending'},
            {value: '-1', text: 'Trash'}
        ];

        var canceler = $q.defer();
        $scope.filterRoom = function () {
            $('.room-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $scope.filterRoomsByAuthor = $('#filterRoomsByAuthor').val().toString();
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: ($scope.roomsList.currentPage - 1) * $scope.roomsList.itemsPerPage,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue,
                    author : $scope.filterRoomsByAuthor,
                    status : $scope.filterRoomsByStatus
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.room-article .tables_processing').hide();
                $scope.roomsList.data = response.data.data;
                $scope.roomsList.totalRooms = response.data.recordsFiltered;
            });
        };

        $scope.clearFilterRooms = function () {
            $('.btn-room-refresh i').addClass('fa-spin');
            $('.room-article .tables_processing').show();
            $scope.roomsList.filterByHotels = [];
            $scope.roomsList.hotelId = [];
            $('#filterRoomsByAuthor').select2('val','');
            $scope.filterRoomsByStatus = '';
            $scope.roomsList.currentPage = 1;
            $scope.roomsList.roomSearchValue = '';
            $scope.filterRoomsByAuthor = '';
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: ($scope.roomsList.currentPage - 1) * $scope.roomsList.itemsPerPage,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue,
                    author : $scope.filterRoomsByAuthor,
                    status : $scope.filterRoomsByStatus
                }
            }).then(function (response) {
                $('.room-article .tables_processing').hide();
                $('.btn-room-refresh i').removeClass('fa-spin');
                $scope.roomsList.data = response.data.data;
                $scope.roomsList.totalRooms = response.data.recordsFiltered;
            });
        };

        $scope.removeFilterRoomByHotel = function (index) {
            $('.room-article .tables_processing').show();
            $scope.roomsList.filterByHotels.splice(index,1);
            $scope.roomsList.hotelId.splice(index,1);
            $scope.roomsList.currentPage = 1;
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: ($scope.roomsList.currentPage - 1) * $scope.roomsList.itemsPerPage,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue,
                    author : $scope.filterRoomsByAuthor,
                    status : $scope.filterRoomsByStatus
                }
            }).then(function (response) {
                $('.tables_processing').hide();
                $scope.roomsList.data = response.data.data;
                $scope.roomsList.totalRooms = response.data.recordsFiltered;
            });
        };

        $scope.updateStatus = function($id, $status){
            $('.room-article .tables_processing').show();
            $http.post(appConfig.api_path + 'room/changeStatus', {id:$id,status:$status,_token:CSRF_TOKEN}).
                success(function(data, status, headers, config) {
                    $('.room-article .tables_processing').hide();
                });
        };

        $scope.updateOrder = function($id, $data){
            $('.room-article .tables_processing').show();
            $http.post(appConfig.api_path + 'room/changeOrder', {id:$id,top_order:$data,_token:CSRF_TOKEN}).
                success(function(data, status, headers, config) {
                    console.log(data);
                    $('.room-article .tables_processing').hide();
                });
        };
        $scope.setPageRoom = function (page) {
            $('.room-article .tables_processing').show();
            $scope.roomsList.currentPage = page;
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: ($scope.roomsList.currentPage - 1) * $scope.roomsList.itemsPerPage,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue,
                    author : $scope.filterRoomsByAuthor,
                    status : $scope.filterRoomsByStatus
                }
            }).then(function (response) {
                $('.room-article .tables_processing').hide();
                $scope.roomsList.data = response.data.data;
            });
        };

        $scope.sortRoom = function (colName) {
            $('.room-article .tables_processing').show();
            $scope.roomsList.orderBy = colName;
            $scope.roomsList.orderDir = $scope.roomsList.orderDir == 'asc' ? 'desc' : 'asc';
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: ($scope.roomsList.currentPage - 1) * $scope.roomsList.itemsPerPage,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue,
                    author : $scope.filterRoomsByAuthor,
                    status : $scope.filterRoomsByStatus
                }
            }).then(function (response) {
                $('.room-article .tables_processing').hide();
                $scope.roomsList.data = response.data.data;
            });
        };

        $scope.deleteRoom = function ($id) {
            $('.room-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'room/' + $id,
                method: "DELETE"
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.room-article .tables_processing').text('Deleted Room!');
                    $scope.filterRoom();
                }else $('.room-article .tables_processing').text("Can't delete Room!");
                setTimeout(function () {
                    $('.room-article .tables_processing').hide().text('Processing...');
                },2000);
                $('#modal-add-room').modal('hide');
            });
        };

        $scope.editRoom = function (index,room) {
            $scope.$parent.currentRoom = {};
            var $id = room.id;
            $http({
                url: appConfig.api_path + 'room/' + $id + '/edit',
                method: "GET"
            }).then(function (response) {
                $scope.$parent.currentRoom = response.data.data;
                $scope.$parent.currentRoom.index = index;
                $scope.$parent.currentRoom.hotel_id = $scope.$parent.currentRoom.hotel.id;
                $scope.$parent.currentRoom.hotelName = $scope.$parent.currentRoom.hotel.name;
                $scope.$parent.currentRoom.has_discount = $scope.$parent.currentRoom.has_discount === 1;
                if ($scope.$parent.currentRoom.discount_start) {
                    $scope.$parent.currentRoom.discount_start = moment.utc($scope.$parent.currentRoom.discount_start).format('MMM DD YYYY, HH:mm');
                }
                if ($scope.$parent.currentRoom.discount_end) {
                    $scope.$parent.currentRoom.discount_end = moment.utc($scope.$parent.currentRoom.discount_end).format('MMM DD YYYY, HH:mm');
                }
            });
            $('#modal-add-room').modal('show');
        };

        $scope.saveRoom = function (status) {
            var formValidation = $('.addRoomForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if (!$scope.$parent.currentRoom.image) {
                $('.room-image').addClass('has-error');
                $('.modal').animate({
                    scrollTop: $('.room-image').position().top + parseFloat($('.room-image').parents(".modal-body").position().top)
                }, 1000);
                return;
            }
            $('#modal-add-room button.btn').addClass('disabled');
            var $currentRoom = angular.copy($scope.$parent.currentRoom);
            $currentRoom.has_discount = $currentRoom.has_discount ? 1 : 0;
            if (status) $currentRoom.status = status;
            if ($('#room-discount-start').val() && $currentRoom.has_discount) {
                $currentRoom.discount_start = moment.utc($('#room-discount-start').val(), 'MMM DD YYYY, HH:mm').format('YYYY-MM-DD HH:mm:ss');
            } else $currentRoom.discount_start = null;
            if ($('#room-discount-end').val() && $currentRoom.has_discount) {
                $currentRoom.discount_end = moment.utc($('#room-discount-end').val(), 'MMM DD YYYY, HH:mm').format('YYYY-MM-DD HH:mm:ss');
            } else $currentRoom.discount_end = null;
            $('#modal-add-room').modal('hide');
            $('.room-article .tables_processing').show();
            $http.put(appConfig.api_path + 'room/update',$currentRoom
            ).then(function (response) {
                if (!$scope.$parent.currentRoom.id) {
                    if (response.data.msg == true) {
                        $scope.roomsList.filterByHotels = [];
                        $scope.roomsList.hotelId = [];
                        $scope.roomsList.filterByHotels.push({id:$scope.$parent.currentRoom.hotel_id,name:$scope.$parent.currentRoom.hotelName});
                        $scope.roomsList.hotelId.push($scope.$parent.currentRoom.hotel_id);
                        $('.room-article .tables_processing').text('Created Room!');
                        $scope.$parent.currentRoom.hotel.room_count += 1;
                    } else $('.room-article .tables_processing').text("Can't create Room!");
                } else {
                    if (response.data.msg == true) {
                        $('.room-article .tables_processing').text('Updated Room!');
                    } else $('.room-article .tables_processing').text("Can't update Room!");
                }
                $scope.filterRoom();
                setTimeout(function () {
                    $('.room-article .tables_processing').hide().text('Processing...');
                }, 2000);
                $('#modal-add-room button.btn').removeClass('disabled');
            });
        };

        $scope.roomChangeHotel = function () {
            $('#room-change-hotel').addClass('disabled').empty().append("<i class='fa fa-spinner fa-pulse'></i>");
            var params = {
                id : $scope.$parent.currentRoom.id,
                hotel_id : $scope.$parent.currentRoom.hotel_id
            };
            $http.post(appConfig.api_path + 'room/changeHotel',params
            ).then(function (response) {
                    if (response.data.status) {
                        $scope.$parent.currentRoom.hotelName = $('.room-select-hotel').select2('data').name;
                    }
                    $('#room-change-hotel').removeClass('disabled').empty().append('<i class="fa fa-check"></i>');
                    $('.room-hotel-name').show();
                    $scope.$parent.currentRoom.editHotel = false;
                });
        };

        $scope.roomEditHotel = function (editing) {
            if (editing) {
                $('.room-hotel-name').hide();
                $scope.$parent.currentRoom.editHotel = true;
            }else {
                $('.room-hotel-name').show();
                $scope.$parent.currentRoom.editHotel = false;
            }
        };

        $scope.savedRoomDraft = localStorage.getItem("savedRoomDraft");

        setInterval(function () {
            if ($scope.$parent.currentRoom && !$scope.$parent.currentRoom.id && $scope.$parent.currentRoom.title) {
                $scope.savedRoomDraft = true;
                localStorage.setItem("savedRoomDraft",$scope.savedRoomDraft);
                localStorage.setItem("roomDraft", angular.toJson($scope.$parent.currentRoom));
            }
        },$scope.$parent.timeSaveDraft);

        $scope.loadRoomDraft = function () {
            $scope.$parent.currentRoom = angular.fromJson(localStorage.getItem("roomDraft"));
            delete $scope.$parent.currentRoom.id;
            if ($scope.$parent.currentRoom.currency = '%' ) $('#currency-room').parents('.form-inline').find('input.auto-numeric').autoNumeric('update', {vMax: 100});
            $('#room-category').select2('val', $scope.$parent.currentRoom.categories_id);
            if ($scope.$parent.currentRoom.tags) {
                $('.room-tags').select2('val', $scope.$parent.currentRoom.tags.split(","));
            } else {
                $('.room-tags').select2('val', '');
            }
        };

        $scope.resetModal = function () {
            $scope.$apply(function () {
                $scope.$parent.currentRoom = {};
            });
        };

        $scope.removeRoomVendor = function (index) {
            $("[href='#room_"+$scope.$parent.currentRoom.vendors[index].countrycode+"']").parents(".nav-tabs").find('li').first().find('a').click();
            $scope.$parent.currentRoom.vendors.splice(index,1);
        };

        $scope.checkRoomCodeExists = function () {
            canceler.resolve();
            canceler = $q.defer();
            $('.room-code').addClass('input-checking');
            $http({
                url: appConfig.api_path + 'room/checkDuplicate',
                method: "GET",
                params: {
                    id : $scope.$parent.currentRoom.id,
                    hotel_id : $scope.$parent.currentRoom.hotel_id,
                    room_code : $scope.$parent.currentRoom.room_code
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.room-code').removeClass('input-checking');
                $scope.$parent.currentRoom.roomCodeExists = response.data.status;
                if (response.data.status == true) {
                    $scope.$parent.currentRoom.roomsDuplicate = response.data.rooms;
                }
            });
        };

        $scope.roomColumnsShown = angular.fromJson(localStorage.getItem("roomColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.roomColumnsShown) {
                $scope.roomColumnsShown = {};
                $scope.roomColumnsShown.country = true;
                $scope.roomColumnsShown.createdBy = false;
                $scope.roomColumnsShown.updatedBy = false;
                $scope.roomColumnsShown.updatedAt = false;
                $scope.roomColumnsShown.topOrdering = false;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("roomColumnsShown",angular.toJson($scope.roomColumnsShown));
        };


        $scope.setCashBack = function ($index, $cb) {
            if ($cb.cash_back_percent) {
                $scope.$parent.currentRoom.cash_back = $cb.cash_back_percent + '%' + ' Cash Back'
            }else if ($cb.cash_back) {
                $scope.$parent.currentRoom.cash_back = $cb.currency + $cb.cash_back + ' Cash Back'
            }
        };

        $scope.removeCashBack = function () {
            $scope.$parent.currentRoom.cash_back = null;
            $('.radio-cash-back').prop('checked', false);
        }
    });
});