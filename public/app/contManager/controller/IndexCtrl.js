
define(['contManager/module'], function (module) {

    'use strict';

    module.registerController('IndexCtrl', function ($scope, $timeout, $http, CSRF_TOKEN, $cookieStore, $location) {
/*
        Check permission access Area
        Skip check if user role is sa (Super Admin)
*/

        $scope.listCurrency = [
            { id : '%', name: '%'},
            { id : '$', name: '$'},
            { id : '£', name: '£'}
        ];

        $scope.$parent.autoLoad = false;

        $scope.domain_most = appConfig.domain_most;
        $scope.domain_dv = appConfig.domain_dv;
        $scope.domain_u1 = appConfig.domain_u1;
        $scope.domain_g1 = appConfig.domain_g1;
        $scope.domain_in = appConfig.domain_in;
        $scope.suffix_room_most = appConfig.suffix_room_most;
        $scope.suffix_room_dv = appConfig.suffix_room_dv;
        $scope.suffix_room_u1 = appConfig.suffix_room_most;
        $scope.suffix_room_g1 = appConfig.suffix_room_dv;

        $scope.timeSaveDraft = 5000;
        $scope.hotelsList = {};

        $scope.roomsList = {};
        $scope.roomsList.data = [];
        $scope.roomsList.currentPage = 1;
        $scope.roomsList.itemsPerPage = 10;
        $scope.roomsList.orderBy = 'rooms.created_at';
        $scope.roomsList.roomSearchValue = '';
        $scope.roomsList.orderDir = 'desc';
        $scope.roomsList.totalRooms = 0;
        $scope.roomsList.filterByHotels = [];
        $scope.roomsList.hotelId = [];
        $scope.filterRoomByHotel = function (hotel) {
            if (!$scope.arrayContains(hotel.id,$scope.roomsList.hotelId)) {
                $scope.roomsList.filterByHotels.push(hotel);
                $scope.roomsList.hotelId.push(hotel.id);
            }
            $('.room-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'room',
                method: "GET",
                params: {
                    "hotelId[]": $scope.roomsList.hotelId,
                    orderBy: $scope.roomsList.orderBy,
                    orderDir: $scope.roomsList.orderDir,
                    start: 0,
                    length: $scope.roomsList.itemsPerPage,
                    search: $scope.roomsList.roomSearchValue
                }
            }).then(function (response) {
                $('.room-article .tables_processing').hide();
                $scope.roomsList.data = response.data.data;
                $scope.roomsList.currentPage = 1;
                $scope.roomsList.totalRooms = response.data.recordsFiltered;
            });
        };
        $scope.hotelItem = {};

        $scope.addRoom = function (index,hotel) {
            $scope.currentRoom = {};
            $scope.currentRoom.hotel = hotel;
            $scope.currentRoom.hotel_id = hotel.id;
            $scope.currentRoom.hotelName = hotel.name;
            $scope.currentRoom.size = 2;
            $scope.currentRoom.price = 0;
            $scope.currentRoom.discount_percent = 0;
            $scope.currentRoom.discount_price = 0;
            $scope.currentRoom.ordering = 9999;
            $('#room-discount-start').val('');
            $('#room-discount-end').val('');
            $('#modal-add-room').modal('show');
        };

        $scope.arrayContains = function (value, container) {
            if (value) {
                return $.inArray(value, container) != -1;
            }
            return false
        };

        $scope.escapeHtml = function(str) {
            var entityMap = {
                // "&": "&amp;",
                // "<": "&lt;",
                // ">": "&gt;",
                // '"': '&quot;',
                // "'": '&#39;',
                '\\': ''
            };
            return String(str).replace(/[\\]/g, function (s) {
                return entityMap[s];
            });
        };
    });
});