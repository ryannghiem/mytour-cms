/**
 * Created by Hung Cuong on 6/25/2015.
 */
define(['contManager/module'], function (module) {

    'use strict';

    return module.registerDirective('elemScriptAutoFixed', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                var elStoresWidget = $(element);
                var autoHeight = element.data('auto-height') | false;
                var elProperties = {
                    top: elStoresWidget.parent().offset().top,
                    width: elStoresWidget.parent().width()
                };

                $(window).on("scroll resize",(function(){
                    var _cur_top = $(window).scrollTop();
                    var _viewport_height = $(window).height();
                    var _viewport_width = $(window).width();
                    var isFullSceenMode = elStoresWidget.parent('#jarviswidget-fullscreen-mode').length | false;
                    if(_viewport_width > 750){ // If is mobile device, not run this code
                        if(!isFullSceenMode){
                            elProperties.width = elStoresWidget.parent().width();
                            elProperties.top = elStoresWidget.parent().offset().top;
                        }
                        if(_cur_top >= elProperties.top){
                            elStoresWidget.addClass('fixed-widget').css('width', elProperties.width + 'px');
                            if(autoHeight){
                                elStoresWidget.find('.widget-body').css('height', (_viewport_height-50) + 'px')
                            }
                        }else{
                            elStoresWidget.removeClass('fixed-widget').css('width', '100%');
                            if(autoHeight){
                                elStoresWidget.find('.widget-body').css('height', 'auto')
                            }
                        }
                    }else{
                        elStoresWidget.removeClass('fixed-widget').css('width', '100%');
                    }
                }));
            }
        }
    });
});
