define(['contManager/module'], function(module){

    "use strict";

    return module.registerDirective('hotelsList', function(HotelsList){
        return {
            restrict: 'A',
            templateUrl: 'app/contManager/views/hotels.html',
            link: function(scope){
                HotelsList.initialized.then(function(){
                    scope.hotelsList = HotelsList.data;
                    $('.auto-numeric').autoNumeric('init', {mDec: '0',vMin: '0'});
                    $('.datetimepicker').datetimepicker({
                        minDate: new Date(),
                        format: "MMM DD YYYY, HH:mm",
                        locale: 'en',
                        showClear : true
                    });
                    $('.modal').on('hidden.bs.modal', function (e) {
                        if($('.modal').hasClass('in')) {
                            $('body').addClass('modal-open');
                        }else $('body').removeClass('modal-open');
                    });
                    var addHotelValidator = $(".addHotelForm").validate({
                        rules: {
                            stars : {
                                min : 1,
                                max : 5
                            }
                        },
                        messages: {
                            name: "The Hotel name is required and cannot be empty",
                            hotelCity: {
                                required: "Please choose at least one City"
                            }
                        }//
                    });
                    var addRoomValidator = $(".addRoomForm").validate({
                        rules: {
                            discount_percent : {
                                min : 0,
                                max : 100
                            }
                        }
                    });

                    $('.content-index').on('hidden.bs.modal', '.modal.parent-modal', function () {
                        $('.fileinput').fileinput('clear');
                        addHotelValidator.resetForm();
                        addRoomValidator.resetForm();
                        $('.has-error').removeClass('has-error');
                        $('.myErrorClass').removeClass('myErrorClass');
                        $('.has-success').removeClass('has-success');
                        $('.form-group').find(".symbol.ok").removeClass('ok').addClass('required');
                        $(this).data('bs.modal', null);
                        $("ul.nav.nav-tabs.bordered li:first-child").removeClass('active').addClass('active');
                        $("div.tab-content.form-group div:first-child").removeClass('active').addClass('active');
                    });

                    $.validator.addMethod("greaterThanZero", function (value, element) {
                        return this.optional(element) || (parseFloat(replaceAll(',', '', value)) > 0);
                    }, "Must be greater than zero");
                    $.validator.addMethod("greaterThan",
                        function (value, element, param) {
                            var parVal = ($(param).val()) ? $(param).val() : '0';
                            $(param).val(parVal);
                            return this.optional(element) || parseFloat(replaceAll(',', '', value)) >= parseFloat(replaceAll(',', '', parVal));
                        }, 'Must be greater than {0}.');

                    $.validator.addMethod("lessThan",
                        function (value, element, param) {
                            var parVal = ($(param).val()) ? $(param).val() : '0';
                            $(param).val(parVal);
                            return this.optional(element) || parseFloat(replaceAll(',', '', value)) <= parseFloat(replaceAll(',', '', parVal));
                        }, 'Must be less than {0}.');

                    $.validator.addMethod("dateGreaterThan",
                        function(value, element, params) {

                            if (!/Invalid|NaN/.test(new Date(value))) {
                                if (!$(params).val() || !value) return true;
                                return new Date(value) > new Date($(params).val());
                            }
                            return isNaN(value) && isNaN($(params).val())
                                || (Number(value) > Number($(params).val()));
                        },'Must be greater than {0}.');

                    $.validator.addMethod("dateLessThan",
                        function(value, element, params) {

                            if (!/Invalid|NaN/.test(new Date(value))) {
                                if (!$(params).val() || !value) return true;
                                return new Date(value) < new Date($(params).val());
                            }

                            return isNaN(value) && isNaN($(params).val())
                                || (Number(value) > Number($(params).val()));
                        },'Must be less than {0}.');

                    function replaceAll(find, replace, str) {
                        return str.replace(new RegExp(find, 'g'), replace);
                    }
                    $('.fileinput').fileinput();
                    $('.filterHotelByCreatedAt').datetimepicker({
                        format: "MMM DD YYYY, HH:mm",
                        locale: 'en',
                        showClear : true,
                        useCurrent: false
                    }).on("dp.change", function (e) {
                        angular.element($('#hotel_ctrl')).scope().filterHotel();
                    });
                    $("#filterHotelsByCreatedFrom").on("dp.change", function (e) {
                        if (e.date)
                            $('#filterHotelsByCreatedTo').data("DateTimePicker").minDate(e.date);
                        else $('#filterHotelsByCreatedTo').data("DateTimePicker").minDate(false);
                    });
                    $("#filterHotelsByCreatedTo").on("dp.change", function (e) {
                        if (e.date)
                            $('#filterHotelsByCreatedFrom').data("DateTimePicker").maxDate(e.date);
                        else $('#filterHotelsByCreatedFrom').data("DateTimePicker").maxDate(false);
                    });
                    $('#hotelCity, #filterHotelsByCity').select2("destroy").select2({
                        placeholder: "Select City",
                        closeOnSelect: false,
                        minimumInputLength: 2,
                        ajax: {
                            url: appConfig.api_path + "city/getCities",
                            dataType: 'json',
//                quietMillis: 250,
                            data: function (term, page) { // page is the one-based page number tracked by Select2
                                return {
                                    search: term, //search term
                                    page: page // page number
                                };
                            },
                            results: function (data, page) {
                                var more = (page * 30) < data.total_count; // whether or not there are more results available

                                // notice we return the value of more so Select2 knows if more results can be loaded
                                return {results: data.items, more: more};
                            },
                            cache: true
                        },
                        initSelection: function (element, callback) {
                            // the input tag has a value attribute preloaded that points to a preselected repository's id
                            // this function resolves that id attribute to an object that select2 can render
                            // using its formatResult renderer - that way the repository name is shown preselected
                            var id = $(element).val();
                            if (id !== "") {
                                $.ajax(appConfig.api_path + "city/getCitiesSelected", {
                                    dataType: "json",
                                    method: 'get',
                                    data: {id: id},
                                    cache: true
                                }).done(function (data) {
                                    callback(data.items[0]);
                                });
                            }
                        },
                        formatResult: repoFormatResult,
                        formatSelection: repoFormatSelection
                    }).on('change', function (e) {
                        angular.element($('#hotel_ctrl')).scope().filterHotel();
                    });

                    $('#hotel_ctrl').on('click','.delete-row', function (e) {
                        var id = $(this).attr('delete-id');
                        $.smallBox({
                            title: "Cảnh báo!",
                            content: "Are you sure want to delete this hotel? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#hotel_ctrl\")).scope().deleteHotel("+id+");' class='btn btn-danger btn-sm'>Yes</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>No</a></p>",
                            color: "#C46A69",
                            //timeout: 8000,
                            icon: "fa fa-exclamation-triangle swing animated"
                        });
                    });

                    function repoFormatResult(repo) {
                        var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.name + "</div>";
                        markup += "<div class='select2-result-repository__description'>" + repo.hotel_count + " khách sạn</div>";
                        markup += "</div>";
                        return markup;
                    }

                    function repoFormatSelection(repo) {
                        return repo.name;
                    }
                });
            }
        }
    })
});