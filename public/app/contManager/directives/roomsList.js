define(['contManager/module'], function(module){

    "use strict";

    return module.registerDirective('roomsList', function(RoomsList){
        return {
            restrict: 'A',
            templateUrl: 'app/contManager/views/rooms.tpl.html',
            link: function(scope){
                RoomsList.initialized.then(function(){
                    $('#room-category').select2();
                    $('.filterRoomByCreatedAt').datetimepicker({
                        format: "MMM DD YYYY, HH:mm",
                        locale: 'en',
                        showClear : true,
                        useCurrent: false
                    }).on("dp.change", function (e) {
                        angular.element($('#room_ctrl')).scope().filterRoom();
                    });
                    $("#room-discount-start").on("dp.change", function (e) {
                        if (e.date)
                            $('#room-discount-end').data("DateTimePicker").minDate(e.date);
                        else $('#room-discount-end').data("DateTimePicker").minDate(false);
                    });
                    $("#room-discount-end").on("dp.change", function (e) {
                        if (e.date)
                            $('#room-discount-start').data("DateTimePicker").maxDate(e.date);
                        else $('#room-discount-start').data("DateTimePicker").maxDate(false);
                    });

                    $('#filterRoomsByAuthor').select2("destroy").select2({
                        multiple: true,
                        placeholder: "Select Author (email, fullname)",
                        maximumSelectionSize: 0,
                        closeOnSelect: false,
                        minimumInputLength: 2,
                        ajax: {
                            url: appConfig.api_path + "user/listSelect2",
                            dataType: 'json',
                            data: function (term, page) { // page is the one-based page number tracked by Select2
                                return {
                                    search: term, //search term
                                    page: page // page number
                                };
                            },
                            results: function (data, page) {
                                var more = (page * 30) < data.total_count; // whether or not there are more results available

                                // notice we return the value of more so Select2 knows if more results can be loaded
                                return {results: data.items, more: more};
                            },
                            cache: true
                        },
                        initSelection: function (element, callback) {
                            // the input tag has a value attribute preloaded that points to a preselected repository's id
                            // this function resolves that id attribute to an object that select2 can render
                            // using its formatResult renderer - that way the repository name is shown preselected
                            var id = $(element).val();
                            if (id !== "") {

                            }
                        },
                        formatResult: repoFormatResultAuthor,
                        formatSelection: repoFormatSelectionAuthor
                    }).on('change', function (e) {
                        angular.element($('#room_ctrl')).scope().filterRoom();
                    });
                    function repoFormatResultAuthor(repo) {
                        var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.fullname + "</div>";

                        markup += "<div class='select2-result-repository__description'>" + repo.email + "</div>";
                        markup += "</div>";
                        return markup;
                    }

                    function repoFormatSelectionAuthor(repo) {
                        return repo.email;
                    }

                    $(".room-select-hotel").select2("destroy").select2({
                        placeholder: "Hotel name, url",
                        minimumInputLength: 2,
                        // instead of writing the function to execute the request we use Select2's convenient helper
                        ajax: {
                            url: appConfig.api_path + "hotel/getHotels",
                            dataType: "json",
                            quietMillis: 500,
                            delay: 500,
                            data: function (term, page) {
                                return {
                                    // search term
                                    search: term
                                };
                            },
                            results: function (data, page) {
                                // parse the results into the format expected by Select2.
                                // since we are using custom formatting functions we do not need to alter the remote JSON data
                                return {results: data.items};
                            },
                            cache: true
                        },
                        formatResult: repoFormatResultHotel,
                        formatSelection: repoFormatSelectionHotel
                    });

                    function repoFormatResultHotel(repo) {
                        var markup = "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__title'>" + repo.name + "</div>";

                        markup += "<div class='select2-result-repository__description'>" + repo.hotel_url + "</div>";
                        markup += "</div>";
                        return markup;
                    }

                    function repoFormatSelectionHotel(repo) {
                        return repo.name;
                    }
                    $('#room-image').cropImage({
                        handleCompletedFile: handUploadRoomImage,
                        inputId: 'cropRoomImage',
                        modalId: 'changeRoomImageModal',
                        width:240,
                        height:180
                    });
                    function handUploadRoomImage( fileData){
                        angular.element($('#room_ctrl')).scope().$parent.currentRoom.image = fileData;
                    }

                    $('#room_ctrl').on('click','.delete-row', function (e) {
                        var id = $(this).attr('delete-id');
                        $.smallBox({
                            title: "Cảnh báo!",
                            content: "Are you sure want to delete this room? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#room_ctrl\")).scope().deleteRoom("+id+");' class='btn btn-danger btn-sm'>Yes</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>No</a></p>",
                            color: "#C46A69",
                            //timeout: 8000,
                            icon: "fa fa-exclamation-triangle swing animated"
                        });
                    });
                });
            }
        }
    })
});