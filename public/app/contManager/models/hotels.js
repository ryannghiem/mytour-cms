define(['contManager/module'], function (module) {

    'use strict';

    return module.registerFactory('HotelsList', function ($http, $q) {
        var dfd = $q.defer();

        var HotelsList = {
            initialized: dfd.promise,
            data: {}
        };

        $http({
            url: appConfig.api_path + 'hotel',
            method: "GET",
            params: {
                orderBy: 'hotels.created_at',
                orderDir: 'desc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function (response) {
            HotelsList.data.data = response.data.data;
            HotelsList.data.currentPage = 1;
            HotelsList.data.itemsPerPage = 10;
            HotelsList.data.orderBy = 'hotels.created_at';
            HotelsList.data.hotelSearchValue = '';
            HotelsList.data.orderDir = 'desc';
            HotelsList.data.totalHotels = response.data.recordsFiltered;
            dfd.resolve(HotelsList)
        });

        return HotelsList;
        //.withOption('ajax', {
        //    // Either you specify the AjaxDataProp here
        //    dataSrc: 'data',
        //    url: appConfig.api_path + 'hotel/list',
        //    type: 'GET',
        //    complete:function(data) {
        //        console.log(data.responseJSON);
        //        //dataTbl.hotelsList = data.responseJSON.data;
        //        //$scope.hotelsList = data.responseJSON.data;
        //    }
        //})
        // or here
        //.withDataProp('data')
    });
});