
define(['contManager/module'], function (module) {

    'use strict';

    return module.registerFactory('RoomsList', function ($http, $q) {
        var dfd = $q.defer();

        var RoomsList = {
            initialized: dfd.promise,
            data: {}
        };

        RoomsList.data.data = [];
        RoomsList.data.currentPage = 1;
        RoomsList.data.itemsPerPage = 10;
        RoomsList.data.orderBy = 'rooms.created_at';
        RoomsList.data.roomSearchValue = '';
        RoomsList.data.orderDir = 'desc';
        RoomsList.data.totalRooms = 0;
        dfd.resolve(RoomsList);
        return RoomsList;
    });
});