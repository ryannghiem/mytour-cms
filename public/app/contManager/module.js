define(['angular',
    'angular-couch-potato',
    'angular-ui-router', 'angular-x-editable', 'jquery-validation',
    'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'auto-numeric','jasny-bootstrap'], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.contManager', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
            .state('app.contManager', {
                url: '/content',
                data: {
                    title: 'Content Management'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/contManager/views/index.tpl.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                /*Controller*/
                                'contManager/controller/HotelsCtrl',
                                'contManager/controller/RoomsCtrl',
                                'contManager/controller/IndexCtrl',

                                /* Directives*/
                                'modules/forms/directives/input/smartXEditable',
                                'contManager/directives/elemScriptAutoFixedCSS',
                                'contManager/directives/hotelsList',
                                'contManager/directives/roomsList'
                            ])
                        }
                    }
                }
            }
        )
    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});