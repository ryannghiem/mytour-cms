'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-animate',
    'angular-bootstrap',
    'angular-sanitize',
    'smartwidgets',
    'notification',
    //'ui-select2',
    'angular-x-editable',
    //'angular-slugify',
    'moment',
    'moment-timezone',
    'angular-moment',
    'oc.lazyLoad'
], function (ng, couchPotato) {

    var app = ng.module('app', [
        'ngSanitize',
        'scs.couch-potato',
        'oc.lazyLoad',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'xeditable',
        //'slugifier',
        'angularMoment',
        //'ui.select',
        // App
        'app.auth',
        'app.layout',
        // 'app.chat',
        'app.dashboard',
        // 'app.calendar',
        // 'app.inbox',
        'app.graphs',
        'app.tables',
        'app.forms',
        'app.ui',
        'app.widgets',
        'app.maps',
        'app.appViews',
        'app.misc',
        'app.smartAdmin',
        // 'app.eCommerce',
        'app.staffsManager',
        'app.systemManager',
        'app.contManager',
        'app.slidesManagement',
        'app.adsManagement',
        'app.bookingsManagement'
    ]);

    couchPotato.configureApp(app);
    var xhReq;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xhReq = new XMLHttpRequest();
    }
    else{// code for IE6, IE5
        xhReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhReq.open("GET", appConfig.api_path+"auth/token");
    xhReq.send();
    // Token to constant
    app.constant("CSRF_TOKEN", xhReq.responseText);

    app.constant('angularMomentConfig', {
        // preprocess: 'utc', // optional
        // timezone: appConfig.timeZone //'Asia/Ho_Chi_Minh' // optional
    });

    app.config(function ($provide, $httpProvider) {
        //uiSelectConfig.theme = 'bootstrap';
        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q ) {
            var errorCounter = 0;
            function notifyError(rejection){
                if(rejection.status != 0) {
                    if (rejection.status == 404) {
                        /*$.ajax({
                            type: "GET",
                            url: appConfig.api_path + 'user/logout',
                            success: function(res) {
                                $('.divMessageBox').remove();
                                var loginPath = ((window.location.href).split('#'))[0] + '#login';
                                window.location.replace(loginPath);
                            }
                        });*/
                    }
                    var msg = '';
                    if (rejection.status == 500) {
                        msg = 'Có lỗi xảy ra. Xin ấn Ctrl+F5 để load lại trang.';
                    }
                    if(rejection.data && rejection.data.errCode !== undefined || rejection.status == 404){
                        msg = rejection.data.message;
                        if (rejection.status == 404) {
                            msg = rejection.statusText;
                        }
                        $.SmartMessageBox({
                            title: "<i class='fa fa-sign-out txt-color-orangeDark'></i><strong>Access Denied</strong></span>",
                            content: msg,
                            buttons: '[Yes]'
                        }, function (ButtonPressed) {
                            if (ButtonPressed === "Yes") {
                                $.ajax({
                                    type: "GET",
                                    url: appConfig.api_path + 'user/logout',
                                    success: function(res) {
                                        $('.divMessageBox').remove();
                                        var loginPath = ((window.location.href).split('#'))[0] + '#login';
                                        window.location.replace(loginPath);
                                    }
                                });
                            }
                        });
                    }else if (rejection.status != -1) {
                        var $errMsg = msg;
                        if ($.isPlainObject(rejection.data)) {
                            $errMsg = '';
                            $.each(rejection.data, function(k, v) {
                                //display the key and value pair
                                $errMsg += "<p>" + k + ": " + v + "</p>";
                            });
                        }

                        $.bigBox({
                            title: 'Error code: ' + appConfig.http_status_codes[rejection.status],
                            content: $errMsg ? $errMsg : 'Có lỗi xảy ra. Xin ấn Ctrl+F5 để load lại trang.',//rejection.data.message,//'Cannot load any resource, please try again. Error code: ' + rejection.status + '<br />' + msg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            number: ++errorCounter,
                            timeout: 60000
                        });
                    }
                }
            }

            return {
                // On request failure
                requestError: function (rejection) {
                    // show notification
                    notifyError(rejection);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                },
                // On response failure
                responseError: function (rejection) {
                    // show notification
                    notifyError(rejection);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                }
            };
        });

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');
    });

    app.run(function ($couchPotato, $rootScope, $state, $stateParams, $location, $http, amMoment) {
        app.lazy = $couchPotato;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        amMoment.changeLocale('en');
        $rootScope.$on('$stateChangeStart', function() {
            $http.get(appConfig.api_path + 'user/session?app').then(function(response){
                var $res = response.data;
                if(angular.isUndefined($res) || angular.isUndefined($res.email) || $res.email.length < 5){
                    var nextUrl = $location.path();
                    if (nextUrl != '/forgot-password' && nextUrl != '/login'){
                        angular.isDefined($res.redirect) ? $location.path($res.redirect) : $location.path("/login?next="+nextUrl);
                    }
                }else{
                    /*if ($location.path() == '/login') {
                        $state.go(appConfig.default_page);
                    }*/
                    $rootScope.authenticated = true;
                    $rootScope.userLogin = $res;
                    $rootScope.userLogin.avatar = $res.avatar ? $res.avatar : appConfig.default_avatar;
                }
            });
        });
    });

    app.filter('ageFilter', function() {
        function calculateAge(birthday) { // birthday is a date
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs); // miliseconds from epoch
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }
        function monthDiff(d1, d2) {
            if (d1 < d2){
                var months = d2.getMonth() - d1.getMonth();
                return months <= 0 ? 0 : months;
            }
            return 0;
        }
        return function(birthdate) {
            var age = calculateAge(birthdate);
            if (age == 0)
                return monthDiff(birthdate, new Date()) + ' months';
            return age;
        };
    });

    return app;
});
