

define(['systemManager/module'], function(module){

    "use strict";

    return module.registerDirective('domainsList', function(domainsList){
        return {
            restrict: 'A',
            templateUrl: 'app/systemManager/domain/views/domains.tpl.html',
            link: function(scope){
                domainsList.initialized.then(function(){
                    scope.domainsList = domainsList.data;
                    setTimeout(function () {
                        var addDomainValidator = $(".addDomainForm").validate({
                            lang: 'vi',
                            rules: {
                                domain_code: {
                                    required: true,
                                    maxlength: 3
                                }
                            },
                            errorElement: "span", // contain the error msg in a small tag
                            errorClass: 'help-block myErrorClass',
                            errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                                } else if (element.hasClass("ckeditor") || element.hasClass("date-required")) {
                                    error.appendTo($(element).closest('.form-group'));
                                } else {
                                    error.insertAfter(element);
                                    // for other inputs, just perform default behavior
                                }
                            },
                            highlight: function (element, errorClass, validClass) {
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                                } else {
                                    $(element).closest('.help-block').removeClass('valid');
                                    // display OK icon
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                                    // add the Bootstrap error class to the control group
                                }
                            },
                            unhighlight: function (element, errorClass, validClass) {
                                // revert the change done by hightlight
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                                } else {
                                    $(element).closest('.form-group').removeClass('has-error');
                                    // set error class to the control group
                                }
                            },
                            success: function (label, element) {
                                label.addClass('help-block valid');
                                // mark the current input as valid and display OK icon
                                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                            }
                        });

                        $('body').on('hidden.bs.modal', '#modal-add-domain', function () {
                            $('.fileinput').fileinput('clear');
                            addDomainValidator.resetForm();
                            $('.has-error').removeClass('has-error');
                            $('.myErrorClass').removeClass('myErrorClass');
                            $('.has-success').removeClass('has-success');
                            $('.form-group').find(".symbol.ok").removeClass('ok').addClass('required');
                            $(this).data('bs.modal', null);
                        });
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            }else $('body').removeClass('modal-open');
                        });
                        $('.table').on('click','.delete-row', function (e) {
                            var id = $(this).attr('delete-id');
                            $.smallBox({
                                title: "Cảnh báo!",
                                content: "Bạn chắc chắn muốn xóa không? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#domain_ctrl\")).scope().deleteDomain("+id+");' class='btn btn-danger btn-sm'>Có</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>Không</a></p>",
                                color: "#C46A69",
                                //timeout: 8000,
                                icon: "fa fa-exclamation-triangle swing animated"
                            });
                        });

                        $('#avatar-image').cropImage({
                            handleCompletedFile: handUploadAvatarImage,
                            inputId: 'cropDomainImage',
                            modalId: 'changeImageModal',
                            width:200,
                            height:200
                        });
                        function handUploadAvatarImage( fileData){
                            scope.currentDomain.logo = fileData;
                            scope.$apply();
                        }
                    },1000);
                });
            }
        }
    })
});
