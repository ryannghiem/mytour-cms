
define(['systemManager/module'], function (module) {

    'use strict';

    module.registerController('domainsIndexCtrl', function ($rootScope, $scope, $http, CSRF_TOKEN, $location, $q) {
/*
        Check permission access Area
        Skip check if user role is sa (Super Admin)
*/
        if(!$rootScope.userLogin || $rootScope.userLogin.user_type <= 1){
            $.SmartMessageBox({
                title: "<i class='fa fa-frown-o txt-color-orangeDark'></i> <strong>Access Denied</strong></span>",
                content: 'Bạn không có quyền truy cập',
                buttons: '[Yes]'
            }, function (ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    $location.path('/stores');
                }
            });
        }

    });
});
