
define(['systemManager/module'], function (module) {

    'use strict';

    module.registerController('DomainsCtrl', function ($rootScope, $scope, $http, CSRF_TOKEN, $q, moment) {

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.uppercaseDomainCode = function () {
            $scope.currentDomain.domain_code = $scope.currentDomain.domain_code.toUpperCase().substr(0, 3);
        };

        $scope.clearFilterDomains = function () {
            $('.btn-domain-refresh i').addClass('fa-spin');
            $('.domain-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'domain',
                method: "GET",
                params: {
                    orderBy: 'domains.name',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.domainsList.itemsPerPage,
                    search: ''
                }
            }).then(function (response) {
                $('.domain-article .tables_processing').hide();
                $('.btn-domain-refresh i').removeClass('fa-spin');
                $scope.domainsList.data = response.data.data;
                $scope.domainsList.currentPage = 1;
                $scope.domainsList.orderBy = 'domains.name';
                $scope.domainsList.domainSearchValue = '';
                $scope.domainsList.orderDir = 'desc';
                $scope.domainsList.totalDomains = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterDomain = function () {
            $('.domain-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'domain',
                method: "GET",
                params: {
                    orderBy: $scope.domainsList.orderBy,
                    orderDir: $scope.domainsList.orderDir,
                    start: 0,
                    length: $scope.domainsList.itemsPerPage,
                    search: $scope.domainsList.domainSearchValue
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.domain-article .tables_processing').hide();
                $scope.domainsList.data = response.data.data;
                $scope.domainsList.currentPage = 1;
                $scope.domainsList.totalDomains = response.data.recordsFiltered;
            });
        };

        $scope.setPageDomain = function (page) {
            $('.domain-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'domain',
                method: "GET",
                params: {
                    orderBy: $scope.domainsList.orderBy,
                    orderDir: $scope.domainsList.orderDir,
                    start: (page - 1) * $scope.domainsList.itemsPerPage,
                    length: $scope.domainsList.itemsPerPage,
                    search: $scope.domainsList.domainSearchValue
                }
            }).then(function (response) {
                $('.domain-article .tables_processing').hide();
                $scope.domainsList.data = response.data.data;
                $scope.domainsList.currentPage = page;
            });
        };

        $scope.sortDomain = function (colName) {
            $('.domain-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'domain',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.domainsList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.domainsList.currentPage - 1) * $scope.domainsList.itemsPerPage,
                    length: $scope.domainsList.itemsPerPage,
                    search: $scope.domainsList.domainSearchValue
                }
            }).then(function (response) {
                $('.domain-article .tables_processing').hide();
                $scope.domainsList.data = response.data.data;
                $scope.domainsList.orderBy = colName;
                $scope.domainsList.orderDir = $scope.domainsList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.checkDuplicateCode = function () {
            canceler.resolve();
            canceler = $q.defer();
            $('.domain-code').addClass('input-checking');
            $http({
                url: appConfig.api_path + 'domain/checkDuplicateCode',
                method: "GET",
                params: {
                    id : $scope.currentDomain.id,
                    domain_code : $scope.currentDomain.domain_code
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.domain-code').removeClass('input-checking');
                $scope.currentDomain.domainCodeExists = response.data.status;
                if (response.data.status == true) {
                    $scope.currentDomain.domainsDuplicate = response.data.domains;
                }
            });
        };

        $scope.addDomain = function () {
            $scope.currentDomain = {};
            $scope.currentDomain.addMode = true;
            $scope.currentDomain.status = 1;
            $scope.currentDomain.limit_stores = 1;
            $('#modal-add-domain').modal('show');
        };

        $scope.editDomain = function (index, domain) {
            $scope.currentDomain = {};
            $scope.currentDomain.addMode = false;

            var $id = domain.id;
            $http({
                url: appConfig.api_path + 'domain/'+ $id +'/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentDomain = response.data.data;
            });
            $('#modal-add-domain').modal('show');
        };

        // Save change
        $scope.saveDomain = function () {
            var formValidation = $('.addDomainForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            $('#modal-add-domain').modal('hide');
            $('.domain-article .tables_processing').show();
            $http.put(appConfig.api_path + 'domain/update',$scope.currentDomain
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterDomain();
                    }
                });
        };

        $scope.deleteDomain = function (id) {
            $('.domain-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'domain/' + id,
                method: "DELETE"
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.domain-article .tables_processing').text('Xoá thành công!');
                    $scope.filterDomain();
                } else $('.domain-article .tables_processing').text("Không thể xóa cửa hàng này!");
                setTimeout(function () {
                    $('.domain-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.domain-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'domain/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.domain-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("domainColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.created_at = true;
                $scope.columnsShown.description = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("domainColumnsShown",JSON.stringify($scope.columnsShown));
        };
    });
});

