
define(['systemManager/module'], function (module) {

    'use strict';

    return module.registerFactory('domainsList', function ($http, $q) {
        var dfd = $q.defer();

        var domainsList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'domain',
            method: "GET",
            params: {
                orderBy: 'domains.name',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            domainsList.data.data = response.data.data;
            domainsList.data.currentPage = 1;
            domainsList.data.itemsPerPage = 10;
            domainsList.data.orderBy = 'domains.name';
            domainsList.data.orderDir = 'asc';
            domainsList.data.domainSearchValue = '';
            domainsList.data.totalDomains = response.data.recordsFiltered;
            dfd.resolve(domainsList)
        });

        return domainsList;
    });
});
