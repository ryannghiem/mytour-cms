
define(['systemManager/module'], function (module) {

    'use strict';

    return module.registerFactory('packagesList', function ($http, $q) {
        var dfd = $q.defer();

        var packagesList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'package',
            method: "GET",
            params: {
                orderBy: 'packages.ordering',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            packagesList.data.data = response.data.data;
            packagesList.data.currentPage = 1;
            packagesList.data.itemsPerPage = 10;
            packagesList.data.orderBy = 'packages.ordering';
            packagesList.data.orderDir = 'asc';
            packagesList.data.packageSearchValue = '';
            packagesList.data.totalPackages = response.data.recordsFiltered;
            dfd.resolve(packagesList)
        });

        return packagesList;
    });
});
