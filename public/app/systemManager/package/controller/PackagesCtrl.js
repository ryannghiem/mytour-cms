
define(['systemManager/module'], function (module) {

    'use strict';

    module.registerController('PackagesCtrl', function ($rootScope, $scope, $http, CSRF_TOKEN, $q, moment) {

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.uppercasePackageCode = function () {
            $scope.currentPackage.package_code = $scope.currentPackage.package_code.toUpperCase().substr(0, 3);
        };

        $scope.clearFilterPackages = function () {
            $('.btn-package-refresh i').addClass('fa-spin');
            $('.package-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'package',
                method: "GET",
                params: {
                    orderBy: 'packages.ordering',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.packagesList.itemsPerPage,
                    search: ''
                }
            }).then(function (response) {
                $('.package-article .tables_processing').hide();
                $('.btn-package-refresh i').removeClass('fa-spin');
                $scope.packagesList.data = response.data.data;
                $scope.packagesList.currentPage = 1;
                $scope.packagesList.orderBy = 'packages.ordering';
                $scope.packagesList.packageSearchValue = '';
                $scope.packagesList.orderDir = 'desc';
                $scope.packagesList.totalPackages = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterPackage = function () {
            $('.package-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'package',
                method: "GET",
                params: {
                    orderBy: $scope.packagesList.orderBy,
                    orderDir: $scope.packagesList.orderDir,
                    start: 0,
                    length: $scope.packagesList.itemsPerPage,
                    search: $scope.packagesList.packageSearchValue
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.package-article .tables_processing').hide();
                $scope.packagesList.data = response.data.data;
                $scope.packagesList.currentPage = 1;
                $scope.packagesList.totalPackages = response.data.recordsFiltered;
            });
        };

        $scope.setPagePackage = function (page) {
            $('.package-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'package',
                method: "GET",
                params: {
                    orderBy: $scope.packagesList.orderBy,
                    orderDir: $scope.packagesList.orderDir,
                    start: (page - 1) * $scope.packagesList.itemsPerPage,
                    length: $scope.packagesList.itemsPerPage,
                    search: $scope.packagesList.packageSearchValue
                }
            }).then(function (response) {
                $('.package-article .tables_processing').hide();
                $scope.packagesList.data = response.data.data;
                $scope.packagesList.currentPage = page;
            });
        };

        $scope.sortPackage = function (colName) {
            $('.package-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'package',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.packagesList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.packagesList.currentPage - 1) * $scope.packagesList.itemsPerPage,
                    length: $scope.packagesList.itemsPerPage,
                    search: $scope.packagesList.packageSearchValue
                }
            }).then(function (response) {
                $('.package-article .tables_processing').hide();
                $scope.packagesList.data = response.data.data;
                $scope.packagesList.orderBy = colName;
                $scope.packagesList.orderDir = $scope.packagesList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.checkDuplicateCode = function () {
            canceler.resolve();
            canceler = $q.defer();
            $('.package-code').addClass('input-checking');
            $http({
                url: appConfig.api_path + 'package/checkDuplicateCode',
                method: "GET",
                params: {
                    id : $scope.currentPackage.id,
                    package_code : $scope.currentPackage.package_code
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.package-code').removeClass('input-checking');
                $scope.currentPackage.packageCodeExists = response.data.status;
                if (response.data.status == true) {
                    $scope.currentPackage.packagesDuplicate = response.data.packages;
                }
            });
        };

        $scope.addPackage = function () {
            $scope.currentPackage = {};
            $scope.currentPackage.addMode = true;
            $scope.currentPackage.status = 1;
            $scope.currentPackage.limit_stores = 1;
            $('#modal-add-package').modal('show');
        };

        $scope.editPackage = function (index, $package) {
            $scope.currentPackage = {};
            $scope.currentPackage.addMode = false;

            var $id = $package.id;
            $http({
                url: appConfig.api_path + 'package/'+ $id +'/edit',
                method: "GET"
            }).then(function (response) {
                $scope.currentPackage = response.data.data;
            });
            $('#modal-add-package').modal('show');
        };

        // Save change
        $scope.savePackage = function () {
            var formValidation = $('.addPackageForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            $('#modal-add-package').modal('hide');
            $('.package-article .tables_processing').show();
            $http.put(appConfig.api_path + 'package/update',$scope.currentPackage
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterPackage();
                    }
                });
        };

        $scope.deletePackage = function (id) {
            $('.package-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'package/' + id,
                method: "DELETE"
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.package-article .tables_processing').text('Xoá thành công!');
                    $scope.filterPackage();
                } else $('.package-article .tables_processing').text("Không thể xóa cửa hàng này!");
                setTimeout(function () {
                    $('.package-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.package-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'package/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.package-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("packageColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.created_at = true;
                $scope.columnsShown.description = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("packageColumnsShown",JSON.stringify($scope.columnsShown));
        };
    });
});

