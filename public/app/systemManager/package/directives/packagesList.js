

define(['systemManager/module'], function(module){

    "use strict";

    return module.registerDirective('packagesList', function(packagesList){
        return {
            restrict: 'A',
            templateUrl: 'app/systemManager/package/views/packages.tpl.html',
            link: function(scope){
                packagesList.initialized.then(function(){
                    scope.packagesList = packagesList.data;
                    setTimeout(function () {
                        var addPackageValidator = $(".addPackageForm").validate({
                            lang: 'vi',
                            rules: {
                                package_code: {
                                    required: true,
                                    maxlength: 3
                                }
                            },
                            errorElement: "span", // contain the error msg in a small tag
                            errorClass: 'help-block myErrorClass',
                            errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                                    error.insertAfter($(element).closest('.form-group').children('div').children().last());
                                } else if (element.hasClass("ckeditor") || element.hasClass("date-required")) {
                                    error.appendTo($(element).closest('.form-group'));
                                } else {
                                    error.insertAfter(element);
                                    // for other inputs, just perform default behavior
                                }
                            },
                            highlight: function (element, errorClass, validClass) {
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                                } else {
                                    $(element).closest('.help-block').removeClass('valid');
                                    // display OK icon
                                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                                    // add the Bootstrap error class to the control group
                                }
                            },
                            unhighlight: function (element, errorClass, validClass) {
                                // revert the change done by hightlight
                                var elem = $(element);
                                if (elem.hasClass("select2-offscreen")) {
                                    $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                                } else {
                                    $(element).closest('.form-group').removeClass('has-error');
                                    // set error class to the control group
                                }
                            },
                            success: function (label, element) {
                                label.addClass('help-block valid');
                                // mark the current input as valid and display OK icon
                                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                            }
                        });
                        $('body').on('hidden.bs.modal', '#modal-add-package', function () {
                            $('.fileinput').fileinput('clear');
                            if (addPackageValidator) addPackageValidator.resetForm();
                            $('.has-error').removeClass('has-error');
                            $('.myErrorClass').removeClass('myErrorClass');
                            $('.has-success').removeClass('has-success');
                            $('.form-group').find(".symbol.ok").removeClass('ok').addClass('required');
                            $(this).data('bs.modal', null);
                        });
                        $('.modal').on('hidden.bs.modal', function (e) {
                            if($('.modal').hasClass('in')) {
                                $('body').addClass('modal-open');
                            }else $('body').removeClass('modal-open');
                        });
                        $('.package-article').on('click','.delete-row', function (e) {
                            var id = $(this).attr('delete-id');
                            console.log('a');
                            $.smallBox({
                                title: "Cảnh báo!",
                                content: "Bạn chắc chắn muốn xóa không? <p class='text-align-right'><a href='javascript:void(0);' onclick='angular.element($(\"#package_ctrl\")).scope().deletePackage("+id+");' class='btn btn-danger btn-sm'>Có</a> <a href='javascript:void(0);' class='btn btn-default btn-sm'>Không</a></p>",
                                color: "#C46A69",
                                icon: "fa fa-exclamation-triangle swing animated"
                            });
                        });

                        $('#avatar-image').cropImage({
                            handleCompletedFile: handUploadAvatarImage,
                            inputId: 'cropPackageImage',
                            modalId: 'changeImageModal',
                            width:200,
                            height:200
                        });
                        function handUploadAvatarImage( fileData){
                            scope.currentPackage.logo = fileData;
                            scope.$apply();
                        }
                    },1000);
                });
            }
        }
    })
});
