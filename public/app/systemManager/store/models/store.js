
define(['systemManager/module'], function (module) {

    'use strict';

    return module.registerFactory('storesList', function ($http, $q) {
        var dfd = $q.defer();

        var storesList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'store/list',
            method: "GET",
            params: {
                orderBy: 'stores.name',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            storesList.data.data = response.data.data;
            storesList.data.currentPage = 1;
            storesList.data.itemsPerPage = 10;
            storesList.data.orderBy = 'stores.name';
            storesList.data.orderDir = 'asc';
            storesList.data.storeSearchValue = '';
            storesList.data.totalStores = response.data.recordsFiltered;
            dfd.resolve(storesList)
        });

        return storesList;
    });
});
