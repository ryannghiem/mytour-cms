
define(['systemManager/module'], function (module) {

    'use strict';

    module.registerController('StoresCtrl', function ($rootScope, $scope, $http, CSRF_TOKEN, $q, moment) {

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];
        $scope.currentDate = new Date();
        $scope.uppercaseStoreCode = function () {
            $scope.currentStore.store_code = $scope.currentStore.store_code.toUpperCase().substr(0, 3);
        };

        $scope.clearFilterStores = function () {
            $('.btn-store-refresh i').addClass('fa-spin');
            $('.store-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'store/list',
                method: "GET",
                params: {
                    orderBy: 'stores.name',
                    orderDir: 'desc',
                    start: 0,
                    length: $scope.storesList.itemsPerPage,
                    search: ''
                }
            }).then(function (response) {
                $('.store-article .tables_processing').hide();
                $('.btn-store-refresh i').removeClass('fa-spin');
                $scope.storesList.data = response.data.data;
                $scope.storesList.currentPage = 1;
                $scope.storesList.orderBy = 'stores.name';
                $scope.storesList.storeSearchValue = '';
                $scope.storesList.orderDir = 'desc';
                $scope.storesList.totalStores = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterStore = function () {
            $('.store-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'store/list',
                method: "GET",
                params: {
                    orderBy: $scope.storesList.orderBy,
                    orderDir: $scope.storesList.orderDir,
                    start: 0,
                    length: $scope.storesList.itemsPerPage,
                    search: $scope.storesList.storeSearchValue
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.store-article .tables_processing').hide();
                $scope.storesList.data = response.data.data;
                $scope.storesList.currentPage = 1;
                $scope.storesList.totalStores = response.data.recordsFiltered;
            });
        };

        $scope.setPageStore = function (page) {
            $('.store-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'store/list',
                method: "GET",
                params: {
                    orderBy: $scope.storesList.orderBy,
                    orderDir: $scope.storesList.orderDir,
                    start: (page - 1) * $scope.storesList.itemsPerPage,
                    length: $scope.storesList.itemsPerPage,
                    search: $scope.storesList.storeSearchValue
                }
            }).then(function (response) {
                $('.store-article .tables_processing').hide();
                $scope.storesList.data = response.data.data;
                $scope.storesList.currentPage = page;
            });
        };

        $scope.sortStore = function (colName) {
            $('.store-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'store/list',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.storesList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.storesList.currentPage - 1) * $scope.storesList.itemsPerPage,
                    length: $scope.storesList.itemsPerPage,
                    search: $scope.storesList.storeSearchValue
                }
            }).then(function (response) {
                $('.store-article .tables_processing').hide();
                $scope.storesList.data = response.data.data;
                $scope.storesList.orderBy = colName;
                $scope.storesList.orderDir = $scope.storesList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.checkDuplicateCode = function () {
            canceler.resolve();
            canceler = $q.defer();
            $('.store-code').addClass('input-checking');
            $http({
                url: appConfig.api_path + 'store/checkDuplicateCode',
                method: "GET",
                params: {
                    id : $scope.currentStore.id,
                    store_code : $scope.currentStore.store_code
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.store-code').removeClass('input-checking');
                $scope.currentStore.storeCodeExists = response.data.status;
                if (response.data.status == true) {
                    $scope.currentStore.storesDuplicate = response.data.stores;
                }
            });
        };

        $scope.addStore = function () {
            $scope.currentStore = {};
            $scope.currentStore.addMode = true;
            $scope.currentStore.status = 1;
            $(".store-select-domain").select2('val','');
            $('#modal-add-store').modal('show');
        };

        $scope.editStore = function (index, store) {
            $scope.currentStore = {};
            $scope.currentStore.addMode = false;
            var $id = store.id;
            $http({
                url: appConfig.api_path + 'store/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentStore = response.data.data;
                if ($scope.currentStore.start_actived) {
                    $scope.currentStore.start_actived = moment($scope.currentStore.start_actived).format('DD/MM/YYYY');
                }
                if ($scope.currentStore.expire_date) {
                    $scope.currentStore.expire_date = moment($scope.currentStore.expire_date).format('DD/MM/YYYY');
                }
                $(".store-select-domain").select2('val', $scope.currentStore.domain_id);
            });
            $('#modal-add-store').modal('show');
        };

        // Save change
        $scope.saveStore = function () {
            var formValidation = $('.addStoreForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            $('#modal-add-store').modal('hide');
            $('.store-article .tables_processing').show();
            if ($('#store-start-work').val()) {
                $scope.currentStore.start_actived = moment($('#store-start-work').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.currentStore.start_actived = null;
            if ($('#store-expire-work').val()) {
                $scope.currentStore.expire_date = moment($('#store-expire-work').val(), 'DD/MM/YYYY').format('YYYY-MM-DD, HH:mm');
            } else $scope.currentStore.expire_date = null;
            $scope.currentStore.domain_id = $(".store-select-domain").select2('val');//.toString();
            $http.post(appConfig.api_path + 'store/update',$scope.currentStore
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterStore();
                    }
                });
        };

        $scope.deleteStore = function (id) {
            $('.store-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'store/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.storesList.orderBy,
                    orderDir: $scope.storesList.orderDir,
                    start: ($scope.storesList.currentPage - 1) * $scope.storesList.itemsPerPage,
                    length: $scope.storesList.itemsPerPage,
                    search: $scope.storesList.storeSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.store-article .tables_processing').text('Xoá thành công!');
                    $scope.filterStore();
                } else $('.store-article .tables_processing').text("Không thể xóa cửa hàng này!");
                setTimeout(function () {
                    $('.store-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.store-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'store/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.store-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("storeColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.domain_name = true;
                $scope.columnsShown.phone_mobile = true;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("storeColumnsShown",JSON.stringify($scope.columnsShown));
        };

        $scope.differenceInDays = function() {

            var dt1 = $scope.firstdate.split('/'),
                dt2 = $scope.seconddate.split('/'),
                one = new Date(dt1[2], dt1[1]-1, dt1[0]),
                two = new Date(dt2[2], dt2[1]-1, dt2[0]);

            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisBetween = two.getTime() - one.getTime();
            var days = millisBetween / millisecondsPerDay;

            return Math.floor(days);
        };
        $scope.color = function() {
            return ($scope.differenceInDays() > 10) ? 'green' : 'red';
        };
    });
});

