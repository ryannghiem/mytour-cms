
define(['systemManager/module'], function(module){

    "use strict";

    return module.registerDirective('changePassList', function(){
        return {
            restrict: 'A',
            templateUrl: 'app/systemManager/changePass/views/changePass.tpl.html',
            link: function(scope){
                setTimeout(function () {
                    var changePassValidator = $(".changePassForm").validate({
                        lang: 'vi',
                        rules : {
                            password_old : {
                                minlength : 6,
                                maxlength : 32
                            },
                            password_new : {
                                minlength : 6,
                                maxlength : 32
                            },
                            password_confirm : {
                                minlength : 6,
                                maxlength : 32,
                                equalTo : "#password_new"
                            }
                        },
                        errorElement: "span", // contain the error msg in a small tag
                        errorClass: 'help-block myErrorClass',
                        errorPlacement: function (error, element) { // render error placement for each input type
                            if (element.attr("type") == "radio" || element.attr("type") == "checkbox" || element.attr("type") == "file") { // for chosen elements, need to insert the error after the chosen container
                                error.insertAfter($(element).closest('.form-group').children('div').children().last());
                            } else if (element.hasClass("ckeditor") || element.hasClass("date-required")) {
                                error.appendTo($(element).closest('.form-group'));
                            } else {
                                error.insertAfter(element);
                                // for other inputs, just perform default behavior
                            }
                        },
                        highlight: function (element, errorClass, validClass) {
                            var elem = $(element);
                            if (elem.hasClass("select2-offscreen")) {
                                $("#s2id_" + elem.attr("id") + " ul").addClass(errorClass);
                            } else {
                                $(element).closest('.help-block').removeClass('valid');
                                // display OK icon
                                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                                // add the Bootstrap error class to the control group
                            }
                        },
                        unhighlight: function (element, errorClass, validClass) {
                            // revert the change done by hightlight
                            var elem = $(element);
                            if (elem.hasClass("select2-offscreen")) {
                                $("#s2id_" + elem.attr("id") + " ul").removeClass(errorClass);
                            } else {
                                $(element).closest('.form-group').removeClass('has-error');
                                // set error class to the control group
                            }
                        },
                        success: function (label, element) {
                            label.addClass('help-block valid');
                            // mark the current input as valid and display OK icon
                            $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                        }
                    });

                },1000);
            }
        }
    })
});
