
define(['systemManager/module'], function (module) {

    'use strict';

    module.registerController('ChangePassCtrl', function ($rootScope, $scope, $http, CSRF_TOKEN, $q, moment) {
        $scope.currentUser = {};
        // Save change
        $scope.changePassword = function () {
            var formValidation = $('.changePassForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            $('.btn-change').text('Đang lưu').addClass('disabled');
            $http.post(appConfig.api_path + 'user/change-pass',$scope.currentUser
            ).then(function (response) {
                if (response.data.msg == true) {
                    $scope.msgSubmit = 'Thay đổi mật khẩu thành công!';
                }else {
                    $scope.msgSubmit = response.data.msg;
                }
                setTimeout(function () {
                    $scope.msgSubmit = '';
                },5000);
                    $('.btn-change').text('Thay đổi').removeClass('disabled');
            });
        };

    });
});

