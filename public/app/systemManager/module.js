define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.systemManager', ['ui.router','brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.systemManager', {
            abstract: true,
            data: {title: 'Quản lý hệ thống'}
        })
        // Childs
        .state('app.systemManager.store', {
            url: '/stores',
            data: {title: 'Quản lý cửa hàng'},
            views: {
                "content@app": {
                    templateUrl: 'app/systemManager/store/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'systemManager/store/controller/storesIndexCtrl',
                            'systemManager/store/controller/StoresCtrl',

                            'systemManager/store/directives/storesList'
                        ])
                    }
                }
            }
        })
        .state('app.systemManager.domain', {
            url: '/domains',
            data: {title: 'Quản lý tên miền'},
            views: {
                "content@app": {
                    templateUrl: 'app/systemManager/domain/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'systemManager/domain/controller/domainsIndexCtrl',
                            'systemManager/domain/controller/DomainsCtrl',

                            'systemManager/domain/directives/domainsList'
                        ])
                    }
                }
            }
        })
        .state('app.systemManager.package', {
            url: '/packages',
            data: {title: 'Quản lý gói cước'},
            views: {
                "content@app": {
                    templateUrl: 'app/systemManager/package/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'systemManager/package/controller/packagesIndexCtrl',
                            'systemManager/package/controller/PackagesCtrl',

                            'systemManager/package/directives/packagesList'
                        ])
                    }
                }
            }
        })
        .state('app.systemManager.changePass', {
            url: '/change-password',
            data: {title: 'Thay đổi mật khẩu'},
            views: {
                "content@app": {
                    templateUrl: 'app/systemManager/changePass/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'systemManager/changePass/controller/changePassIndexCtrl',
                            'systemManager/changePass/controller/ChangePassCtrl',
                            'systemManager/changePass/directives/changePassList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});