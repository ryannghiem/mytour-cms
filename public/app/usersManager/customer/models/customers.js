
define(['usersManager/module'], function (module) {

    'use strict';

    return module.registerFactory('customersList', function ($http, $q) {
        var dfd = $q.defer();

        var customersList = {
            initialized: dfd.promise,
            data: {}
        };
        $http({
            url: appConfig.api_path + 'customer/list',
            method: "GET",
            params: {
                orderBy: 'customers.lastname',
                orderDir: 'asc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function(response){
            customersList.data.data = response.data.data;
            customersList.data.currentPage = 1;
            customersList.data.itemsPerPage = 10;
            customersList.data.orderBy = 'customers.lastname';
            customersList.data.orderDir = 'asc';
            customersList.data.customerSearchValue = '';
            customersList.data.totalCustomers = response.data.recordsFiltered;
            dfd.resolve(customersList)
        });

        return customersList;
    });
});
