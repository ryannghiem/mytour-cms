
define(['usersManager/module'], function (module) {

    'use strict';

    module.registerController('CustomersCtrl', function ($scope, $http, CSRF_TOKEN, $q, moment) {

        $scope.uppercaseUsername = function () {
            $scope.currentCustomer.username = $scope.currentCustomer.username.toUpperCase();
        };

        $scope.generateFullname = function () {
            if (!$scope.currentCustomer.id)
            $scope.currentCustomer.fullname = $scope.currentCustomer.username;
        };

        $scope.selectStatus = [
            {value: 1, text: 'Hoạt động'},
            {value: 0, text: 'Không hoạt động'}
        ];

        $scope.clearFilterCustomers = function () {
            $('.btn-customer-refresh i').addClass('fa-spin');
            $('.customer-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'customer/list',
                method: "GET",
                params: {
                    orderBy: 'customers.lastname',
                    orderDir: 'asc',
                    start: 0,
                    length: $scope.customersList.itemsPerPage,
                    search: ''
                }
            }).then(function (response) {
                $('.customer-article .tables_processing').hide();
                $('.btn-customer-refresh i').removeClass('fa-spin');
                $scope.customersList.data = response.data.data;
                $scope.customersList.currentPage = 1;
                $scope.customersList.orderBy = 'customers.lastname';
                $scope.customersList.customerSearchValue = '';
                $scope.customersList.orderDir = 'asc';
                $scope.customersList.totalCustomers = response.data.recordsFiltered;
            });
        };

        var canceler = $q.defer();
        $scope.filterCustomer = function () {
            $('.customer-article .tables_processing').show();
            canceler.resolve();
            canceler = $q.defer();
            $http({
                url: appConfig.api_path + 'customer/list',
                method: "GET",
                params: {
                    orderBy: $scope.customersList.orderBy,
                    orderDir: $scope.customersList.orderDir,
                    start: 0,
                    length: $scope.customersList.itemsPerPage,
                    search: $scope.customersList.customerSearchValue
                },
                timeout: canceler.promise
            }).then(function (response) {
                $('.customer-article .tables_processing').hide();
                $scope.customersList.data = response.data.data;
                $scope.customersList.currentPage = 1;
                $scope.customersList.totalCustomers = response.data.recordsFiltered;
            });
        };

        $scope.setPageCustomer = function (page) {
            $('.customer-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'customer/list',
                method: "GET",
                params: {
                    orderBy: $scope.customersList.orderBy,
                    orderDir: $scope.customersList.orderDir,
                    start: (page - 1) * $scope.customersList.itemsPerPage,
                    length: $scope.customersList.itemsPerPage,
                    search: $scope.customersList.customerSearchValue
                }
            }).then(function (response) {
                $('.customer-article .tables_processing').hide();
                $scope.customersList.data = response.data.data;
                $scope.customersList.currentPage = page;
            });
        };

        $scope.sortCustomer = function (colName) {
            $('.customer-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'customer/list',
                method: "GET",
                params: {
                    orderBy: colName,
                    orderDir: $scope.customersList.orderDir == 'asc' ? 'desc' : 'asc',
                    start: ($scope.customersList.currentPage - 1) * $scope.customersList.itemsPerPage,
                    length: $scope.customersList.itemsPerPage,
                    search: $scope.customersList.customerSearchValue
                }
            }).then(function (response) {
                $('.customer-article .tables_processing').hide();
                $scope.customersList.data = response.data.data;
                $scope.customersList.orderBy = colName;
                $scope.customersList.orderDir = $scope.customersList.orderDir == 'asc' ? 'desc' : 'asc';
            });
        };

        $scope.addCustomer = function () {
            $scope.currentCustomer = {};
            $scope.currentCustomer.addMode = true;
            $scope.currentCustomer.gender = 1;
            $scope.currentCustomer.status = 1;
            $('#modal-add-customer').modal('show');
        };

        $scope.editCustomer = function (index, customer) {
            $scope.currentCustomer = {};
            $scope.currentCustomer.addMode = false;

            var $id = customer.id;
            $http({
                url: appConfig.api_path + 'customer/edit/' + $id,
                method: "GET"
            }).then(function (response) {
                $scope.currentCustomer = response.data.data;
                if ($scope.currentCustomer.birthday) {
                    $scope.currentCustomer.birthday = moment($scope.currentCustomer.birthday).format('DD/MM/YYYY');
                }
            });
            $('#modal-add-customer').modal('show');
        };

        // Save change
        $scope.saveCustomer = function () {
            var formValidation = $('.addCustomerForm');
            formValidation.validate();
            if (!formValidation.valid()) return;
            if ($('#customer-birthday-date').val()) {
                $scope.currentCustomer.birthday = moment($('#customer-birthday-date').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
            } else $scope.currentCustomer.birthday = null;
            $('#modal-add-customer').modal('hide');
            $('.customer-article .tables_processing').show();
            $http.post(appConfig.api_path + 'customer/update',$scope.currentCustomer
            ).then(function (response) {
                    if (response.data.msg == true) {
                        $scope.filterCustomer();
                    }
                });
        };

        $scope.deleteCustomer = function (id) {
            $('.customer-article .tables_processing').show();
            $http({
                url: appConfig.api_path + 'customer/delete',
                method: "GET",
                params: {
                    id: id,
                    orderBy: $scope.customersList.orderBy,
                    orderDir: $scope.customersList.orderDir,
                    start: ($scope.customersList.currentPage - 1) * $scope.customersList.itemsPerPage,
                    length: $scope.customersList.itemsPerPage,
                    search: $scope.customersList.customerSearchValue
                }
            }).then(function (response) {
                if (response.data.msg == true) {
                    $('.customer-article .tables_processing').text('Xoá thành công!');
                    $scope.filterCustomer();
                } else $('.customer-article .tables_processing').text("Không thể xóa khách hàng này!");
                setTimeout(function () {
                    $('.customer-article .tables_processing').hide().text('Processing...');
                }, 2000);
            });
        };

        $scope.updateStatus = function ($id, $status) {
            $('.customer-article .tables_processing').show();
            // The code validate and update to API server will create at here!
            $http.post(appConfig.api_path + 'customer/changeStatus', {id: $id, status: $status, _token: CSRF_TOKEN}).
                success(function (data, status, headers, config) {
                    $('.customer-article .tables_processing').hide();
                    // this callback will be called asynchronously
                    // when the response is available
                });
        };
        $scope.columnsShown = JSON.parse(localStorage.getItem("customerColumnsShown"));

        $scope.initColumnsShown = function () {
            if (!$scope.columnsShown) {
                $scope.columnsShown = {};
                $scope.columnsShown.avatar = true;
                $scope.columnsShown.username = true;
                $scope.columnsShown.fullname = true;
                $scope.columnsShown.lastname = true;
                $scope.columnsShown.phone = true;
                $scope.columnsShown.email = true;
                $scope.columnsShown.firstname = false;
                $scope.columnsShown.address = false;
                $scope.columnsShown.birthday = false;
                $scope.columnsShown.identity_card = false;
            }
        };

        $scope.initColumnsShown();

        $scope.updateColumnsShown = function () {
            localStorage.setItem("customerColumnsShown",JSON.stringify($scope.columnsShown));
        };
    });
});

