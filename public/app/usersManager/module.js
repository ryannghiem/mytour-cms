define(
    [
        'angular',
        'angular-couch-potato',
        'angular-ui-router', 'angular-cookies',
        'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'
    ], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.usersManager', ['ui.router','brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {

        $stateProvider
        // Parent
        .state('app.usersManager', {
            abstract: true,
            data: {title: 'Quản lý khách hàng'}
        })
        // Childs
        .state('app.usersManager.customer', {
            url: '/customers',
            data: {title: 'Danh sách khách hàng'},
            views: {
                "content@app": {
                    templateUrl: 'app/usersManager/customer/views/index.tpl.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            /*Controllers*/
                            'usersManager/customer/controller/customersIndexCtrl',
                            'usersManager/customer/controller/CustomersCtrl',

                            'usersManager/customer/directives/customersList'
                        ])
                    }
                }
            }
        })
    });
    couchPotato.configureApp(module);
    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});