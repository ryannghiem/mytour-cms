/**
 * Created by Phuong on 11/2/2015.
 */
define(['layout/module'], function (module) {

    'use strict';
    module.registerDirective('clickAndDisable', function () {
        return {
            scope: {
                clickAndDisable: '&'
            },
            restrict: 'A',
            link: function(scope, iElement, iAttrs) {
                iElement.bind('click', function() {
                    if (iElement.hasClass('disabled')) return;
                    // iElement.attr("disabled", true);
                    $('.btn-change-quantity').addClass('disabled');
                    scope.clickAndDisable();
                    // scope.clickAndDisable().finally(function() {
                    //     iElement.removeClass('disabled');
                    // })
                });
            }
        }
    });
});