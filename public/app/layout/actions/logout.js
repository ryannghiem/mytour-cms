/**
 * Created by Phuong on 6/17/2015.
 */
define(['layout/module'], function (module) {

    "use strict";

    module.registerDirective('logout', function($rootScope, $location, $http, $cookies){
        return {
            restrict: 'A',
            link: function(scope, element){
                var doLogout = function(){
                    $.SmartMessageBox({
                        title: "<i class='fa fa-sign-out txt-color-orangeDark'></i> Đăng xuất tài khoản <span class='txt-color-orangeDark'><strong>"+ $("#show-shortcut").text()+"</strong></span> ?",
                        content: "Bạn chắc chắn muốn đăng xuất không?",
                        buttons: '[No][Yes]'
                    }, function (ButtonPressed) {
                        if (ButtonPressed === "Yes") {
                            $http.get(appConfig.api_path + 'user/logout').then(function (response) {
                                $cookies = null;
                                $rootScope.userLogin = null;
                                $location.path("/login");
                            });
                        }
                    });
                };
                element.on('click', doLogout);
            }
        }
    })
});
