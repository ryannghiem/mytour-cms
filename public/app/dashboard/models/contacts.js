/**
 * Created by Phuong on 6/29/2015.
 */
define(['contactManager/module'], function (module) {

    'use strict';

    return module.registerFactory('ContactsList', function ($http, $q) {
        var dfd = $q.defer();

        var ContactsList = {
            initialized: dfd.promise,
            data: {}
        };

        $http({
            url: appConfig.api_path + 'contact/list',
            method: "GET",
            params: {
                orderBy: 'contacts.created_at',
                orderDir: 'desc',
                start: 0,
                length: 10,
                search: ''
            }
        }).then(function (response) {
            ContactsList.data.data = response.data.data;
            ContactsList.data.currentPage = 1;
            ContactsList.data.itemsPerPage = 10;
            ContactsList.data.orderBy = 'contacts.created_at';
            ContactsList.data.contactSearchValue = '';
            ContactsList.data.orderDir = 'desc';
            ContactsList.data.totalContacts = response.data.recordsFiltered;
            dfd.resolve(ContactsList)
        });

        return ContactsList;
    });
});