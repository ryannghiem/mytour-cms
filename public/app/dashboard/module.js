define(['angular',
    'angular-couch-potato',
    'angular-ui-router',
    'brantwills.paging', 'stringjs', 'select2', 'crop-image', 'jquery-validation','auto-numeric','jasny-bootstrap'], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.dashboard', ['ui.router', 'brantwills.paging']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider
            .state('app.dashboard', {
                url: '/dashboard',
                data: {
                    title: 'Thống kê'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/dashboard/views/index.tpl.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                /*Controller*/
                                'dashboard/controller/IndexCtrl',

                                /* Directives*/
                                'dashboard/directives/contactsList'
                            ])
                        }
                    }
                }
            }
        )
    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});