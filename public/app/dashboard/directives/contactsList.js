/**
 * Created by Phuong on 6/29/2015.
 */
define(['contactManager/module'], function(module){

    "use strict";

    return module.registerDirective('contactsList', function(ContactsList){
        return {
            restrict: 'A',
            templateUrl: 'app/contactManager/views/contact-table.html',
            link: function(scope){
                ContactsList.initialized.then(function(){
                    scope.contactsList = ContactsList.data;
                });
            }
        }
    })
});