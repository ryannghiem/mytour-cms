<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    //
    protected $table = 'hotels';

    protected $fillable = ['id',
        'name',
        'alias',
        'description',
        'address',
        'image_small',
        'image_origin',
        'stars',
        'is_hot',
        'is_best',
        'city_id',
        'hotel_url',
        'last_booking',
        'status',
        'created_by',
        'updated_by'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname', 'email']);
    }
}
