<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    //
    protected $table = 'ads';

    protected $fillable = ['id',
        'name',
        'description',
        'room_id',
//        'image',
        'show_in_home',
        'ordering',
        'status',
        'ads_url',
        'created_by',
        'updated_by'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname', 'email']);
    }
}
