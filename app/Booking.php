<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $table = 'bookings';

    protected $fillable = ['id',
        'room_id',
        'price',
        'number_night',
        'start_date',
        'end_date',
        'booking_at',
        'status',
        'created_by',
        'updated_by'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname', 'email']);
    }
    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id')->select(['id', 'name', 'address']);
    }
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id')->select(['id', 'name', 'price']);
    }
}
