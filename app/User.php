<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract{

    use Authenticatable, CanResetPassword;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    // protected $with = ['created_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'id',
        'fullname',
        'email',
        'password',
        'avatar',
        'phone',
        'address',
        'gender',
        'birthday',
        'identity_card',
        'passport',
        'description',
        'node',
        'customer_code',
        'user_type',
        'status',
        'store_ids',
        'store_id',
        'start_work',
        'created_by',
        'updated_by'
    ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password'];

	public static function _generateSalt($length = 22) {
        $salt = str_replace(
            ['+', '='], '.', base64_encode(sha1(uniqid('mostcoupon', true), true))
        );
        return substr($salt, 0, $length);
    }

    public static function _encrypt($password, $salt) {
        return sha1(sha1(sha1($password)) . $salt);
    }

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname','email']);
    }

    public function store()
    {
        return $this->belongsTo('App\Store', 'store_id')->select(['id', 'name','address','logo']);
    }
}
