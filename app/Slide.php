<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //
    protected $table = 'slides';

    protected $fillable = ['id',
        'name',
        'hotel_id',
        'description',
        'room_id',
//        'image',
        'ordering',
        'status',
        'created_by',
        'updated_by'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname', 'email']);
    }
    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id')->select(['id', 'name', 'address']);
    }
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_id')->select(['id', 'name', 'price']);
    }
}
