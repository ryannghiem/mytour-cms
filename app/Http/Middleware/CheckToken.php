<?php

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->api_token != config('config.api_token')) {
            return response(['status' => false, 'msg' => 'Login failed!']);
        }
        return $next($request);
    }
}
