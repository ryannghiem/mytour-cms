<?php
/**
 * Created by PhpStorm.
 * User: Phuong
 * Date: 10/9/2015
 * Time: 4:50 PM
 */

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;
use Session;

class StaffController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'fullname' => 'required|string|max:255',
                'firstname' => 'nullable|string|max:255',
                'lastname' => 'nullable|string|max:255',
                'email' => 'string|email',
                'password' => 'string|max:100',
                'salt' => 'nullable|string|max:255'
            ];
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('users as staffs')
            ->where('staffs.user_type', '=', '1')
            ->where(function ($query) use ($params) {
                return $query->whereRaw("staffs.fullname like '%" . $params['search'] . "%'")
                    ->orWhere('staffs.email', 'ILIKE', '%' . $params['search'] . '%');
            });
        $result['data'] = DB::connection()->table('users as staffs')
            ->whereIn('staffs.user_type',[1,2])
            ->leftJoin('users as uc', 'uc.id', '=', 'staffs.created_by')
            ->leftJoin('users as uu', 'uu.id', '=', 'staffs.updated_by')
            ->select('staffs.id', 'staffs.fullname', 'staffs.email', 'staffs.created_at', 'staffs.updated_at', 'staffs.status',
                'uc.fullname as author_created', 'uu.fullname as author_updated')
            ->where(function ($query) use ($params) {
                return $query->whereRaw("staffs.fullname like '%" . $params['search'] . "%'")
                    ->orWhere('staffs.email', 'LIKE', '%' . $params['search'] . '%');
            });
//        $per = $this->checkPermission('users','read');
//        if ($per != 'all') {
//            $result['recordsFiltered'] = $result['recordsFiltered']->whereIn('users.id', $per);
//            $result['data'] = $result['data']->whereIn('users.id', $per);
//        }
//
//        $perDomain = $this->checkPermission('domains','access');
//        if ($perDomain != 'all') {
//            $result['recordsFiltered'] = $result['recordsFiltered']->whereIn('users.countrycode', $perDomain);
//            $result['data'] = $result['data']->whereIn('users.countrycode', $perDomain);
//        }

        if (Session::get('user')['user_type'] != 3) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('staffs.store_id','=', Session::get('user')['store_id']);
            $result['data'] = $result['data']->where('staffs.store_id','=', Session::get('user')['store_id']);
        }

        if (isset($params['status']) && $params['status'] != '') {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('staffs.status', '=', $params['status']);
            $result['data'] = $result['data']->where('staffs.status', '=', $params['status']);
        }

        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])) {
            $oderSearch = str_replace("'", "''", $params['search']);
            $result['data'] = $result['data']->orderByRaw("staffs.fullname = '" . $oderSearch . "' DESC, lower(staffs.fullname) = '" . strtolower($oderSearch) . "' DESC, staffs.fullname LIKE '" . $oderSearch . "%' DESC, lower(staffs.fullname) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("staffs.fullname ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    public function allStaffsPublic(Request $request){
        $u = User::whereIn('user_type',[1,2]);
        $params = $request->all();
        if (!empty($params['seller_id']))
            $u = $u->where(function ($query) use ($params) {
                return $query->where('status','=',1)
                    ->orWhere('id','=',$params['seller_id']);
            });
        else $u = $u->where('status','=',1);

        if (Session::get('user')['user_type'] != 3) {
            $u = $u->where('store_id','=', Session::get('user')['store_id']);
            /*if (Session::get('user')['user_type'] == 1)
                $u = $u->where('id','=', Session::get('user')['id']);*/
        }

        if (!empty($params['store_id']))
            $u = $u->where('store_id','=', $params['store_id']);

        return response()->json($u->orderBy('user_type', 'desc')->orderBy('lastname', 'asc')->get(['id','fullname']));
    }

    public function allStaffs(){
        $u = User::whereIn('user_type',[1,2]);

        if (Session::get('user')['user_type'] != 3) {
            $u = $u->where('store_id','=', Session::get('user')['store_id']);
        }
        return response()->json($u->orderBy('user_type', 'desc')->orderBy('lastname', 'asc')->get(['id','fullname', 'status']));
    }

    public function edit($id)
    {
        $u = User::with('author')->where('id', '=', $id)->first();
        return response()->json(['data' => $u]);
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            $params = $request->all();
            $ip = $this->inputs;
            $this->validate($request, $ip);
//            if (!empty($params['store_ids'])) {
//                $params['store_ids'] = json_encode(explode(',', $params['store_ids']));
//            }else $params['store_ids'] = null;
            if (empty($params['id'])) {
                $u = new User;
            } else {
                $u = User::find($params['id']);
                $arrExclude = ['password', 'salt', 'token', 'author', 'wp_id'];
                foreach ($arrExclude as $key) {
                    if (isset($params[$key])) unset($params[$key]);
                }
            }

            $u->fill($params);
            if (empty($params['id'])) {
                $u->created_by = Session::get('user')['id'];
            }
            $u->updated_by = Session::get('user')['id'];
            if (!empty($u->password) && empty($u->salt)) {
                $u->salt = User::_generateSalt();
                $u->password = User::_encrypt($u->password, $u->salt);
            }
            if (!empty($u->fullname)) {
                $parts = explode(" ", $u->fullname);
                $u->lastname = array_pop($parts);
                $u->firstname = implode(" ", $parts);
            }
            $result['msg'] = $u->save();

            if (!empty($params['avatar']) && strpos($params['avatar'], 'base64') !== false) {
                $avatar = config('config.user_avatar_dir'). $u->id . '.png';
                $u->avatar = $this->base64_to_image($params['avatar'], $avatar);
                $u->save();
            }

            return response()->json($result);
        } else {
            return response()->json(['msg' => false]);
        }
    }

    public function delete(Request $request)
    {
        $params = $request->all();
        $user = User::find($params['id']);
        $result['msg'] = $user->delete();
        return response()->json($result);
    }

    public function changeStatus(Request $requests)
    {
        if ($requests->isMethod('post')) {
            $params = $requests->all();
            $s = User::find($params['id']);
            if (!empty($s)) {
                $s->status = $params['status'];
                $s->updated_by = Session::get('user')['id'];
                $s->save();
            }
        }
    }

    public function checkPass(Request $requests){
        if ($requests->isMethod('post')) {
            $params = $requests->all();
            $u = User::find($params['staff_id']);
            if (!empty($u)) {
                $salt = $u->salt;
                $password = User::_encrypt($params['password'], $salt);
                $check = User::where('id','=',$params['staff_id'])->where('password','=',$password)->first();
                if ($check) {
                    $result['msg'] = true;
                    return response()->json($result);
                }
            }
        }
        $result['msg'] = false;
        return response()->json($result);
    }
}