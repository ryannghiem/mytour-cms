<?php

namespace App\Http\Controllers;

use App\City;
use DB;
use Illuminate\Http\Request;
use Session;

class CityController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'name' => 'required|string',
                'country_id' => 'integer',
                'is_hot' => 'integer',
                'price' => 'integer'
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('cities')
            ->whereRaw("lower(cities.name) like '%". strtolower($params['search']) . "%'");
        $result['data'] = DB::connection()->table('cities')
            ->select('cities.id', 'cities.ordering', 'cities.name', 'cities.description',  'cities.status', 'cities.months_used',  'cities.price', 'cities.created_at')
            ->whereRaw("lower(cities.name) like '%". strtolower($params['search']) . "%'");
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('cities.status', '=', $params['status']);
            $result['data'] = $result['data']->where('cities.status', '=', $params['status']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])){
            $oderSearch = str_replace("'","''",$params['search']);
            $result['data'] = $result['data']->orderByRaw("cities.name = '" . $oderSearch . "' DESC, lower(cities.name) = '" . strtolower($oderSearch) . "' DESC, cities.name LIKE '" . $oderSearch . "%' DESC, lower(cities.name) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("cities.name ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $store = City::where('id', '=', $id)->first();
        return response()->json(['data' => $store]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        if ($request->isMethod('put')) {
            $params = $request->all();
            $ip = $this->inputs;
            $this->validate($request, $ip);
            if (empty($params['id'])) {
                $city = new City;
            } else {
                $city = City::find($params['id']);
            }

            $city->fill($params);

            if (empty($params['id'])) {
                $city->created_by = Session::get('user')['id'];
            }
            $city->updated_by = Session::get('user')['id'];
            $result['msg'] = $city->save();

            return response()->json($result);
        } else {
            return response()->json(['msg' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $cat = City::find($id);
        return response(['msg' => $cat->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        if ($requests->isMethod('post')) {
            $params = $requests->all();
            $s = City::find($params['id']);
            if (!empty($s)) {
                $s->status = $params['status'];
                $s->updated_by = Session::get('user')['id'];
                $s->save();
            }
        }
    }

    public function listCitys()
    {
        $cities = City::where('status','=',1)->get(['id','name']);
        return $cities;
    }

    public function getCities(Request $request)
    {
        $params = $request->all();
        $s = City::whereRaw("lower(cities.name) like '%" . strtolower($params['search']) . "%'")
            ->where('status','=',1);
        $s = $s->orderBy('ordering')
            ->get(['id', 'name', 'hotel_count']);
        return response()->json(['items' => $s]);
    }

    public function getCitiesSelected(Request $request) {
        $data = $request->all();

        $domains = City::whereIn('id',explode(',',$data['id']))
            ->get(['id', 'name', 'hotel_count']);
        return response()->json(['items' => $domains]);
    }

    public function getCitySelected(Request $request) {
        $data = $request->all();

        $domain = City::where('id','=',$data['id'])
            ->first(['id', 'name', 'hotel_count']);
        return response()->json(['items' => $domain]);
    }
}
