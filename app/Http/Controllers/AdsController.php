<?php

namespace App\Http\Controllers;

use App\Ads;
use DB;
use Illuminate\Http\Request;
use Session;

class AdsController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'name' => 'required|string|max:255',
                'description' => 'nullable|string',
                'image_origin' => 'required|string',
                'status' => 'integer|max:2',
                'ordering' => 'integer',
                'ads_url' => 'string|url'
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('ads')
            ->leftJoin('users as uc', 'uc.id', '=', 'ads.created_by')
            ->whereRaw("lower(ads.name) like  '%" . strtolower($params['search']) . "%'");
        $result['data'] = DB::connection()->table('ads')
            ->leftJoin('users as uc', 'uc.id', '=', 'ads.created_by')
            ->select('ads.id', 'ads.name', 'ads.description', 'ads.status', 'ads.created_at', 'ads.ordering',
                'uc.fullname as author_created')
            ->whereRaw("lower(ads.name) like  '%" . strtolower($params['search']) . "%'");;
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('ads.status', '=', $params['status']);
            $result['data'] = $result['data']->where('ads.status', '=', $params['status']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])){
            $oderSearch = str_replace("'","''",$params['search']);
            $result['data'] = $result['data']->orderByRaw("ads.name = '" . $oderSearch . "' DESC, lower(ads.name) = '" . strtolower($oderSearch) . "' DESC, ads.name LIKE '" . $oderSearch . "%' DESC, lower(ads.name) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("ads.name ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $ads = Ads::with('author')->where('id', '=', $id)->first();
        return response()->json(['data' => $ads]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        $params = $request->all();
        $ip = $this->inputs;
        $this->validate($request, $ip);
        if (empty($params['id'])) {
            $ads = new Ads;
        } else {
            $ads = Ads::find($params['id']);
        }

        $ads->fill($params);

        if (empty($params['id'])) {
            $ads->created_by = Session::get('user')['id'];
        }
        $ads->updated_by = Session::get('user')['id'];
        $result['msg'] = $ads->save();
        $uploadImage = false;
        if (!empty($params['image_origin']) && strpos($params['image_origin'], 'base64') !== false) {
            $image_origin = config('config.ads_image_origin_dir'). $ads->id . '.png';
            $ads->image_origin = $this->base64_to_image($params['image_origin'], $image_origin);
            $this->optimizeImage($ads->id . '.png',$ads->image_origin,630,400);
            $uploadImage = true;
        }
        if (!empty($params['image_small']) && strpos($params['image_small'], 'base64') !== false) {
            $image_small = config('config.ads_image_small_dir'). $ads->id . '.png';
            $ads->image_small = $this->base64_to_image($params['image_small'], $image_small);
            $this->optimizeImage($ads->id . '.png',$ads->image_small,315,200);
            $uploadImage = true;
        }
        if ($uploadImage) $ads->save();
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $ads = Ads::find($id);
        return response(['msg' => $ads->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        $params = $requests->all();
        $s = Ads::find($params['id']);
        if (!empty($s)) {
            $s->status = $params['status'];
            $s->updated_by = Session::get('user')['id'];
            $s->save();
        }
    }

    public function listAds()
    {
        $ads = Ads::where('status','=',1)->get(['id','name']);
        return $ads;
    }

    public function getAdss(Request $request)
    {
        $params = $request->all();
        $s = Ads::whereRaw("lower(ads.name) like '%" . strtolower($params['search']) . "%'")
            ->where('status','=',1);
        $s = $s->orderBy('ordering')
            ->get(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $s]);
    }

}
