<?php namespace App\Http\Controllers;

use App\User;
use Auth;
use DB;
use Illuminate\Http\Response;
use Session;

use App\UserRole;
use App\Permission;

use Illuminate\Http\Request;

class UserController extends Controller {

	protected $inputs = [];

	public function __construct()
    {
    	$this->inputs =
		[
	    	'fullname' 		=> 'nullable|string|max:100',
			'username' 		=> 'required|max:200|unique:users|alpha_num',
			'email' 		=> 'required|email|unique:users',
			'password' 		=> 'required|string|max:100',
			'salt' 			=> 'string|max:255',
			'facebook_id' 	=> 'nullable|string|50|unique:users',
			'status' 		=> 'required|string|max:20',
			'token' 		=> 'string|max:100',
			'author' 		=> 'nullable|string|max:36',
			'language' 		=> 'nullable|string|max:2',
			'avatar' 		=> 'nullable|string|max:255',
			'phone' 		=> 'nullable|string|max:255',
			'skype' 		=> 'nullable|string|max:255',
			'department' 	=> 'nullable|string|max:255',
			'postal'		=> 'nullable|string|max:255',
			'role'			=> 'required|string|max:20',
			'time_zone'		=> 'nullable|string|max:50',
			'currency'		=> 'nullable|string|max:3',
			'email_temp'	=> 'nullable|string|max:255',
			'last_login'	=> 'nullable|date',
			'deleted'		=> 'nullable|date',
			'birthday'		=> 'nullable|date',
			'gender'		=> 'integer|max:1',
			'city'			=> 'nullable|string|max:255',
			'state'			=> 'nullable|string|max:255',
			'zipcode'		=> 'nullable|string|max:255',
			'country'		=> 'nullable|string|max:255',
			'website'		=> 'url',
			'bio'			=> 'nullable|string|max:255',
			'quote'			=> 'nullable|string|max:255',
			'wp_id'			=> 'integer|max:32',
			'first_login'	=> 'integer|max:1'
	    ];
    }

    public function session(Request $request){
        if(Session::has('user') && Auth::check()){
            return Session::get('user');
      	}else{
            return [
              'message' => 'Not logged in',
              'redirect' => '/login',
              'status' => false
            ];
      	}
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $usersApplied = DB::table('user_roles')->get(['user_id']);
        $usersAppliedIds = [];
        foreach($usersApplied as $u){
            array_push($usersAppliedIds, $u->user_id);
        }
        if($request->keyword){
            $keyword = $request->keyword;
            return response(DB::table('users')->whereNotIn('id', $usersAppliedIds)
                                        ->where(function($query) use ($keyword){
                                            return $query->where('fullname', 'ILIKE', '%' . $keyword . '%')
                                                ->orWhere('username', 'ILIKE', '%' . $keyword . '%')
                                                ->orWhere('email', 'ILIKE', '%' . $keyword . '%');
                                        })->limit(20)->get($request->columns));
        }else{
            return response("false");
        }
	}


  /*
  Get permissions
  */
  public function userPermission(Request $request){
      if(Session::has('permissions')){
          return Session::get('permissions');
      }else{
          return 'Permissions not set';
      }
  }

  public function logout(){
      $user_id = Session::get('user')['id'];
      Session::flush();
      $this->clearSession([$user_id]);
  }

	protected function getPermissions($user_id){
		// init array allow actions
		$roleId = UserRole::where('user_id', '=', $user_id)->pluck('role_id');
        if($roleId){
            $permissions = Permission::where('role_id', '=', $roleId)->pluck('permissions');
            return $permissions;
        }
        return [];
	}

    public function whereIn(Request $request){
        $data = $request->json('data');
        $temp = [];
        foreach ($data as $d) {
            $user = DB::table('users')->where('id', '=', $d['user_id'])->first(['username']);
            $role = DB::table('roles')->where('id', '=', $d['role_id'])->first(['role_name', 'description']);
            $t['id'] = $d['id'];
            $t['user_id'] = $d['user_id'];
            $t['role_id'] = $d['role_id'];
            $t['role_name'] = $role->role_name;
            $t['description'] = $role->description;
            array_push($temp, $t);
        }
        return $temp;
    }

    public function getUsersInRole($id){
        $usersId = DB::table('user_roles')->where('role_id', '=', $id)->get(['user_id']);
        $temp = [];
        foreach($usersId as $u){
            array_push($temp, $u->user_id);
        }
        $users = DB::table('users')->whereIn('id', $temp)->get(['id','fullname','username', 'email']);
        return $users;

    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function update(Request $request)
	{
		if($request->isMethod('post')){
            $params = $request->all();
			$ip = $this->inputs;
			$arrExclude = ['password', 'salt', 'token', 'author', 'wp_id'];
		    foreach ($arrExclude as $key) {
		    	unset($params[$key]);
		    }
			$this->validate($request, $ip);
            if (empty($params['id'])) {
                $user = new User;
            } else {
                $user = User::find($params['id']);
            }
            $user->fill($params);

            return response(['status' => $user->save()]);

		}else{
			return response(['error' => 'Require POST method']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
  		$user = User::find($id);
  		return response($user->delete());
	}

    public function getById(Request $request){
        $id = $request->json('id');
        if($id){
            return DB::table('users')->where('id', '=', $id)->get();
        }else{
            return ['Missing User Id'];
        }
    }
    public function listSelect2(Request $request) {
        $params = $request->all();
        $users = User::where(function($query) use ($params){
            return $query->whereRaw("lower(email) like '%" . strtolower($params['search']) . "%'")
                ->orWhereRaw("lower(fullname) like '%" . strtolower($params['search']) . "%'");
        });
        $users = $users->where('status', '=', 1)->get(['id','email','fullname']);
        return response()->json(['items' => $users]);
    }

    public function select2Selected(Request $request) {
        $params = $request->all();
        $users = User::where('id', '=',$params['id'])->first(['id','email','fullname']);
        return response()->json(['items' => $users]);
    }

    public function getPer(Request $request) {
        $permission = json_decode(Session::get('permissions'));
        $response = new Response(['permission' => $permission]);
        return $response;
    }

    public function getAllEmail(){
        $users = User::whereNotNull('email')->get(['id','username as name','email']);
        return response()->json(['data' => $users]);
    }

    public function listByEmails(Request $request){
        $data = $request->all();
        $users = User::whereIn('email',(array)explode(',',$data['emails']))
            ->get(['id','username as name','email']);
        return response()->json(['items' => $users]);
    }

    public function changePass(Request $request){
        $oldPassword = $request->json('oldPassword');
        $newPassword = $request->json('newPassword');
        $user = User::find(Session::get('user')['id']);
        if($user) {
            $oldPassword = User::_encrypt($oldPassword, $user->salt);
            if ($oldPassword == $user->password) {
                $user->password =  User::_encrypt($newPassword, $user->salt);
                return response()->json(['msg' => $user->save()]);
            }else return response()->json(['msg' => 'Mật khẩu cũ không chính xác!']);
        }
    }

    public function resetPass(Request $request){
        $data = $request->all();
        $user = User::find($data['id']);
        if($user) {
            $user->fill($data);
            $user->salt = User::_generateSalt();
            $user->password = User::_encrypt($data['password'], $user->salt);
            return response()->json(['msg' => $user->save()]);
        }
    }

    public function checkEmailExist(Request $request){
        $data = $request->all();
        $user = User::where('email','=',strtolower($data['email']));
        if (!empty($data['id'])) $user = $user->where('id','<>',$data['id']);
        $user = $user->first();
        if ($user) {
            return response()->json(['exist' => true]);
        }else return response()->json(['exist' => false]);
    }
}
