<?php namespace App\Http\Controllers;

use File;
use ImageOptim\API;
use Session;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Storage;

abstract class Controller extends BaseController {

	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function uploadToS3($alias, $fileUpload) {

        $pos  = strpos($fileUpload, ';');
        $mime_type = explode(':', substr($fileUpload, 0, $pos))[1];
        //input a row into the database to track the image (if needed)
        $image = [
            'id' => $alias,
            'ext' => str_replace('image/','',$mime_type)
        ];

        //Use some method to generate your filename here. Here we are just using the ID of the image
        $filename = $image['id'];
        list($type, $fileUpload) = explode(';', $fileUpload);
        list(, $fileUpload)      = explode(',', $fileUpload);
        $data = base64_decode($fileUpload);
        //Push file to S3
        Storage::disk('s3')->put($filename, $data, 'public');
        $bucket = config('filesystems.disks.s3.bucket');
        $s3 = Storage::disk('s3');
        $avatarUrl = $s3->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $filename);

        //Use this line to move the file to local storage & make any thumbnails you might want
        //$request->file('file')->move('/full/path/to/uploads', $filename);

        //Thumbnail as needed here. Then use method shown above to push thumbnails to S3

        //If making thumbnails uncomment these to remove the local copy.
        //if(Storage::disk('s3')->exists('uploads/' . $filename))
        //Storage::disk()->delete('uploads/' . $filename);

        //If we are still here we are good to go.
        return $avatarUrl;
    }

    function base64_to_image($base64_string, $output_file) {
        $data = explode(',', $base64_string);
        Storage::disk('local')->put($output_file, base64_decode($data[1]));
        return 'storage' . $output_file;
    }

    function optimizeImage($filename, $sourceImageUrl,$width,$height) {
        //https://imageoptim.com/api/post?username=
        $api = new API("vncmjfjfgr");
        $storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
        $sourceImageUrl = str_replace('storage/','',$sourceImageUrl);
        if(Storage::disk('local')->exists($sourceImageUrl)) {
            $imageData = $api->imageFromPath($storagePath . $sourceImageUrl)
                ->resize($width, $height, 'fit')
                ->getBytes();
            Storage::disk('local')->put($sourceImageUrl, $imageData);
            File::copy($storagePath . $sourceImageUrl, config('config.frontend_image_dir') . $sourceImageUrl);
        }
    }

    /**
     * @param $table_name string
     * @param $action string
     * @param $id string
     * @return mixed
     */
    public function checkPermission($table_name,$action = null,$id = null){
        $per = json_decode(Session::get('permissions'), true);
        if (!empty($per[$table_name]['0'][$action])) return 'all';
        if (!empty($id)) {
            if ($per[$table_name][$id][$action]) {
                return true;
            }else return false;
        }else {
            return !empty($per[$table_name]['list']) ? $per[$table_name]['list'] : [];
        }
    }
    /*
      Clear user ids from table sessions
      Input : $userIds
      Type : array user ids
    */
//    public function clearSession($userIds){
//        DB::table('sessions')->whereIn('user_id', $userIds)->delete();
//    }

    function _encodeQS($data)
    {
        $req = "";

        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }
        // Cut the last '&'
        $req = rtrim($req, '&');
        return $req;
    }

    function _submitHTTPGet($path, $data)
    {
        $req = $this->_encodeQS($data);
        if (!empty($req)) $req = '?' . $req;
        $response = json_decode(file_get_contents($path . $req), true);
        return $response;
    }

    function _submitHTTPPost($path, $data)
    {
        $req = $this->_encodeQS($data);
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $path);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //execute post
        $result = curl_exec($ch);

        //Do something
        //var_dump($result)

        //close connection
        curl_close($ch);
        return json_decode($result,true);
    }

    protected function _generateEmailTemplate($domain, $siteName, $template, $dataMail)
    {
        return view('emails.'.$template)->with(array_merge(array('domain' => $domain, 'siteName' => $siteName ), $dataMail));
    }
}
