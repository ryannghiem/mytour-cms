<?php

namespace App\Http\Controllers;

use App\Hotel;
use DB;
use Illuminate\Http\Request;
use Session;

class HotelController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'name' => 'required|string|max:255',
                'alias' => 'required|string|max:255',
                'description' => 'nullable|string',
                'address' => 'required|string',
                'hotel_url' => 'nullable|string',
                'image_small' => 'nullable|string',
                'image_origin' => 'required|string',
                'status' => 'integer|max:2',
                'stars' => 'integer|max:5',
                'is_hot' => 'integer|max:1',
                'is_best' => 'integer|max:1',
                'city_id' => 'integer',
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('hotels')
            ->join('cities as c','c.id','=','hotels.city_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'hotels.created_by')
            ->whereRaw("lower(hotels.name) like  '%" . strtolower($params['search']) . "%'");
        $result['data'] = DB::connection()->table('hotels')
            ->join('cities as c','c.id','=','hotels.city_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'hotels.created_by')
            ->select('hotels.id', 'hotels.name', 'hotels.description', 'hotels.address',  'hotels.alias', 'hotels.stars',  'hotels.is_hot', 'hotels.is_best', 'hotels.status', 'hotels.created_at',
                'hotels.room_count', 'uc.fullname as author_created', 'c.name as city_name')
            ->whereRaw("lower(hotels.name) like  '%" . strtolower($params['search']) . "%'");;
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('hotels.status', '=', $params['status']);
            $result['data'] = $result['data']->where('hotels.status', '=', $params['status']);
        }
        if (!empty($params['city_id'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('hotels.city_id', '=', $params['city_id']);
            $result['data'] = $result['data']->where('hotels.city_id', '=', $params['city_id']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])){
            $oderSearch = str_replace("'","''",$params['search']);
            $result['data'] = $result['data']->orderByRaw("hotels.name = '" . $oderSearch . "' DESC, lower(hotels.name) = '" . strtolower($oderSearch) . "' DESC, hotels.name LIKE '" . $oderSearch . "%' DESC, lower(hotels.name) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("hotels.name ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $hotel = Hotel::with('author')->where('id', '=', $id)->first();
        return response()->json(['data' => $hotel]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        if ($request->isMethod('put')) {
            $params = $request->all();
            $ip = $this->inputs;
            $this->validate($request, $ip);
            if (empty($params['id'])) {
                $hotel = new Hotel;
            } else {
                $hotel = Hotel::find($params['id']);
            }

            $hotel->fill($params);
            if (!empty($params['image_origin']) && strpos($params['image_origin'], 'base64') !== false) {
                $image_origin = config('config.hotel_image_origin_dir'). $hotel->alias . '.png';
                $hotel->image_origin = $this->base64_to_image($params['image_origin'], $image_origin);
                $this->optimizeImage($hotel->alias . '.png',$hotel->image_origin,270,180);
            }
            if (!empty($params['image_small']) && strpos($params['image_small'], 'base64') !== false) {
                $image_small = config('config.hotel_image_small_dir'). $hotel->alias . '.png';
                $hotel->image_small = $this->base64_to_image($params['image_small'], $image_small);
                $this->optimizeImage($hotel->alias . '.png',$hotel->image_small,90,60);
            }
            if (empty($params['id'])) {
                $hotel->created_by = Session::get('user')['id'];
            }
            $hotel->updated_by = Session::get('user')['id'];
            $result['msg'] = $hotel->save();

            return response()->json($result);
        } else {
            return response()->json(['msg' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $hotel = Hotel::find($id);
        return response(['msg' => $hotel->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        if ($requests->isMethod('post')) {
            $params = $requests->all();
            $s = Hotel::find($params['id']);
            if (!empty($s)) {
                $s->status = $params['status'];
                $s->updated_by = Session::get('user')['id'];
                $s->save();
            }
        }
    }

    public function listHotels()
    {
        $hotels = Hotel::where('status','=',1)->get(['id','name']);
        return $hotels;
    }

    public function getHotels(Request $request)
    {
        $params = $request->all();
        $s = Hotel::where(function ($query) use ($params) {
                return $query->whereRaw("lower(name) like  '%" . strtolower($params['search']) . "%'")
                    ->orWhereRaw("lower(address) like  '%" . strtolower($params['search']) . "%'");
            })
            ->where('status','=',1);
        $s = $s->orderBy('name')
            ->get(['id', 'name', 'description', 'address']);
        return response()->json(['items' => $s]);
    }

    public function getHotelsSelected(Request $request) {
        $data = $request->all();

        $domains = Hotel::whereIn('id',explode(',',$data['id']))
            ->get(['id', 'name', 'description', 'address']);
        return response()->json(['items' => $domains]);
    }

    public function getHotelSelected(Request $request) {
        $data = $request->all();

        $domain = Hotel::where('id','=',$data['id'])
            ->first(['id', 'name', 'description', 'address']);
        return response()->json(['items' => $domain]);
    }
}
