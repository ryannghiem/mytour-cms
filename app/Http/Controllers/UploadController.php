<?php
/**
 * Created by PhpStorm.
 * User: Phuong
 * Date: 8/3/2015
 * Time: 9:42 AM
 */

namespace App\Http\Controllers;

use File;
use Request;
use Storage;
use Uuid;

class UploadController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->has('file'))
            return response()->json(['error' => 'No File Sent']);

        $fileUpload = $request->input('file');
        $pos = strpos($fileUpload, ';');
        $mime_type = explode(':', substr($fileUpload, 0, $pos))[1];
        //input a row into the database to track the image (if needed)
        $image = [
            'id' => !empty($request->input('id')) ? $request->input('id') : Uuid::generate(),
            'ext' => str_replace('image/', '', $mime_type)
        ];

        //Use some method to generate your filename here. Here we are just using the ID of the image
        $filename = $image['id'] . '-avatar';
        list($type, $fileUpload) = explode(';', $fileUpload);
        list(, $fileUpload) = explode(',', $fileUpload);
        $data = base64_decode($fileUpload);
        //Push file to S3
        Storage::disk('s3')->put($filename, $data, 'public');
        $bucket = config('filesystems.disks.s3.bucket');
        $s3 = Storage::disk('s3');
        $avatarUrl = $s3->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $filename);
        $result = $this->_submitHTTPPost(config('config.api_url') . 'users/update/', [
            'id' => $this->dataPage['user']['id'],
            'avatar' => $avatarUrl
        ]);
        //Use this line to move the file to local storage & make any thumbnails you might want
        //$request->file('file')->move('/full/path/to/uploads', $filename);

        //Thumbnail as needed here. Then use method shown above to push thumbnails to S3

        //If making thumbnails uncomment these to remove the local copy.
        //if(Storage::disk('s3')->exists('uploads/' . $filename))
        //Storage::disk()->delete('uploads/' . $filename);

        //If we are still here we are good to go.
        return response()->json(['status' => 'success', 'avatarUrl' => $avatarUrl]);
    }

    public function ckeditorImage(Request $request)
    {

        //Push file to S3
        $s3 = Storage::disk('s3');
        $bucket = config('filesystems.disks.s3.bucket');
        $file = Request::file('upload');
        $fileName = $file->getClientOriginalName();
        $status = $s3->put($fileName,  File::get($file), 'public');

        $filrUrl = $s3->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $fileName);

        //If we are still here we are good to go.
        return response($filrUrl);
    }
}