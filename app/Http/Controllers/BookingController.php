<?php

namespace App\Http\Controllers;

use App\Booking;
use DB;
use Illuminate\Http\Request;
use Session;

class BookingController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'user_id' => 'integer',
                'description' => 'nullable|string',
                'number_night' => 'integer',
                'start_date' => 'date',
                'end_date' => 'date',
                'booking_at' => 'date'
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('bookings')
            ->join('rooms as r','r.id','=','bookings.room_id')
            ->join('hotels as h','h.id','=','r.hotel_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'bookings.created_by');
//            ->where(function ($query) use ($params) {
//                return $query->whereRaw("lower(h.name) like  '%" . strtolower($params['search']) . "%'")
//                    ->orWhereRaw("lower(h.address) like  '%" . strtolower($params['search']) . "%'");
//            });
        $result['data'] = DB::connection()->table('bookings')
            ->join('rooms as r','r.id','=','bookings.room_id')
            ->join('hotels as h','h.id','=','r.hotel_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'bookings.created_by')
            ->select('bookings.id', 'bookings.number_night', 'bookings.start_date', 'bookings.end_date', 'bookings.price',
                'uc.fullname as author_created', 'h.name as hotel_name', 'h.address as hotel_address', 'r.name as room_name');
//            ->where(function ($query) use ($params) {
//                return $query->whereRaw("lower(h.name) like  '%" . strtolower($params['search']) . "%'")
//                    ->orWhereRaw("lower(h.address) like  '%" . strtolower($params['search']) . "%'");
//            });
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('bookings.status', '=', $params['status']);
            $result['data'] = $result['data']->where('bookings.status', '=', $params['status']);
        }
        if (!empty($params['hotelId'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->whereIn('r.hotel_id', $params['hotelId']);
            $result['data'] = $result['data']->whereIn('r.hotel_id', $params['hotelId']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $booking = Booking::with('author','hotel', 'room')->where('id', '=', $id)->first();
        return response()->json(['data' => $booking]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        $params = $request->all();
        $ip = $this->inputs;
        $this->validate($request, $ip);
        if (empty($params['id'])) {
            $booking = new Booking;
        } else {
            $booking = Booking::find($params['id']);
        }

        $booking->fill($params);

        if (empty($params['id'])) {
            $booking->created_by = Session::get('user')['id'];
        }
        $booking->updated_by = Session::get('user')['id'];
        $result['msg'] = $booking->save();
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        return response(['msg' => $booking->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        $params = $requests->all();
        $s = Booking::find($params['id']);
        if (!empty($s)) {
            $s->status = $params['status'];
            $s->updated_by = Session::get('user')['id'];
            $s->save();
        }
    }

    public function listBookings()
    {
        $bookings = Booking::where('status','=',1)->get(['id','name']);
        return $bookings;
    }

    public function getBookings(Request $request)
    {
        $params = $request->all();
        $s = Booking::whereRaw("lower(bookings.name) like '%" . strtolower($params['search']) . "%'")
            ->where('status','=',1);
        $s = $s->orderBy('ordering')
            ->get(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $s]);
    }

    public function getBookingsSelected(Request $request) {
        $data = $request->all();

        $domains = Booking::whereIn('id',explode(',',$data['id']))
            ->get(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $domains]);
    }

    public function getBookingSelected(Request $request) {
        $data = $request->all();

        $domain = Booking::where('id','=',$data['id'])
            ->first(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $domain]);
    }
}
