<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Session;

class SessionController extends Controller {

	protected $inputs = [];

	public function __construct()
    {
    	$this->inputs =
		[
			'username' => 'required|string|max:100',
			'time' => 'required|string|max:100',
			'user_id' => 'required|string|max:36',
			'role_id' => 'required|integer|max:32'
	    ];
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if($request->isMethod('post')){
			$ip = $this->inputs;
			$this->validate($request, $ip);

			$session = new Session;
			foreach ($ip as $key => $value) {
				$session->$key = $request->json($key);
			}

			return (string)$session->save();
		}else{
			return ['error' => 'Require POST method'];
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Session::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		if($request->isMethod('post')){
			$ip = $this->inputs;
			$this->validate($request, $ip);
			$session = Session::find($id);

			foreach ($ip as $key => $value) {
				$session->$key = $request->json($key);
			}
			if($session->save()){
				return ['status' => true];
			}else{
				return ['status' => false];
			}

		}else{
			return ['error' => 'Require POST method'];
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$session = Session::find($id);
		if(!is_null($session)){
			return (string)$session->delete();
		}
	}

}
