<?php

namespace App\Http\Controllers;

use App\Slide;
use DB;
use Illuminate\Http\Request;
use Session;

class SlideController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'name' => 'required|string|max:255',
                'description' => 'nullable|string',
                'image' => 'required|string',
                'status' => 'integer|max:2',
                'ordering' => 'integer',
                'room_id' => 'integer',
                'hotel_id' => 'integer'
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('slides')
            ->join('hotels as h','h.id','=','slides.hotel_id')
            ->join('rooms as r','r.id','=','slides.room_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'slides.created_by')
            ->whereRaw("lower(slides.name) like  '%" . strtolower($params['search']) . "%'");
        $result['data'] = DB::connection()->table('slides')
            ->join('hotels as h','h.id','=','slides.hotel_id')
            ->join('rooms as r','r.id','=','slides.room_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'slides.created_by')
            ->select('slides.id', 'slides.name', 'slides.description', 'slides.status', 'slides.created_at', 'slides.ordering',
                'uc.fullname as author_created', 'h.name as hotel_name', 'r.name as room_name')
            ->whereRaw("lower(slides.name) like  '%" . strtolower($params['search']) . "%'");;
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('slides.status', '=', $params['status']);
            $result['data'] = $result['data']->where('slides.status', '=', $params['status']);
        }
        if (!empty($params['hotelId'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->whereIn('slides.hotel_id', $params['hotelId']);
            $result['data'] = $result['data']->whereIn('slides.hotel_id', $params['hotelId']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])){
            $oderSearch = str_replace("'","''",$params['search']);
            $result['data'] = $result['data']->orderByRaw("slides.name = '" . $oderSearch . "' DESC, lower(slides.name) = '" . strtolower($oderSearch) . "' DESC, slides.name LIKE '" . $oderSearch . "%' DESC, lower(slides.name) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("slides.name ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $slide = Slide::with('author','hotel', 'room')->where('id', '=', $id)->first();
        return response()->json(['data' => $slide]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        $params = $request->all();
        $ip = $this->inputs;
        $this->validate($request, $ip);
        if (empty($params['id'])) {
            $slide = new Slide;
        } else {
            $slide = Slide::find($params['id']);
        }

        $slide->fill($params);

        if (empty($params['id'])) {
            $slide->created_by = Session::get('user')['id'];
        }
        $slide->updated_by = Session::get('user')['id'];
        $result['msg'] = $slide->save();
        if ($result['msg'] && !empty($params['image']) && strpos($params['image'], 'base64') !== false) {
            $image = config('config.slide_image_dir'). $slide->id . '.png';
            $slide->image = $this->base64_to_image($params['image'], $image);
            $result['note'] = $this->optimizeImage($slide->id . '.png',$slide->image,986,302);
            $slide->save();
        }
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        return response(['msg' => $slide->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        $params = $requests->all();
        $s = Slide::find($params['id']);
        if (!empty($s)) {
            $s->status = $params['status'];
            $s->updated_by = Session::get('user')['id'];
            $s->save();
        }
    }

    public function listSlides()
    {
        $slides = Slide::where('status','=',1)->get(['id','name']);
        return $slides;
    }

    public function getSlides(Request $request)
    {
        $params = $request->all();
        $s = Slide::whereRaw("lower(slides.name) like '%" . strtolower($params['search']) . "%'")
            ->where('status','=',1);
        $s = $s->orderBy('ordering')
            ->get(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $s]);
    }

    public function getSlidesSelected(Request $request) {
        $data = $request->all();

        $domains = Slide::whereIn('id',explode(',',$data['id']))
            ->get(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $domains]);
    }

    public function getSlideSelected(Request $request) {
        $data = $request->all();

        $domain = Slide::where('id','=',$data['id'])
            ->first(['id', 'name', 'description', 'months_used', 'price']);
        return response()->json(['items' => $domain]);
    }
}
