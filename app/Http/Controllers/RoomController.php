<?php

namespace App\Http\Controllers;

use App\Room;
use DB;
use Illuminate\Http\Request;
use Session;

class RoomController extends Controller
{
    protected $inputs = [];

    public function __construct()
    {
        $this->inputs =
            [
                'name' => 'required|string|max:255',
                'description' => 'nullable|string',
                'image' => 'required|string',
                'status' => 'integer|max:2',
                'ordering' => 'integer',
                'size' => 'integer',
                'price' => 'integer',
                'hotel_id' => 'integer',
                'has_discount' => 'integer|max:1',
                'discount_percent' => 'nullable|numeric|max:100|min:0',
                'discount_price' => 'nullable|integer',
                'discount_start' => 'nullable|date',
                'discount_end' => 'nullable|date',
            ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $result['recordsFiltered'] = DB::connection()->table('rooms')
            ->join('hotels as h','h.id','=','rooms.hotel_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'rooms.created_by')
            ->whereRaw("lower(rooms.name) like  '%" . strtolower($params['search']) . "%'");
        $result['data'] = DB::connection()->table('rooms')
            ->join('hotels as h','h.id','=','rooms.hotel_id')
            ->leftJoin('users as uc', 'uc.id', '=', 'rooms.created_by')
            ->select('rooms.id', 'rooms.name', 'rooms.description', 'rooms.size',  'rooms.price', 'rooms.status', 'rooms.created_at', 'rooms.ordering', 'rooms.updated_at',
                'uc.fullname as author_created', 'h.name as hotel_name')
            ->whereRaw("lower(rooms.name) like  '%" . strtolower($params['search']) . "%'");;
        if (!empty($params['status'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->where('rooms.status', '=', $params['status']);
            $result['data'] = $result['data']->where('rooms.status', '=', $params['status']);
        }
        if (!empty($params['hotelId'])) {
            $result['recordsFiltered'] = $result['recordsFiltered']->whereIn('rooms.hotel_id', $params['hotelId']);
            $result['data'] = $result['data']->whereIn('rooms.hotel_id', $params['hotelId']);
        }
        $result['recordsFiltered'] = $result['recordsFiltered']->count();
        if (!empty($params['search'])){
            $oderSearch = str_replace("'","''",$params['search']);
            $result['data'] = $result['data']->orderByRaw("rooms.name = '" . $oderSearch . "' DESC, lower(rooms.name) = '" . strtolower($oderSearch) . "' DESC, rooms.name LIKE '" . $oderSearch . "%' DESC, lower(rooms.name) LIKE '" . strtolower($oderSearch) . "%' DESC");
        }

        $result['data'] = $result['data']->orderBy($params['orderBy'], $params['orderDir'])->orderByRaw("rooms.name ASC")->skip($params['start'])->take($params['length'])
            ->get();
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $room = Room::with('author','hotel')->where('id', '=', $id)->first();
        return response()->json(['data' => $room]);
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        $params = $request->all();
        $ip = $this->inputs;
        $this->validate($request, $ip);
        if (empty($params['id'])) {
            $room = new Room;
        } else {
            $room = Room::find($params['id']);
        }

        $room->fill($params);

        if (empty($params['id'])) {
            $room->created_by = Session::get('user')['id'];
        }
        $room->updated_by = Session::get('user')['id'];
        $result['msg'] = $room->save();
        if ($result['msg'] && !empty($params['image']) && strpos($params['image'], 'base64') !== false) {
            $image = config('config.room_image_dir'). $room->id . '.png';
            $room->image = $this->base64_to_image($params['image'], $image);
            $this->optimizeImage($room->id . '.png',$room->image,240,180);
            $room->save();
        }
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $room = Room::find($id);
        return response(['msg' => $room->delete()]);
    }

    public function changeStatus(Request $requests)
    {
        $params = $requests->all();
        $s = Room::find($params['id']);
        if (!empty($s)) {
            $s->status = $params['status'];
            $s->updated_by = Session::get('user')['id'];
            $s->save();
        }
    }

    public function listRooms()
    {
        $rooms = Room::where('status','=',1)->get(['id','name']);
        return $rooms;
    }

    public function getRooms(Request $request)
    {
        $params = $request->all();
        $s = Room::whereRaw("lower(rooms.name) like '%" . strtolower($params['search']) . "%'")
            ->where('status','=',1);
        if (!empty($params['hotel_id']))
            $s = $s->where('hotel_id','=',$params['hotel_id']);
        $s = $s->orderBy('ordering')
            ->get(['id', 'name', 'description', 'price', 'discount_price', 'has_discount']);
        return response()->json(['items' => $s]);
    }

    public function getRoomsSelected(Request $request) {
        $data = $request->all();

        $domains = Room::whereIn('id',explode(',',$data['id']))
            ->get(['id', 'name', 'description', 'price']);
        return response()->json(['items' => $domains]);
    }

    public function getRoomSelected(Request $request) {
        $data = $request->all();

        $domain = Room::where('id','=',$data['id'])
            ->first(['id', 'name', 'description', 'price']);
        return response()->json(['items' => $domain]);
    }
}
