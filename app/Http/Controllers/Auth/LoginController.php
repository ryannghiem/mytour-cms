<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $rules = [];

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->rules = [
            'email' => 'required|email',
            'password' => 'required|string'
        ];
    }

    public function postLogin(Request $request)
    {
        $failedLogin = Session::has('failedLogin') ? Session::get('failedLogin') : 0;
        $rule = $this->rules;
        if ($failedLogin > 3) $rule['g-recaptcha-response'] = 'required|recaptcha';
        $this->validate($request, $rule);
        $email = $request->input('email', '');
        $password = $request->input('password', '');
        $remember = $request->input('remember', false);
        $salt = DB::table('users')->where('email', '=', $email)->first(['salt']);
        if($salt){
            $password = User::_encrypt($password, $salt->salt);
            $user = DB::table('users')->where('email', '=', $email)->where('password', '=', $password)->where('status', '=', 1)->where('user_type','>',1)
                ->first(['id', 'email', 'fullname', 'user_type', 'status']);
            if($user){
                $user = (array)$user;
                Auth::loginUsingId($user['id'], $remember);
                $userWithStore = User::where('id', '=', $user['id'])->first();
                Session::forget('failedLogin');
                Session::put('user', $userWithStore);
                return $userWithStore;
            }else{
                $failedLogin ++;
                Session::put('failedLogin', $failedLogin);
                return ['error' => '404',
                    'message' => 'Wrong email / password!',
                    'failedLogin' => $failedLogin > 3,
                    'rule' => $rule
                ];
            }
        }else{
            $failedLogin ++;
            Session::put('failedLogin', $failedLogin);
            return ['error' => '404',
                'message' => 'Wrong email / password!',
                'failedLogin' => $failedLogin > 3,
                'rule' => $rule
            ];
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->flush();
        Session::flush();
        return response()->json(['msg' => Session::get('user')]);
    }

    public function failedLogin(){
        return response(['data' => Session::has('failedLogin') ? Session::get('failedLogin') > 3 : false]);
    }
}
