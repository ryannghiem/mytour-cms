<?php namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;

class TokenController extends Controller {

    public function token()
    {
        return csrf_token();
    }

    public function checkRememberLogin(){
        if (Auth::check()) {
            return \Session::get('user');
        }else return ['status' => false];
    }
}
