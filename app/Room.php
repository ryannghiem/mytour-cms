<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    protected $table = 'rooms';

    protected $fillable = ['id',
        'name',
        'hotel_id',
        'description',
        'size',
        'price',
//        'image',
        'ordering',
        'status',
        'has_discount',
        'discount_percent',
        'discount_price',
        'discount_start',
        'discount_end',
        'created_by',
        'updated_by'
    ];

    public function author()
    {
        return $this->belongsTo('App\User', 'created_by')->select(['id', 'fullname', 'email']);
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hotel', 'hotel_id')->select(['id', 'name', 'address']);
    }
}
