<?php

namespace App\Providers;

use App\City;
use App\Hotel;
use App\Room;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //

        Hotel::created(function($hotel)
        {
            $city = City::find($hotel->city_id);
            if ($city) {
                $city->hotel_count ++;
                $city->save();
            }
        });
        Room::created(function($room)
        {
            $hotel = Hotel::find($room->hotel_id);
            if ($hotel) {
                $hotel->room_count ++;
                $hotel->save();
            }
        });
    }
}
