<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    //
    protected $table = 'cities';

    protected $fillable = ['id',
        'name',
        'country_id',
        'is_hot',
        'hotel_count',
        'ordering',
        'status',
        'created_by',
        'updated_by'
    ];
}
