<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    //Store
    Route::get('store/list', ['uses' => 'StoreController@index']);
    Route::get('store/edit/{id}', ['uses' => 'StoreController@edit']);
    Route::post('store/update', ['uses' => 'StoreController@update']);
    Route::get('store/delete', ['uses' => 'StoreController@delete']);
    Route::post('store/changeStatus', ['uses' => 'StoreController@changeStatus']);
    Route::get('store/getStores', ['uses' => 'StoreController@getStores']);
    Route::get('store/getStoresSelected', ['uses' => 'StoreController@getStoresSelected']);
    Route::get('store/getStoreSelected', ['uses' => 'StoreController@getStoreSelected']);
    Route::get('store/checkDuplicateCode', ['uses' => 'StoreController@checkDuplicateCode']);


    // Upload image
    Route::post('upload/image', ['uses' => 'UploadController@index']);
    Route::post('upload/ckeditorImage', ['uses' => 'UploadController@ckeditorImage']);

    // Session table
    Route::get('session/show/{id}', ['uses' => 'SessionController@show']);
    Route::post('session/new', ['as' => 'new', 'uses' => 'SessionController@store']);
    Route::post('session/update/{id}', ['as' => 'update', 'uses' => 'SessionController@update']);
    Route::get('session/delete/{id}', ['uses' => 'SessionController@destroy']);

    // User table
    //Staff
    Route::get('staff/list', ['uses' => 'StaffController@index']);
    Route::get('staff/all', ['uses' => 'StaffController@allStaffsPublic']);
    Route::get('staff/all-staffs', ['uses' => 'StaffController@allStaffs']);
    Route::get('staff/edit/{id}', ['uses' => 'StaffController@edit']);
    Route::post('staff/update', ['uses' => 'StaffController@update']);
    Route::get('staff/delete', ['uses' => 'StaffController@delete']);
    Route::post('staff/changeStatus', ['uses' => 'StaffController@changeStatus']);

    Route::get('user/listSelect2', ['uses' => 'UserController@listSelect2']);
    Route::get('user/select2Selected', ['uses' => 'UserController@select2Selected']);
    Route::get('user/profile', ['uses' => 'UserController@profile']);
    Route::post('user/update/{id}', ['uses' => 'UserController@update']);
    Route::post('user/whereIn', ['uses' => 'UserController@whereIn']);
    Route::get('user/getUsersInRole/{id}', ['uses' => 'UserController@getUsersInRole']);
    Route::post('user/get-permissions', ['uses' => 'UserController@userPermission']);
    Route::get('user/getPer', ['uses' => 'UserController@getPer']);
    Route::get('user/all', ['uses' => 'UserController@getAllEmail']);
    Route::get('user/listByEmails', ['uses' => 'UserController@listByEmails']);
    Route::get('user/getDebt', ['uses' => 'CustomerController@sumDebt']);
    Route::get('user/getDebtDetail', ['uses' => 'CustomerController@debtDetail']);
    Route::get('user/getDebtDetailWithoutInvoice', ['uses' => 'CustomerController@debtDetailWithoutInvoice']);
    Route::post('user/change-pass', ['uses' => 'UserController@changePass']);
    Route::post('staff/checkPass', ['uses' => 'StaffController@checkPass']);
    Route::post('user/resetPass', ['uses' => 'UserController@resetPass']);
    Route::get('user/checkEmailExist', ['uses' => 'UserController@checkEmailExist']);

    //City
    Route::post('city/changeStatus', ['uses' => 'CityController@changeStatus']);
    Route::get('city/getCities', ['uses' => 'CityController@getCities']);
    Route::get('city/getCitySelected', ['uses' => 'CityController@getCitySelected']);
    Route::get('city/getCitiesSelected', ['uses' => 'CityController@getCitiesSelected']);
    Route::resource('city', 'CityController');

    //Hotel
    Route::post('hotel/changeStatus', ['uses' => 'HotelController@changeStatus']);
    Route::get('hotel/getHotels', ['uses' => 'HotelController@getHotels']);
    Route::get('hotel/getHotelSelected', ['uses' => 'HotelController@getHotelSelected']);
    Route::get('hotel/getHotelsSelected', ['uses' => 'HotelController@getHotelsSelected']);
    Route::resource('hotel', 'HotelController');

    //Room
    Route::post('room/changeStatus', ['uses' => 'RoomController@changeStatus']);
    Route::post('room/changeOrder', ['uses' => 'RoomController@changeOrder']);
    Route::get('room/getRooms', ['uses' => 'RoomController@getRooms']);
    Route::get('room/getRoomSelected', ['uses' => 'RoomController@getRoomSelected']);
    Route::get('room/getRoomsSelected', ['uses' => 'RoomController@getRoomsSelected']);
    Route::resource('room', 'RoomController');

    //Slide
    Route::post('slide/changeStatus', ['uses' => 'SlideController@changeStatus']);
    Route::post('slide/changeOrder', ['uses' => 'SlideController@changeOrder']);
    Route::get('slide/getSlides', ['uses' => 'SlideController@getSlides']);
    Route::get('slide/getSlideSelected', ['uses' => 'SlideController@getSlideSelected']);
    Route::get('slide/getSlidesSelected', ['uses' => 'SlideController@getSlidesSelected']);
    Route::resource('slide', 'SlideController');

    //Ads
    Route::post('ads/changeStatus', ['uses' => 'AdsController@changeStatus']);
    Route::post('ads/changeOrder', ['uses' => 'AdsController@changeOrder']);
    Route::get('ads/getAds', ['uses' => 'AdsController@getAds']);
    Route::resource('ads', 'AdsController');

    //Bookings
    Route::post('booking/changeStatus', ['uses' => 'BookingController@changeStatus']);
    Route::post('booking/changeOrder', ['uses' => 'BookingController@changeOrder']);
    Route::get('booking/getBookings', ['uses' => 'BookingController@getBookings']);
    Route::resource('booking', 'BookingController');
});

// User
Route::post('user/authenticate', 'Auth\LoginController@postLogin');
Route::get('user/session', ['as' => 'session', 'uses' => 'UserController@session']);

Route::get('user/logout', ['uses' => 'Auth\LoginController@logout']);

Route::get('user/failed-login', ['uses' => 'Auth\LoginController@failedLogin']);
//Get _token for Form submit
Route::get('auth/token', 'Auth\TokenController@token');


